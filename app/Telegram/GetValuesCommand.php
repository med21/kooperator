<?php

namespace App\Telegram;

use App\AllianceUsers;
use App\AllianceUsersValues;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class GetValuesCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'values';

    /**
     * @var string Command Description
     */
    protected $description = 'Команда відображає останні показники та сплачену суму';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $telegram_user = Telegram::getWebhookUpdates()['message']['from'];

        if ($telegram_user) {
            if ($user_data = AllianceUsers::where('alliance_user_email', 'like', '%' . $telegram_user['username'] . '%')->first()) {

                if($user_data->alliance_user_telegram_id == null){
                    $user_data->alliance_user_telegram_id= $telegram_user['id'];
                    $user_data->update();
                }

                $user_values = AllianceUsersValues::where('alliance_id', $user_data['alliance_id'])->where('alliance_users_id', $user_data['id'])->get()->reverse()->first();

                $text = sprintf('%s', 'Ваші останні показники:' . PHP_EOL);
                $text .= sprintf('%s', 'Дата зняття показників: ' . $user_values['alliance_users_date_check'] . PHP_EOL);
                $text .= sprintf('%s', 'День: ' . $user_values['alliance_users_val_d'] . PHP_EOL);
                $text .= sprintf('%s', 'Ніч: ' . $user_values['alliance_users_val_n'] . PHP_EOL);
                $text .= sprintf('%s', 'Сплачено: ' . round($user_values['alliance_users_payment_total_debt'], 2) . ' грн.');
            } else {
                $text = sprintf('%s', 'Користувач з нік-неймом:' . $telegram_user . ' не знайдено у сістемі' . PHP_EOL);
                $text .= sprintf('%s', 'Зверніться до адміністратора, info@it-hause.com' . PHP_EOL);
            }

            $this->replyWithMessage(['text' => $text]);
        }
    }
}