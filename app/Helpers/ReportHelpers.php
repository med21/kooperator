<?php

function lastMonth($previous_month)
{
    if (($previous_month->alliance_counter_payment_total - $previous_month->alliance_counter_paid_gov) > 0) {
        $profit = $previous_month->alliance_counter_payment_total - $previous_month->alliance_counter_paid_gov;
        return ($profit);
    } else {
        $loss = $previous_month->alliance_counter_payment_total - $previous_month->alliance_counter_paid_gov;
        return ($loss);
    }
}

function thisYear($thisYear)
{
    if (($thisYear->sum('alliance_counter_payment_total')) - ($thisYear->sum('alliance_counter_paid_gov')) > 0)
    {
    $profitY = ($thisYear->sum('alliance_counter_payment_total')) - ($thisYear->sum('alliance_counter_paid_gov'));
    return ($profitY);
}else{
    $lossY = ($thisYear->sum('alliance_counter_payment_total')) - ($thisYear->sum('alliance_counter_paid_gov'));
    return ($lossY);
}
}