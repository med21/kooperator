<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Auth;
use DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email',
        'password','member_id','avatar',
        'end_date', 'status',
        'order_id', 'surname', 'middle_name', 'tel',
        'alliance_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRolById($id)
    {
        $sql = "SELECT role_id FROM role_user WHERE user_id=". $id;
        $rolUser = DB::select($sql);
        return $rolUser;
    }

    public function allianceID()
    {
        $allianceId = Alliance::where('alliance_chef_id', Auth::user()->id)->select('id')->get()[0];
        return $allianceId;
    }

    public function getFeesCount()
    {
        return DB::table('fees')->select('*')
            ->where('alliance_user_id',$this->member_id)
            ->where('alliance_user_pay_status','=', 0)
            ->count();
    }
    public function gAlliance()
    {
        return Alliance::where('alliance_chef_id','=', Auth::user()->id)->get()->first();
    }

}
