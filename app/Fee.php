<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'alliance_user_id',
            'alliance_user_fee_sum',
            'alliance_user_pay_date',
            'alliance_user_pay_purpose',
            'alliance_user_pay_desc',
            'alliance_user_pay_status',
        ];
}
