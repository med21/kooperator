<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'user_id',
            'amount',
            'type',
            'date',
            'status',
            'result',
            'key',
            'desc',
        ];
}
