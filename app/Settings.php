<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    public $fillable =
        [
            'alliance_id',
            'liqpay_pr_key',
            'liqpay_id',
        ];
}
