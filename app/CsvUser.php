<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CsvUser extends Model
{

    protected $table = 'csv_user';

    protected $fillable = ['csv_filename', 'csv_header', 'csv_data'];

}
