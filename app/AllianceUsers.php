<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AllianceUsers extends Model
{
    use Notifiable;

    protected $table = 'alliance_users';
    protected $fillable =
        [
            'alliance_user_surname',
            'alliance_user_name',
            'alliance_user_middlename',
            'alliance_user_address',
            'alliance_user_counter_number',
            'alliance_user_counter_date',
            'alliance_user_tel',
            'alliance_user_email',
            'alliance_user_counter_type',
            'alliance_id',
            'avatar',
            'alliance_users_pensioner',
            'alliance_users_pensioner_number',
            'alliance_users_land_square',
            'alliance_users_land_doc_numb',
            'alliance_users_land_doc_date',
        ];

    public function val()
    {
        return $this->hasMany('App\AllianceUsersValues', 'alliance_users_id');
    }
    
}
