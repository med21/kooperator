<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceUsersValues extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'alliance_users_id',
            'alliance_users_val_d',
            'alliance_users_val_n',
            'alliance_users_consumption_d',
            'alliance_users_consumption_n',
            'alliance_users_date_check',
            'alliance_users_date_interval',
            'alliance_users_payment_d',
            'alliance_users_payment_n',
            'alliance_users_payment_total',
            'alliance_users_payment_total_debt',
            'alliance_users_payment_is_paid',
        ];
    // Relation Many UsersValues belongs to Specific AllianceUsers
    public function usersval()
    {
        return $this->belongsTo('App\AllianceUsers', 'id');
    }
}
