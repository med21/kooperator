<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Alliance extends Model
{
    /*
        *'alliance_name', - название кооператива
        *'alliance_chef_id', - глава кооператива
        *'alliance_state', - область
        *'alliance_region', - район
        *'alliance_village', - ближайший населенный пунк
        *'alliance_edrpou', - код ЕДРПОУ
        *'alliance_bank_account' - р/р
     */
    protected $fillable=
        [
            'alliance_name',
            'alliance_chef_id',
            'alliance_state',
            'alliance_region',
            'alliance_village',
            'alliance_street',
            'alliance_edrpou',
            'tariff_d',
            'tariff_n',
            'alliance_geo_alt',
            'alliance_geo_ltd',
            'alliance_bank_account',
        ];

    public function getPayout() {
        return Settings::where('alliance_id', $this->id)->get()->first();
    }
    public function getCounter() {
        return AllianceCounter::where('alliance_id', $this->id)->get()->first();
    }
    public function payDay()
    {
        $user = User::where('id', $this->alliance_chef_id)->get()->first();
        $now = Carbon::now();
        $end = Carbon::parse($user->end_date);
        //dd($end);
        if ($end >= $now) {
           return $length = $end->diffInDays($now);
        } else {
            return $length = "Оплаченый срок пользования истек";
        }
    }

    public function getAlliance()
    {
        return Alliance::where('alliance_chef_id','=',Auth::user()->id);
    }

    public function getAllianceUsersCount()
    {
        return AllianceUsers::where('alliance_id','=',$this->id)->count();
    }
    /*
     * $alliance_payout = $alliance->getPayout();
     * if (is_null($alliance_payout)) {
     *      Нема оплати
     * } else {
     *      Є оплата
     * }
     */
}
