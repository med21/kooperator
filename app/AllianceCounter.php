<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceCounter extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'alliance_counter_type',
            'alliance_counter_number',
            'alliance_counter_date',
            'alliance_counter_tariff_d',
            'alliance_counter_tariff_n',
        ];
}
