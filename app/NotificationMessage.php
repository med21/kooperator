<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NotificationUser;
use Hashids;

class NotificationMessage extends Model
{
    protected $fillable =
        [
            'user_id',
            'alliance_id',
            'title',
            'text',
            'end_date',
        ];
    public function userMessagesU(){
        return $this->hasMany('App\NotificationUser','notification_id','id');
    }

    public function getRecipientsCount(){
        return NotificationUser::where('id',$this->id)->count();
    }
}
