<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpendingValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alliance_id' => 'required',
            'propose' => 'required',
            'sum' => 'required',
            'date' => 'required',
        ];
    }
    public  function messages()
    {
        return [
            'propose.required' => 'Назначение платежа обязательно',
            'sum.required' => 'Сумма обязательна',
            'date.required' => 'Дата необходима ',
        ];
    }
}
