<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AllianceValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alliance_name' => 'required|',
            'alliance_chef_id' => 'required|',
            'alliance_state' => 'required|',
            'alliance_region' => 'required|',
            'alliance_village' => 'required|',
            'alliance_street' => 'required|',
            'alliance_edrpou' => 'required|numeric|digits:10|unique:alliances',
            'alliance_bank_account' => 'nullable|numeric',
            'tariff_d' => 'required|numeric',
            'tariff_n' => 'required|numeric',
            'alliance_geo_alt' => 'numeric',
            'alliance_geo_ltd' => 'numeric',
        ];
    }
    public function messages()
    {
        return [
            'alliance_name.required' => 'Название кооператива обязательно',
            'alliance_state.required'  => 'Область обязательна',
            'alliance_region.required'  => 'Район обязателен',
            'alliance_village.required'  => 'Ближайший населенный пункт обязательно',
            'alliance_street.required'  => 'Улица кооператива обязательна',
            'alliance_edrpou.required'  => 'Код ЕДРПОУ кооператива обязателен',
            'alliance_edrpou.numeric'  => 'Код ЕДРПОУ кооператива состоит только из цифр',
            'alliance_bank_account.numeric'  => 'P/P кооператива состоит только из цифр',
            'alliance_edrpou.digits'  => 'Код ЕДРПОУ кооператива состоит только из 10 цифр',
            'tariff_d.required'  => 'Дневной тариф для потребителей кооператива обязателен',
            'tariff_d.numeric'  => 'Дневной тариф состоит из цифр',
            'tariff_n.required'  => 'Ночной тариф для потребителей кооператива обязателен',
            'tariff_n.numeric'  => 'Ночной тариф состоит из цифр',
            'alliance_geo_alt.numeric'  => 'GPS координаты вышоты состоят из цифр',
            'alliance_geo_ltd.numeric'  => 'GPS координаты широты состоят из цифр',
        ];
    }
}
