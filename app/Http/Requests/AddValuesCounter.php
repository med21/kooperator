<?php

namespace App\Http\Requests;

use App\AllianceCounter;
use App\AllianceCounterValues;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Alliance;
use App\AllianceUsers;
use App\AllianceUsersValues;
use Hashids;
use DB;
class AddValuesCounter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->input('alliance_counter_id');
        $id_a = $request->input('alliance_id');
        $user = AllianceCounter::where('alliance_id',$id_a)->where('id',$id)->get();
        $values = AllianceCounterValues::where('alliance_counter_id',$id)->get()->last();

        if($user[0]->alliance_user_counter_type == 2) {

            return [
                'alliance_counter_val_d' => "required|numeric|filled|min:".(is_null($values) ? 0 :$values->alliance_counter_val_d),
                'alliance_counter_val_n' => "required|numeric|filled|min:".(is_null($values) ? 0 :$values->alliance_counter_val_n),
                'alliance_counter_date_check' => 'required|date',
                'alliance_counter_payment_d' => 'required|numeric|filled',
                'alliance_counter_payment_n' => 'required|numeric|filled',
                'alliance_counter_payment_total' => 'required|numeric|filled|min:0',
            ];
        } else{
            return [
                'alliance_counter_val_d' => "required|numeric|filled|min:".(is_null($values) ? 0 : $values->alliance_counter_val_d),
                'alliance_counter_date_check' => 'required|date',
                'alliance_counter_payment_d' => 'required|numeric|filled',
                'alliance_counter_payment_total' => 'required|numeric|filled|min:0',
            ];
        }
    }
    public function messages()
    {
        return [
            'alliance_counter_date_check.required' => 'Необходимо указать дату',
            'alliance_counter_date_check.numeric' => 'Дата - число',
            'alliance_counter_date_check.date' => 'Дата должна быть датой',
            'alliance_counter_val_d.min' => 'Значение потребления не может быть меньше предидущего :min кВт',
            'alliance_counter_val_n.min' => 'Значение потребления не может быть меньше предидущего :min кВт',
            'alliance_counter_payment_total' => 'оплата не может быть меньше 0'
        ];
    }
}
