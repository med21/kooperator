<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AllianceCounterValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alliance_counter_type' =>'required|',
            'alliance_counter_number' =>'required|',
            'alliance_counter_date' =>'required|date|before_or_equal:'.Carbon::now()->format('Y-m-d'),
            'alliance_counter_tariff_d' =>'required|numeric',
            'alliance_counter_tariff_n' =>'required|numeric',
        ];
    }
    public function messages()
    {
        return[
            'alliance_counter_type.required' => 'Тариф счетчика обязателен',
            'alliance_counter_number.required' => 'Номер счетчика обязателен',
            'alliance_counter_date.required' => 'Дата поверки счетчика обязательна',
            'alliance_counter_date.date' => 'Дата должна быть в формате ГГГГ-ММ-ДД прим.'.Carbon::now()->format('Y-m-d'),
            'alliance_counter_date.before_or_equal' => 'Дата поверки не может быть больше чем текущая дата '.Carbon::now()->format('Y-m-d'),
            'alliance_counter_tariff_d.required' => 'Дневной тариф согласно договору с поставщиком Электроенергии',
            'alliance_counter_tariff_n.required' => 'Ночной тариф согласно договору с поставщиком Электроенергии',
            'alliance_counter_tariff_d.numeric' => 'Тариф должен быть указан через точку прим. 1.76',
            'alliance_counter_tariff_n.numeric' => 'Тариф должен быть указан через точку прим. 0.88',
    ];
    }
}
