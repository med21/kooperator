<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AddAllianceUsers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $pensioner = $request->input('alliance_users_pensioner');

        if ($pensioner == 1) {
            return [

                'alliance_user_surname' => 'required',
                'alliance_user_name' => 'required',
                'alliance_user_middlename' => 'required',
                'alliance_user_address' => 'required',
                'alliance_user_counter_number' => 'required',
                'alliance_user_counter_date' => 'required|date',
                'alliance_user_tel' => 'required',
                'alliance_user_email' => 'required',
                'alliance_user_counter_type' => 'required',
                'alliance_users_pensioner_number' => 'required',
                'alliance_users_land_square' => 'required',
                'alliance_users_land_doc_numb' => 'required',
                'alliance_users_land_doc_date' => 'required|date',
            ];
        } else {
            return [

                'alliance_user_surname' => 'required',
                'alliance_user_name' => 'required',
                'alliance_user_middlename' => 'required',
                'alliance_user_address' => 'required',
                'alliance_user_counter_number' => 'required',
                'alliance_user_counter_date' => 'required|date',
                'alliance_user_tel' => 'required',
                'alliance_user_email' => 'required',
                'alliance_user_counter_type' => 'required',
                'alliance_users_land_square' => 'required',
                'alliance_users_land_doc_numb' => 'required',
                'alliance_users_land_doc_date' => 'required|date',
            ];
        }
    }

    public function messages()
    {
        return [

            'alliance_user_surname.required' => 'Фамилия Обязательное поле',
            'alliance_user_name.required' => 'Имя Обязательное поле',
            'alliance_user_middlename.required' => 'Отчество Обязательное поле',
            'alliance_user_address.required' => 'Адрес Обязательное поле',
            'alliance_user_counter_number.required' => 'Номер счетчика Обязательное поле',
            'alliance_user_counter_date.required' => 'Дата Обязательное поле',
            'alliance_user_tel.required' => 'Телефон Обязательное поле',
            'alliance_user_email.required' => 'Мыло Обязательное поле',
            'alliance_user_counter_type.required' => 'Тариф Обязательное поле',
            'alliance_users_pensioner.required' => 'Пенсионер чекбокс Обязательное поле',
            'alliance_users_pensioner_number.required' => 'Пенсионное свидетельствоОбязательное поле',
            'alliance_users_land_square.required' => ' площадь участка Обязательное поле',
            'alliance_users_land_doc_numb.required ' => '№ актаОбязательное поле',
            'alliance_users_land_doc_date.required' => ' Дата акта Обязательное поле',
        ];
    }
}
