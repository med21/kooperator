<?php

namespace App\Http\Controllers;

use App\AdditionalContributor;
use Illuminate\Http\Request;

class AdditionalContributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdditionalContributor  $additionalContributor
     * @return \Illuminate\Http\Response
     */
    public function show(AdditionalContributor $additionalContributor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdditionalContributor  $additionalContributor
     * @return \Illuminate\Http\Response
     */
    public function edit(AdditionalContributor $additionalContributor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdditionalContributor  $additionalContributor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdditionalContributor $additionalContributor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdditionalContributor  $additionalContributor
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdditionalContributor $additionalContributor)
    {
        //
    }
}
