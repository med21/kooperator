<?php

namespace App\Http\Controllers;

use App\AllianceUsers;
use App\Alliance;
use App\Fee;
use Carbon\Carbon;
use Hashids;
use Illuminate\Http\Request;
use DB;
use App\Notifications\NewFeeCreated;
use Illuminate\Notifications\Notifiable;
use Notification;


class FeeController extends Controller
{
    use Notifiable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $allianceUsers)
    {
        $decode1 = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode1);
        $decode2 = Hashids::decode($allianceUsers)[0];
        $user = AllianceUsers::find($decode2);
        $fees = Fee::where('alliance_user_id', $decode2)
            ->where('alliance_id', $decode1)->get();

        //dd($fees);
        return view('fees.fee')->with([
            'alliance' => $alliance,
            'user' => $user,
            'fees' => $fees,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $decode1 = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode1);
        //$decode2 = Hashids::decode($allianceUsers)[0];
        //$user= AllianceUsers::find($decode2);

        //dd($users);
        return view('fees.create')->with([
//            'user'=>$user,
            'alliance' => $alliance,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        include_once(app_path() . '/Sms/smscApi.php');
        $decode1 = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode1);

        $users = AllianceUsers::where('alliance_id', $decode1)->get();

        foreach ($users as $u) {

            $fee = new Fee();
            $fee->alliance_id = $decode1;
            $fee->alliance_user_id = $u->id;
            $fee->alliance_user_fee_sum = $request->input('alliance_user_fee_sum') * $u->alliance_users_land_amount;
            $fee->alliance_user_pay_purpose = $request->input('alliance_user_pay_purpose');
            $fee->alliance_user_pay_desc = $request->input('alliance_user_pay_desc');
            $fee->alliance_user_pay_status = $request->input('alliance_user_pay_status');
            $fee->save();

//            Notification::route('mail',$u->alliance_user_email)->notify(new NewFeeCreated($fee));
//
//            $tel = '+380'.$users->alliance_user_tel;
//            $dateY = Carbon::now()->format('Y');
//            $message = "C/T Jalynka splata chlennskogo vnesku za ".$dateY." Suma ".$request->input('alliance_user_fee_sum')."UAH"."Detal'nishe za adresoju https://it-hause.com/login";
//            //dd($tel,$message);
//            send_sms($tel, $message);

        }
        return redirect('/members/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fee $fee
     * @return \Illuminate\Http\Response
     */
    public function show(Fee $fee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fee $fee
     * @return \Illuminate\Http\Response
     */
    public function edit(Fee $fee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Fee $fee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fee $fee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fee $fee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fee $fee)
    {
        //
    }

    public function payFee(Request $request, $id, $allianceUsers, $fee)
    {
//        include_once(app_path() . '/Sms/smscApi.php');

        $decode2 = Hashids::decode($allianceUsers)[0];
        $user = AllianceUsers::find($decode2);
        $decode3 = Hashids::decode($fee)[0];

        $fees = Fee::find($decode3)
            ->where('alliance_user_id', $decode2)
            ->orderBy('id', 'DESC')->first();

        $fees->alliance_user_pay_status = '1';
        $fees->alliance_user_pay_date = Carbon::today()->format('Y-m-d');
        $fees->update();


        $tel = '+380'.$user->alliance_user_tel;
        $dateY = Carbon::now()->format('Y');
        $message = "C/T Jalynka splata chlenskjgo vnesku za ".$dateY." rik.  Suma ".$fees->alliance_user_fee_sum."UAH "." Detal'nishe za adresoju https://it-hause.com/login";
//        send_sms($tel, $message);

        return redirect('alliance/' . $id . '/dashboard/index')->with([
        ]);
    }
}
