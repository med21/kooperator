<?php

namespace App\Http\Controllers;

use App\AllianceCounter;
use App\AllianceUsers;
use App\Alliance;
use App\AllianceCounterValues;
use App\AllianceUsersValues;
use App\Fee;
use App\User;
use Carbon\Carbon;
use Hashids;
use App\Settings;
use Illuminate\Http\Request;
use DB;

class AllianceDashBoardController extends Controller
{
    public function index($id, Request $request)
    {
        $year = $request->has('year') ? $request->input('year') : date('Y');
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $ck = AllianceCounter::where('alliance_id', $decode)->get()->first();

        $currentYear = date('Y');
        $y = DB::table('alliance_counter_values')->select('*')
            ->selectRaw('YEAR(alliance_counter_date_check) as year', [$currentYear])->get()
            ->keyBy('year')->toArray();
        //dd($y);

        if (is_null($counter = AllianceCounter::where('alliance_id', $decode)->get()->first())) {
            return redirect('/alliance/' . Hashids::encode($alliance->id) . '/counter/create');
        }

        $values = DB::table('alliance_counter_values')
            ->select('*')
            ->where('alliance_counter_id', $counter->id)
            ->where('alliance_counter_date_check', '>=', "{$year}-01-01")
            ->where('alliance_counter_date_check', '<', ($year + 1) . "-01-01")
            ->orderBy('alliance_counter_date_check', 'desc')
            ->get();

        if ($values->sum('alliance_counter_paid_gov') != 0) {
            $profit = $values->sum('alliance_counter_payment_total') - $values->sum('alliance_counter_paid_gov');
        } else {
            $profit = 0;
        }
        $users = AllianceUsers::where('alliance_id', $decode)->count();


        $debtors = DB::table('alliance_users')->select('alliance_users.*', \DB::raw("sum(alliance_users_values.alliance_users_payment_total - alliance_users_values.alliance_users_payment_total_debt) as diff"))
            ->leftJoin('alliance_users_values', function ($join) {
                $join->on('alliance_users_values.alliance_users_id', '=', 'alliance_users.id')->where('alliance_users_values.alliance_users_debt', '!=', '0');
            })->where('alliance_users.alliance_id', $decode)
            ->whereNotNull('alliance_users_values.id')->groupBy('alliance_users.id')
            ->get();
        $debtors_count = $debtors->count();
        $debtors_sum = $debtors->sum('diff');

        $fee_debtor = DB::table('fees')
            ->select('alliance_users.alliance_user_surname',
                'alliance_users.alliance_user_name',
                'alliance_users.alliance_user_middlename',
                'alliance_users.alliance_user_tel',
                'alliance_users.alliance_user_email',
                'alliance_users.id as user_id',
                'fees.alliance_id',
                'fees.alliance_user_id',
                'fees.alliance_user_pay_purpose',
                'fees.alliance_user_fee_sum',
                'fees.alliance_user_pay_desc',
                'fees.alliance_user_pay_status',
                'fees.id as order_id')
            ->leftJoin('alliance_users', function ($join) {
                $join->on('alliance_users.id', '=', 'fees.alliance_user_id');
            })
            ->where('alliance_user_pay_status', '=', 0)
            ->where('fees.alliance_id', $decode)
            ->get();
        //dd($fee_debtor);
        $fee_coll = DB::table('fees')->select('*')
            ->leftJoin('alliance_users', function ($join) {
                $join->on('alliance_users.id', '=', 'fees.alliance_user_id');
            })
            ->where('alliance_user_pay_status', '=', 1)
	        ->where('alliance_user_pay_date','>','2019-01-01')
            ->where('fees.alliance_id', $decode)
            ->where('alliance_user_pay_desc','=','Членський внесок за 2018 рік')
            ->get();
        $fee_collected = $fee_coll->sum('alliance_user_fee_sum');
        //dd($fee_debtor);
        $fee_count = $fee_debtor->count();
        $fee_sum = $fee_debtor->sum('alliance_user_fee_sum');
        //dd($debtors);

        return view('alliance.dashboard.index')->with([
            'alliance' => $alliance,
            'users' => $users,
            'debtors' => $debtors,
            'debtors_count' => $debtors_count,
            'fee_debtor' => $fee_debtor,
            'fee_count' => $fee_count,
            'counter' => $counter,
            'values' => $values,
            'profit' => $profit,
            'year' => $year,
            'y' => $y,
            'fee_sum' => $fee_sum,
            'debtors_sum' => $debtors_sum,
            'fee_collected' => $fee_collected,

        ]);
    }

    public function settings($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $settings = Settings::where('alliance_id', $decode)->get()->first();

        return view('alliance.settings')->with(['alliance' => $alliance, 'settings' => $settings,]);

    }

    public function settingsSave(Request $request, $id)
    {
        $settings = Settings::where('alliance_id', $request->input('alliance_id'))->get()->first();
        if (is_null($settings)) {
            $data = new Settings();
            $data->fill($request->all(), true)->save();
            return redirect('alliance/' . $id . '/dashboard/settings')->with('success', 'Данные сохранены');
        } else {
            $settings->fill($request->all(), true)->update();
            return redirect('alliance/' . $id . '/dashboard/settings')->with('success', 'Данные обновлены');
        }


    }

    public function deleteAlliance($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);

        $users = DB::table('users')->where('alliance_id', '=', $decode)->whereNotNull('member_id')->delete();
        $allianceUsers = DB::table('alliance_users')->where('alliance_id', '=', $decode)->delete();
        $allianceUsersValues = DB::table('alliance_users_values')->where('alliance_id', '=', $decode)->delete();
        $allianceCounterValues = DB::table('alliance_counter_values')->where('alliance_id', '=', $decode)->delete();
        $allianceCounter = DB::table('alliance_counters')->where('alliance_id', '=', $decode)->delete();

//        dd($users,$allianceUsers,$allianceUsersValues,$allianceCounterValues);
        return redirect('/alliance/' . $id . '/delete-account')->with(['success', 'Данные пользователей кооператива и данные счетчиков удалены!'])->with(compact('alliance'));
    }

    public function deleteAccount($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        return view('alliance.alliance_delete')->with(compact('alliance'));
    }
    public function deleteAcc($id)
    {
        $decode = Hashids::decode($id)[0];
        $user = \Auth::user()->id;
        $alliance = DB::table('alliances')->where('id','=',$decode)->delete();
        $alliance_chef = DB::table('alliance_chefs')->where('user_id','=',$user)->delete();
        \Auth::user()->delete();

        return redirect('/');
    }
}
