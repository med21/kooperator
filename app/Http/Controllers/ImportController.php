<?php

namespace App\Http\Controllers;

use App\AllianceUsersValues;
use App\Alliance;
use App\CsvData;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Hashids;

class ImportController extends Controller
{

    public function getImport($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        return view('import.import')->with([
            'alliance'=>$alliance,
        ]);
    }

    public function parseImport(CsvImportRequest $request, $id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);

        $path = $request->file('csv_file')->getRealPath();

        if ($request->has('header')) {
            $data = Excel::load($path, function($reader) {})->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $csv_data = array_slice($data, 0, 2);

            $csv_data_file = CsvData::create([
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data)
            ]);
        } else {
            return redirect()->back();
        }

        return view('import.import_fields', compact( 'csv_header_fields', 'csv_data', 'csv_data_file','alliance'));

    }

    public function processImport(Request $request,$id)
    {
        $data = CsvData::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        foreach ($csv_data as $row) {
            $contact = new AllianceUsersValues();
            foreach (config('app.db_fields') as $index => $field) {
                if ($data->csv_header) {
                    $contact->$field = $row[$request->fields[$field]];
                } else {
                    $contact->$field = $row[$request->fields[$index]];
                }
            }
            $contact->save();
        }

        return redirect('/members/'.$id)->with('success', 'Данные импортированы');
    }

}
