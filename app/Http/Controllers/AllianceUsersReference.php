<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Alliance;
use App\AllianceUsers;
use App\User;
use Hashids;
use DB;
use PDF;

class AllianceUsersReference extends Controller
{
    public function index($id, $allianceUsers)
    {
        $decode = Hashids::decode($id)[0];
        $decode2 = Hashids::decode($allianceUsers)[0];
        $alliance = Alliance::find($decode)->first();
        $user = AllianceUsers::where('id',$decode2)->where('alliance_id',$decode)->get()->first();
        $chef = User::where('id','=',$alliance->alliance_chef_id)->get()->first();

        $pdf = PDF::loadView('users.references.privat_home', compact('alliance','user','chef'),[
            'format' => 'A4'
        ]);
        $filename = Carbon::now().'home.pdf';
        return $pdf->download($filename);
    }
}
