<?php

namespace App\Http\Controllers\Apiv2;

use App\AllianceCounter;
use App\AllianceCounterValues;
use App\AllianceUsers;
use App\Fee;
use App\Http\Requests\AddValues;
use App\Http\Requests\AddValuesCounter;
use App\Settings;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\AllianceUsersValues;
use DB;
use Validator;

class ApiAllianceUsersValuesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

//$user = User::find(Auth::user()->getAuthIdentifier());
//
//if($user->member_id == null){
//    $values =
//return response()->json(['data'=>$values], 200);
//}else{
//    $values =
//    return response()->json(['data'=>$values], 200);
//}
//return response()->json( 'Bad Request', 404);


    /*GET usersValues
     * if user hasRole->alliance_chef => isNull(member_id)===true
     * if user hasRole->alliance_user = (member_id) == alliance_users_id in Table(alliance_users_values)
     *  */

    private function validator($data)
    {
        return Validator::make($data, [
            'image' => 'image|max:2048',
        ]);
    }

    public function usersValues()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        if ($user) {
            if ($user->member_id == null) {
                $values = DB::table('alliance_counter_values')->select('*')
                    ->where('alliance_id', $user->alliance_id)->get();
                return response()->json($values, 200);
            } else {
                $values = DB::table('alliance_users_values')->select('*')
                    ->where('alliance_users_id', $user->member_id)->get();
                return response()->json($values, 200);
            }
        }
        return response()->json('Bad Request', 404);
    }

    /*GET all fees of AllianceUsers
     *
     * if user hasRole->alliance_user = all his fee will be shown
     * if user hasRole->alliance_chef = all unpaid fees will be shown
     * */
    public function usersFee()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        if ($user) {
            if ($user->member_id == null) {
                $values = DB::table('fees')->select('*')
                    ->where('alliance_id', $user->alliance_id)->get();
                return response()->json($values, 200);
            } else {
                $values = DB::table('fees')->select('*')
                    ->where('alliance_users_id', $user->member_id)->get();
                return response()->json($values, 200);
            }
        }
        return response()->json('Bad Request', 404);
    }

    /*GET LastCounterValues - preparation to add NEW COUNTER_VALUES
     * if user hasRole->alliance_chef = isNull(member_id) -> show LastValues
     * if user hasRole->alliance_user = (member_id) == alliance_users_id in Table(alliance_users_values)
     *  */

    public function latestUserValues()
    {
        if ($user = User::find(Auth::user()->getAuthIdentifier())) {

            if ($user->member_id == null) {
                $values = DB::table('alliance_counter_values')->select('*')
                    ->where('alliance_id', $user->alliance_id)->latest()
                    ->first();
                if (empty($values)) {
                    $values = [
                        'alliance_counter_val_d' => 0,
                        'alliance_counter_val_n' => 0,
                        'alliance_counter_date_check' => now(3)->format('Y-m-d')
                    ];
                }
                return response()->json($values, 200);
            } else {
                $values = DB::table('alliance_users_values')->select('*')
                    ->where('alliance_users_id', $user->member_id)->latest()
                    ->first();

                return response()->json($values, 200);
            }
        }
        return response()->json('Bad Request', 404);
    }

    /*POST NewCounterValues -  add NEW COUNTER_VALUES*/
    public function addAllianceValues()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $alliance_counter = AllianceCounter::where('alliance_id', $user->alliance_id)->get()->first();
        $alliance_counter_val_d = request()->json()->all()['alliance_counter_val_d'];
        $alliance_counter_val_n = request()->json()->all()['alliance_counter_val_n'];
        $alliance_counter_date_check = request()->json()->all()['alliance_counter_date_check'];
        $previous = json_decode($this->latestUserValues()->content());
        $day_diff = Carbon::parse($alliance_counter_date_check)->diffInDays($previous->alliance_counter_date_check);
        $pay_d = ($alliance_counter_val_d - $previous->alliance_counter_val_d) * $alliance_counter->alliance_counter_tariff_d;
        $pay_n = ($alliance_counter_val_n - $previous->alliance_counter_val_n) * $alliance_counter->alliance_counter_tariff_n;
        $total = $pay_d + $pay_n;

        $data = new AllianceCounterValues();

        $data->alliance_id = $user->alliance_id;
        $data->alliance_counter_id = $alliance_counter->id;
        $data->alliance_counter_val_d = $alliance_counter_val_d;
        $data->alliance_counter_val_n = $alliance_counter_val_n;
        $data->alliance_counter_consumption_d = $alliance_counter_val_d - $previous->alliance_counter_val_d;
        $data->alliance_counter_consumption_n = $alliance_counter_val_n - $previous->alliance_counter_val_n;
        $data->alliance_counter_date_check = $alliance_counter_date_check;
        $data->alliance_counter_date_interval = $day_diff;
        $data->alliance_counter_payment_d = $pay_d;
        $data->alliance_counter_payment_n = $pay_n;
        $data->alliance_counter_payment_total = $total;
        $data->alliance_counter_payment_total_debt = $total;
        $data->alliance_counter_paid_gov = 0;
        $data->alliance_counter_payment_is_paid = 0;
        $data->save();

        return response()->json('values added', 200);
    }

    public function deleteAllianceValues()
    {
        $value = AllianceCounterValues::where('id', request()->json()->all()['id'])->get()->first();
        if ($value) {
            $value->delete();
            return response()->json('Data Deleted', 200);
        }
        return response()->json('Not found, Bad Request', 404);
    }

    public function createFee()
    {
        $data = DB::table('fees_purposes')->insert([
            'title' => request()->json()->all()['title'],
            's_title' => request()->json()->all()['title'],
            'description' => request()->json()->all()['description'],
        ]);
        return response()->json('New Fee Created', 200);
    }

    public function editUserInfo()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        if ($user) {
            if (isset(request()->json()->all()['name'])) {
                $user->name = request()->json()->all()['name'];
                $user->update();
                return response()->json('success', 200);
            }
//            dd(request()->file('avatar'));
////            $validator = $this->validator(request()->getContent());
//            if(isset(request()['avatar'])){
//
//
//                if ($validator->fails()) {
//
//                    return response()->json($validator->errors(),412);
//                }
//                if($image->isValid()){
//                    $imageName = $image->getClientOriginName();
//                    $imagePath = storage_path('avatars').$user->id.'/';
//
//                    $user->avatar = $imagePath.$imageName;
//                    $user->update();
//
//                    $image->move($imagePath,$imageName);
//
//                    return response('success',200);
//                }

            return response('Conflict', 409);
        }
        if (isset(request()->json()->all()['surname'])) {
            $user->surname = request()->json()->all()['surname'];
            $user->update();
            return response()->json('success', 200);
        }
        if (isset(request()->json()->all()['middle_name'])) {
            $user->middle_name = request()->json()->all()['middle_name'];
            $user->update();
            return response()->json('success', 200);
        }
        if (isset(request()->json()->all()['tel'])) {
            $user->tel = request()->json()->all()['tel'];
            $user->update();
            return response()->json('success', 200);
        }
        if (isset(request()->json()->all()['email'])) {
            $user->email = request()->json()->all()['email'];
            $user->update();
            return response()->json('success', 200);
        }
        return response('Nothing has been changed', 200);

        return response()->json('Not found, Bad Request', 404);
    }

    public function addAllianceValuesWithPayment()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $alliance_counter = AllianceCounter::where('alliance_id', $user->alliance_id)->get()->first();
        $alliance_counter_val_d = request()->json()->all()['alliance_counter_val_d'];
        $alliance_counter_val_n = request()->json()->all()['alliance_counter_val_n'];
        $alliance_counter_paid = request()->json()->all()['$alliance_counter_paid'];
        $alliance_counter_date_check = request()->json()->all()['alliance_counter_date_check'];
        $previous = json_decode($this->latestUserValues()->content());
        $day_diff = Carbon::parse($alliance_counter_date_check)->diffInDays($previous->alliance_counter_date_check ?? now(3));
        $pay_d = ($alliance_counter_val_d - $previous->alliance_counter_val_d ?? 0) * $alliance_counter->alliance_counter_tariff_d;
        $pay_n = ($alliance_counter_val_n - $previous->alliance_counter_val_n ?? 0) * $alliance_counter->alliance_counter_tariff_n;
        $total = $pay_d + $pay_n;
//        if($alliance_counter_val_d < $previous->alliance_counter_val_d){
//            return response()->json( 'Day Value can\'t be less than previous' , 301);
//        }
//        if($alliance_counter_val_n < $previous->alliance_counter_val_n){
//            return response()->json( 'Day Value can\'t be less than previous' , 301);
//        }
        dd($pay_d);
        $data = new AllianceCounterValues();

        $data->alliance_id = $user->alliance_id;
        $data->alliance_counter_id = $alliance_counter->id;
        $data->alliance_counter_val_d = $alliance_counter_val_d;
        $data->alliance_counter_val_n = $alliance_counter_val_n;
        $data->alliance_counter_consumption_d = $alliance_counter_val_d - $previous->alliance_counter_val_d;
        $data->alliance_counter_consumption_n = $alliance_counter_val_n - $previous->alliance_counter_val_n;
        $data->alliance_counter_date_check = $alliance_counter_date_check;
        $data->alliance_counter_date_interval = $day_diff;
        $data->alliance_counter_payment_d = $pay_d;
        $data->alliance_counter_payment_n = $pay_n;
        $data->alliance_counter_payment_total = $total;
        $data->alliance_counter_payment_total_debt = $total;
        $data->alliance_counter_paid_gov = 0;
        $data->alliance_counter_payment_is_paid = $alliance_counter_paid ?? 0;
        $data->save();

//        if(request()->json()->all()['payment']){
//
//        }

        return response()->json('values added', 200);
    }

    public function getTariff()
    {
        $user = $user = User::find(Auth::user()->getAuthIdentifier());
        $tariff = DB::table('alliance_counters')
            ->select(\DB::raw('ROUND(alliance_counter_tariff_d,3) as day, ROUND(alliance_counter_tariff_n,3) as night'))
            ->where('alliance_id', $user->alliance_id)
            ->get();

        return response()->json($tariff, 200);
    }

    public function dropAllValues()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        if ($user->alliance_id !== 1) {
            AllianceCounterValues::where('alliance_user_id', $user->alliance_id)->delete();

            return response()->json('All values deleted', 203);
        }
        return response()->json('error - no permission to delete values in this account', 403);
    }

    public function getAllianceCredential()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        if (!is_null($user->alliance_id)) {
            $credential = Settings::where('alliance_id', $user->alliance_id)->select('liqpay_id', 'liqpay_pr_key')->get();

            return response()->json($credential, 200);
        }

        return response()->json('error  - no data found', 404);
    }

    public function getAllianceUsers()
    {
        $user = User::find(Auth::user()->getAuthIdentifier());
        $users = DB::table('alliance_users')
            ->select('id as user_id', 'alliance_user_name', 'alliance_user_middlename', 'alliance_user_surname')
            ->where('alliance_id', $user->alliance_id)
            ->get();

        return response()->json($users, 200);
    }
}
