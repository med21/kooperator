<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;


class TelegramBotController extends Controller
{
    public function telegram()
    {
        $telegram = Telegram::getMe();
        $data = ['telegram_url' => '',
            'bot_name' => $telegram['first_name'],
            'user_name' => $telegram['username'],
            'bot_id' => $telegram['id']
        ];
        return view('admin.telegram')->with(['data' => $data]);
    }

    public function setWH()
    {
        $response = Telegram::setWebhook(['url' => 'https://it-hause.com/telegrambot/'.Telegram::getAccessToken().'/']);

        return redirect()->route('telegram')->with('status',$response);
    }

    public function setWebHook(Request $request)
    {
        $result = $this->sendTelegramData('telegrambot', [
            'query' => ['url' => $request->url . '/' . Telegram::getAccessToken().'/']
        ]);
        return redirect()->route('telegram')->with('status', $result);
    }

    public function getWebHookInfo(Request $request)
    {
        $result = $this->sendTelegramData('getWebhookInfo');

        return redirect()->route('telegram')->with('status', $result);
    }


    public function sendTelegramData($route = '', $params = [], $method = 'POST')
    {
        $token = config('telegram.bot_token');
        $client = new Client(['base_uri' => 'https://api.telegram.org/bot' . $token . '/']);
        $result = $client->request($method, $route, $params);

        return (string)$result->getBody();
    }
}
