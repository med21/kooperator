<?php

namespace App\Http\Controllers;

use App\AllianceChef;
use Illuminate\Http\Request;

class AllianceChefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllianceChef  $allianceChef
     * @return \Illuminate\Http\Response
     */
    public function show(AllianceChef $allianceChef)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllianceChef  $allianceChef
     * @return \Illuminate\Http\Response
     */
    public function edit(AllianceChef $allianceChef)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AllianceChef  $allianceChef
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllianceChef $allianceChef)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllianceChef  $allianceChef
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllianceChef $allianceChef)
    {
        //
    }
}
