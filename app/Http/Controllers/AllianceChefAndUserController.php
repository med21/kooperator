<?php

namespace App\Http\Controllers;

use App\AllianceChefAndUser;
use Illuminate\Http\Request;

class AllianceChefAndUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllianceChefAndUser  $allianceChefAndUser
     * @return \Illuminate\Http\Response
     */
    public function show(AllianceChefAndUser $allianceChefAndUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllianceChefAndUser  $allianceChefAndUser
     * @return \Illuminate\Http\Response
     */
    public function edit(AllianceChefAndUser $allianceChefAndUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AllianceChefAndUser  $allianceChefAndUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllianceChefAndUser $allianceChefAndUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllianceChefAndUser  $allianceChefAndUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllianceChefAndUser $allianceChefAndUser)
    {
        //
    }
}
