<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\AllianceUsersValues;
use App\AllianceUsers;
use App\Alliance;
use Maatwebsite\Excel\Facades\Excel;
use Hashids;
use DB;

class ValuesExportController extends Controller
{
    public function exportValues($id, AllianceUsersValues $allianceUsersValues)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $data = AllianceUsers::where('alliance_id',$alliance->id)->select('id','alliance_user_surname','alliance_user_name','alliance_user_middlename')->get()->toArray();

        Excel::create('userValuesTemplate-' . Carbon::now(), function ($excel) use ($data) {

            $excel->sheet('Sheetname', function ($sheet) use ($data) {

                $sheet->fromArray($data);

            });
        })->export('csv');

    }

    public function exportUsers($id, AllianceUsers $allianceUsers)
    {
        $decode = Hashids::decode($id)[0];
        $data = AllianceUsers::where('alliance_id', $decode)
            ->get()->map(function ($user) {
                $data = $user->toArray();
                foreach ($data as $key => $value) {
                    if (in_array($key, ['avatar', 'created_at', 'updated_at'])) {
                        unset($data[$key]);
                    } else if ($key != 'id') {
                        $data[$key] = "";
                    }
                }
                return $data;
            })->toArray();
        //dd($data);

        Excel::create('userValuesTemplate-' . Carbon::now(), function ($excel) use ($data) {
            $excel->sheet('Sheetname', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->export('csv');

    }
}
