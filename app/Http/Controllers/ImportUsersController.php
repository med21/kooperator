<?php

namespace App\Http\Controllers;

use App\AllianceUsers;
use App\User;
use App\Alliance;
use App\CsvUser;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Hashids;

class ImportUsersController extends Controller
{

    public function getImport($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        return view('members.import.import')->with([
            'alliance' => $alliance,
        ]);
    }

    public function parseImport(CsvImportRequest $request, $id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);

        $path = $request->file('csv_file')->getRealPath();

        if ($request->has('header')) {
            $data = Excel::load($path, function ($reader) {
            })->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        if (count($data) > 0) {
            if ($request->has('header')) {
                $csv_header_fields = [];
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }
            $csv_data = array_slice($data, 0, 2);

            $csv_data_file = CsvUser::create([
                'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data)
            ]);
        } else {
            return redirect()->back();
        }
        //dd($csv_data_file);
        return view('members.import.import_fields', compact('csv_header_fields', 'csv_data', 'csv_data_file', 'alliance'));

    }

    public function processImport(Request $request, $id)
    {
        $data = CsvUser::find($request->csv_data_file_id);
        $csv_data = json_decode($data->csv_data, true);
        foreach ($csv_data as $row) {
            $contact = new AllianceUsers();
            foreach (config('us.users_fields') as $index => $field) {
                if ($data->csv_header) {
                    $contact->$field = $row[$request->fields[$field]];
                } else {
                    $contact->$field = $row[$request->fields[$index]];
                }
            }
            $contact->save();

            $user = new User();
            $user->member_id = $contact->id;
            $user->name = $row['alliance_user_name'];
            $user->email = $row['alliance_user_email'];
            $user->password = bcrypt($row['alliance_user_tel']);

            $user->save();
                //Добавляем роль "-3-  член кооператива"
            $user->attachRole(3);
        }

        return redirect('/members/' . $id)->with('success', 'Пользователи импортированы');
    }

}
