<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceUsers;
use App\Notifications\TelegramNotifications;
use App\User;
use App\Http\Requests\AddAllianceUsers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Hashids;
use Image;
use Illuminate\Http\Request;

class AllianceUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $alliance_users= AllianceUsers::where('alliance_id',$decode)->get();

        return view('members.index')->with([
            'alliance_users'=>$alliance_users,
            'alliance' => $alliance,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        return view('members.add-user')->with([
            'alliance'=>$alliance,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddAllianceUsers $request,$id)
    {
        $user = new AllianceUsers();
        $user->fill($request->all(),true)->save();

        $alter_user = new User();
        $alter_user->name = $request->input('alliance_user_name');
        $alter_user->alliance_id = $request->input('alliance_id');
        $alter_user->member_id = $user->id;
        $alter_user->surname = $request->input('alliance_user_surname');
        $alter_user->status = 0;
        $alter_user->end_date = \Carbon\Carbon::now()->addYear(1);
        $alter_user->email = $request->input('alliance_user_email');
        $alter_user->password = bcrypt($request->input('alliance_user_tel'));
        $alter_user->save();

        $alter_user->attachRole(3);

        //dd($alter_user);
        return redirect('members/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllianceUsers  $allianceUsers
     * @return \Illuminate\Http\Response
     */
    public function show(AllianceUsers $allianceUsers)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllianceUsers  $allianceUsers
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$allianceUsers)
    {
        $decode1 = Hashids::decode($id)[0];
        $decode = Hashids::decode($allianceUsers)[0];
        $user = AllianceUsers::findOrFail($decode);
        $alliance = Alliance::findOrFail($decode1);

        return view('users.edit')->with([
            'user'=>$user,
            'alliance' => $alliance,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AllianceUsers  $allianceUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $allianceUsers)
    {
        $decode = Hashids::decode($allianceUsers)[0];
        $decode2 = Hashids::decode($id)[0];
        $user = AllianceUsers::find($decode);

        $user->fill($request->all(),true)->update();

        return redirect('members/'.Hashids::encode($decode2))->with('success', 'Успешно обновленны данные!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllianceUsers  $allianceUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $allianceUsers)
    {
        $decode = Hashids::decode($allianceUsers)[0];
        //dd($decode);
        $users = AllianceUsers::findOrFail($decode);
        $users->delete();
        return redirect('members/'.$id)->with('success', 'OK');
    }

    public function userProfile($id, $allianceUsers)
    {
        $decode = Hashids::decode($id)[0];
        $decode2 = Hashids::decode($allianceUsers)[0];
        $users = AllianceUsers::findOrFail($decode2);
        $alliance = Alliance::findOrFail($decode);
        //dd($users);
        return view('users.profile')->with([
            'users'=>$users,
            'alliance' => $alliance,
        ]);
    }

    public function update_avatar(Request $request, $id, $allianceUsers)
    {
        // Logic for user upload of avatar
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

            $d = Hashids::decode($allianceUsers)[0];
            $user = AllianceUsers::find($d);
            //dd($user);
            $user->avatar = $filename;
            $user->save();
        }

        return redirect('/members/'.$id.'/profile/'.$allianceUsers);
    }

    public function chart(Request $request, $id, $allianceUsers)
    {
        $decode= Hashids::decode($allianceUsers)[0];
        $alliance = Alliance::find(Hashids::decode($id)[0]);
        $user = AllianceUsers::find($decode);
        //dd($user);

        if (is_null($user)) {
            return redirect('/');
        }
        $year = Carbon::now()->format('Y') ? $request->input('year') : date('Y');

        //dd($year);


        $values = \DB::table('alliance_users_values')->select(
            \DB::raw("DATE_FORMAT(alliance_users_date_check, '%m') as month"),
            \DB::raw('sum(alliance_users_consumption_d) as day'),
            \DB::raw('sum(alliance_users_consumption_n) as night'))
            ->where('alliance_users_id', $user->id)
            ->where('alliance_users_date_check', '>=', "{$year}-01-01")
            ->where('alliance_users_date_check', '<', ($year + 1) . "-01-01")
            ->groupBy('month')
            ->get()->keyBy('month');

        //dd($values);

        return view('values.chart')->with([
            'user' => $user,
            'year' => $year,
            'night' => ($values->max('night') ?? 0) > 0,
            'max' => (($values->max('day') ?? 10) + ($values->max('night') ?? 10)) * 1.1,
            'values' => $values->toArray(),
            'alliance' => $alliance,
        ]);
    }

    public function showApiSettings($id, $allianceUsers)
    {
        $decode = Hashids::decode($id)[0];
        $decode2 = Hashids::decode($allianceUsers)[0];
        $user = AllianceUsers::findOrFail($decode2);
        $alliance = Alliance::findOrFail($decode);
        return view('members.oauth.index')->with([
            'user'=>$user,
            'alliance' => $alliance
            ]);

    }

    public function sendTelegramMessage(Request $request){

        $decode = Hashids::decode($request->user_id);
        $data = $request->message;

        $user = AllianceUsers::find($decode)->first();
        if($user->alliance_user_telegram_id != null){

            $user->notify(new TelegramNotifications($user,$data));

            return response()->json(['status'=>'Message sended']);
        }

    }
}
