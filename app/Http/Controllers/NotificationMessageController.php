<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceUsers;
use App\NotificationUser;
use App\NotificationMessage;
use Illuminate\Http\Request;
use Hashids;
use DB;
use Carbon\Carbon;
use Auth;

class NotificationMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $users = AllianceUsers::where('alliance_id', $decode)->get();
        $notifications = NotificationMessage::where('alliance_id', $decode)->get();

        return view('alliance.notifications.index')->with([
            'alliance' => $alliance,
            'user' => $users,
            'notifications' => $notifications,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!is_null($request->input('all_users'))) {
            $users = AllianceUsers::find($request->input('alliance_id'))->get();
            $u_count=$users->count($users);

            $notification = new NotificationMessage();
            $notification->alliance_id = $request->input('alliance_id');
            $notification->title = $request->input('title');
            $notification->text = $request->input('text');
            $notification->m_status = 'active';
            $notification->users_count = $u_count;
            $notification->end_date = $request->input('date');
            $notification->save();

            foreach ($users as $user) {
                $message = new NotificationUser();
                $message->user_id = $user->id;
                $message->alliance_id = $request->input('alliance_id');
                $message->notification_id = $notification->id;
                $message->status = 'unread';
                $message->priority = $request->input('priority');
                $message->save();
            }

            return redirect()->back();
        } elseif (is_null($request->input('all_users'))) {
            $u = $request->input('users');
            $users = AllianceUsers::whereIn('id', $u)->get();
            $u_count=$users->count($users);

            $notification = new NotificationMessage();
            $notification->alliance_id = $request->input('alliance_id');
            $notification->title = $request->input('title');
            $notification->text = $request->input('text');
            $notification->m_status = 'active';
            $notification->users_count = $u_count;
            $notification->end_date = $request->input('date');
            $notification->save();

            foreach ($users as $user) {
                $message = new NotificationUser();
                $message->user_id = $user->id;
                $message->alliance_id = $request->input('alliance_id');
                $message->notification_id = $notification->id;
                $message->status = 'unread';
                $message->priority = $request->input('priority');
                $message->save();
            }
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NotificationMessage $notificationMessage
     * @return \Illuminate\Http\Response
     */
    public function show(NotificationMessage $notificationMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NotificationMessage $notificationMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationMessage $notificationMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\NotificationMessage $notificationMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificationMessage $notificationMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NotificationMessage $notificationMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $key)
    {
        $decode = Hashids::decode($id)[0];
        $n = NotificationMessage::where('id',$key)->delete();
        $d = NotificationUser::where('alliance_id',$decode)->where('notification_id',$key)->delete();
        return redirect()->back();
    }
}
