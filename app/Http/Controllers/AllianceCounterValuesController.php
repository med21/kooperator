<?php

namespace App\Http\Controllers;

use App\AllianceCounterValues;
use App\Alliance;
use App\AllianceCounter;
use Carbon\Carbon;
use Hashids;
use Illuminate\Http\Request;
use App\Http\Requests\AddValuesCounter;
use App\NotificationUser;
use App\NotificationMessage;

class AllianceCounterValuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, $allianceCounter)
    {
        $decode = Hashids::decode($allianceCounter)[0];
        $decode2 = Hashids::decode($id)[0];
        $alliance = Alliance::findOrFail($decode2);
        $counter = AllianceCounter::findOrFail($decode);

        $tariff_d = $alliance->tariff_d;
        $tariff_n = $alliance->tariff_n;
        //dd($alliance);
        $data = AllianceCounterValues::where('alliance_counter_id', $decode)->get()->last();
        //dd($data);
        return view('alliance.counter.add-values')->with([
            'alliance' => $alliance,
            'counter' => $counter,
            'tariff_d' => $tariff_d,
            'tariff_n' => $tariff_n,
            'data' => $data ?? [],

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddValuesCounter $request, $id)
    {
        $value = new AllianceCounterValues();
        $value->fill($request->all());
        if ($request->input('alliance_counter_payment_total') == $request->input('alliance_counter_payment_total_debt')) {
            $value->alliance_counter_payment_is_paid = 0;
        } else {
            $value->alliance_counter_payment_is_paid = 0;
        }
        //dd($request->input('alliance_counter_payment_total') == $request->input('alliance_counter_payment_total_debt'));
        $value->save();


        return redirect('/alliance/' . $id . '/dashboard/index')->with('success', 'Данные добавлены!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllianceCounterValues $allianceCounterValues
     * @return \Illuminate\Http\Response
     */
    public function show(AllianceCounterValues $allianceCounterValues)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllianceCounterValues $allianceCounterValues
     * @return \Illuminate\Http\Response
     */
    public function edit(AllianceCounterValues $allianceCounterValues)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\AllianceCounterValues $allianceCounterValues
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllianceCounterValues $allianceCounterValues)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllianceCounterValues $allianceCounterValues
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllianceCounterValues $allianceCounterValues)
    {
        //
    }

    public function payGov(Request $request, $id)
    {
        $value = AllianceCounterValues::where('alliance_id', '=', $request->alliance_id)
            ->where('id', '=', $request->record)->get()->first();

        $value->alliance_counter_paid_gov = $request->input('paid_gov');
        $value->alliance_counter_payment_is_paid = 1;
        $value->update();
        return redirect('/alliance/' . $id . '/dashboard/index')->with('success', 'Данные обновлены!');
    }

    public function markAsRead($id,$allianceUsers,$key)
    {
        $u = NotificationUser::where('alliance_id', Hashids::decode($id))
            ->where('user_id',Hashids::decode($allianceUsers))
            ->where('notification_id', $key)->first();
        $u->status = 'read';
        $u->date = Carbon::now()->format('Y-m-d');
        $u->update();
        $v= NotificationMEssage::where('alliance_id', Hashids::decode($id))
            ->where('id', $key)->first();
        $v->views = $v->views+1;
        $v->update();

        return redirect()->back();
    }
}
