<?php

namespace App\Http\Controllers;

use App\AllianceUsersValues;
use App\AllianceUsers;
use App\Alliance;
use App\Fee;
use Carbon\Carbon;
use DB;
use App\Http\Requests\AddValues;
use Hashids;
use Illuminate\Http\Request;
use App\Settings;
use App\Http\Controllers\LiqPay;
use Mail;
use App\SMS\SMS;

class AllianceUsersValuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @param $id - allianceUsers
     * @param $allianceUsers - Alliance entity
     *
     * Показ данных пользователя по потреблению(данные счетчика пользователя)
     */
    public function index($id, $allianceUsers)
    {
        $decode = Hashids::decode($id)[0];
        $decode2 = Hashids::decode($allianceUsers)[0];
        $alliance = Alliance::findOrFail($decode);
        $user = AllianceUsers::findOrFail($decode2);

        $value = DB::table('alliance_users_values')
            ->select('*')
            ->where('alliance_users_id', $decode2)
            ->orderBy('alliance_users_date_check', 'desc')
            ->get();

        $debt = DB::table('alliance_users_values')
            ->where('alliance_users_id', $decode2)
            ->sum('alliance_users_debt');
        $paid = DB::table('alliance_users_values')
            ->where('alliance_users_id', $decode2)
            ->sum('alliance_users_payment_total_debt');
        $cons_d = DB::table('alliance_users_values')
            ->where('alliance_users_id', $decode2)
            ->sum('alliance_users_consumption_d');
        $cons_n = DB::table('alliance_users_values')
            ->where('alliance_users_id', $decode2)
            ->sum('alliance_users_consumption_n');

        $fees_c = DB::table('fees')->select('*')
            ->where('alliance_user_id', $decode)
            ->where('alliance_user_pay_status', '=', 0)
            ->count();
        $notifications = DB::table('notification_messages')->select('notification_messages.*','notification_users.priority','notification_users.user_id')
            ->leftJoin('notification_users', 'notification_messages.id', '=', 'notification_users.notification_id')
            ->where('notification_messages.end_date', '>', Carbon::now())
            ->where('notification_users.status', 'unread')
            ->where('notification_users.user_id', $user->id)
            ->get();
        return view('values.index')->with([
            'alliance' => $alliance,
            'user' => $user,
            'value' => $value,
            'debt' => $debt,
            'paid' => $paid,
            'cons_d' => $cons_d,
            'cons_n' => $cons_n,
            'fees_c' => $fees_c,
            'notifications' => $notifications,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @param $id - allianceUsers
     * @param $allianceUsers - Alliance entity
     */
    public function create($id, $allianceUsers)
    {
        $decode = Hashids::decode($allianceUsers)[0];
        $decode2 = Hashids::decode($id)[0];
        $alliance = Alliance::findOrFail($decode2);
        $user = AllianceUsers::findOrFail($decode);

        $tariff_d = $alliance->tariff_d;
        $tariff_n = $alliance->tariff_n;
        //dd($alliance);
        $data = AllianceUsersValues::where('alliance_users_id', $decode)->get()->last();
        //dd($data);
        return view('values.add-values')->with([
            'alliance' => $alliance,
            'user' => $user,
            'tariff_d' => $tariff_d,
            'tariff_n' => $tariff_n,
            'data' => $data ?? [],

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddValues $request, $allianceUsers, $id)
    {
        include_once(app_path() . '/Sms/smscApi.php');
        //dd($_POST);
        $value = new AllianceUsersValues();
        $value->fill($request->all());
        $sum_val = $request->input('alliance_users_payment_total');
        if ($request->input('alliance_users_payment_total') == $request->input('alliance_users_payment_total_debt')) {
            $value->alliance_users_debt = 0;
            $value->alliance_users_profit = 0;
            $value->alliance_users_payment_is_paid = 1;

            $user = AllianceUsers::where('id', $request->input('alliance_users_id'))->get()->first();
            $date = Carbon::parse($request->alliance_users_date_check)->format('m-Y');
            Mail::send('emails.payment_wb', ['user' => $user, 'date' => $date, 'request' => $request], function ($m) use ($user, $date) {
                $m->from('info@kkazydub.site', 'Your Application');

                $m->to($user->alliance_user_email)->subject('Отлата за электричество от ' . $date);

            });
            $tel = '+380'.$user->alliance_user_tel;
            $message = "C/T Jalynka splata za electroenergiju ".$date." Suma ".$sum_val."UAH"."Detal'nishe za adresoju https://it-hause.com/login";
            //dd($tel,$message);
           send_sms($tel, $message);
            //dd(send_sms($tel,$message));

        } else if (($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total')) > 0) {

            $newValue = AllianceUsersValues::where('alliance_users_id', $request->input('alliance_users_id'))->where('alliance_users_debt', '!=', 0)->first();
            $delta = ($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total'));
            if (is_null($newValue)) {
                $value->alliance_users_debt = 0;
                $value->alliance_users_profit = round($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total'),2);
                $value->alliance_users_payment_is_paid = 1;

                $user = AllianceUsers::where('id', $request->input('alliance_users_id'))->get()->first();
                $date = Carbon::parse($request->alliance_users_date_check)->format('d-m-Y');
                Mail::send('emails.payment_overpay', ['user' => $user, 'date' => $date, 'request' => $request], function ($m) use ($user, $date) {
                    $m->from('info@kkazydub.site', 'Your Application');

                    $m->to($user->alliance_user_email)->subject('Отлата за электричество от ' . $date);
                });
            } else {
                $value->alliance_users_payment_total_debt = $request->input('alliance_users_payment_total');
                $value->alliance_users_debt = 0;
                $value->alliance_users_profit = 0;
                $value->alliance_users_payment_is_paid = 1;
                $value->save();

                $newValue->alliance_users_payment_total_debt = $newValue->alliance_users_payment_total_debt + $delta;
                $newValue->alliance_users_debt = $newValue->alliance_users_debt + $delta;
                $newValue->alliance_users_payment_is_paid = 0;
                $newValue->update();
            }
        } else if (($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total')) < 0) {
            $value->alliance_users_debt = ($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total'));
            $value->alliance_users_profit = 0;
            $value->alliance_users_payment_is_paid = 0;

            $user = AllianceUsers::where('id', $request->input('alliance_users_id'))->get()->first();
            $date = Carbon::parse($request->alliance_users_date_check)->format('d-m-Y');
            Mail::send('emails.payment_debt', ['user' => $user, 'date' => $date, 'request' => $request], function ($m) use ($user, $date) {
                $m->from('info@kkazydub.site', 'Your Application');

                $m->to($user->alliance_user_email)->subject('Отлата за электричество от ' . $date);
            });

            $debt = ($request->input('alliance_users_payment_total_debt') - $request->input('alliance_users_payment_total'));
            $tel = '+380'.$user->alliance_user_tel;
            $dateM = Carbon::now()->format('m-Y');
            $message = "C/T Jalynka splata za electroenergiju ".$dateM." Borg ".$debt."UAH "."Detal'nishe za adresoju https://it-hause.com/login";
            //dd($tel,$message);
            send_sms($tel, $message);
        }

        //dd($q);
        $value->save();


        return redirect('/members/' . $allianceUsers . '/values/' . $id)->with('success', 'Данные добавлены!');
    }


    public function paydebt(Request $request, $id, $allianceUsers, $allianceUsersValues)
    {
        $decode1 = Hashids::decode($id);
        $decode2 = Hashids::decode($allianceUsers);
        $decode3 = Hashids::decode($allianceUsersValues);

        $alliance = Alliance::find($decode1);
        $user = AllianceUsers::find($decode2);
        $usersValue = AllianceUsersValues::find($decode3)->first();


        if ($usersValue->alliance_users_payment_total_debt == $request->input('alliance_users_payment_total')) {
            $usersValue->alliance_users_payment_total_debt = $request->input('alliance_users_payment_total');
            $usersValue->alliance_users_debt = 0;
            $usersValue->alliance_users_payment_is_paid = 1;
            $usersValue->save();

            Mail::send('emails.payCash', ['user' => $user, 'usersValue' => $usersValue, 'request' => $request], function ($m) use ($user) {
                $m->from('info@kkazydub.site', 'Your Application');

                $m->to($user[0]->alliance_user_email)->subject('Оплата задолжености по Элетричеству');
            });

            return redirect('/members/' . $id . '/values/' . $allianceUsers)->with([
                'alliance' => $alliance,
                'user' => $user,
            ]);
        } elseif ($usersValue->alliance_users_payment_total_debt > $request->input('alliance_users_payment_total')) {
            $usersValue->alliance_users_payment_total_debt = $usersValue->alliance_users_payment_total_debt + $request->input('alliance_users_payment_total');
            $usersValue->alliance_users_debt = $usersValue->alliance_users_debt + $request->input('alliance_users_payment_total');
            $usersValue->alliance_users_payment_is_paid = 0;
            $usersValue->update();
            return redirect('/members/' . $id . '/values/' . $allianceUsers)->with('success', 'Все еще долг');
        } elseif ($usersValue->alliance_users_payment_total_debt < $request->input('alliance_users_payment_total')) {
            $delta = $request->input('alliance_users_payment_total') + $usersValue->alliance_users_debt;

            $usersValue->alliance_users_payment_total_debt = $usersValue->alliance_users_payment_total;
            $usersValue->alliance_users_debt = 0;
            $usersValue->alliance_users_payment_is_paid = 1;
            $usersValue->update();

            $newUsersValue = AllianceUsersValues::where('alliance_users_id', $decode2)->where('alliance_users_debt', '!=', 0)->first();

            if (!is_null($newUsersValue)) {
                $newUsersValue->alliance_users_payment_total_debt = $newUsersValue->alliance_users_payment_total_debt + $delta;
                $newUsersValue->alliance_users_debt = $newUsersValue->alliance_users_debt + $delta;
                $newUsersValue->alliance_users_payment_is_paid = 0;
                $newUsersValue->update();
            } else {
                $usersValue->alliance_users_profit = round($delta,2);
                $usersValue->update();
            }

            return redirect('/members/' . $id . '/values/' . $allianceUsers)->with('success', 'Переплата');
        }

    }

    // як зробити:
    // данний платіж меньше необхідного
    // у користувача э профіт, за рахунок переплати по попередніх сплатах
    // як списати профіт(з кожного платежу, який був з профітом) зробивши наявний борг меньше
}
