<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\Spending;
use Hashids;
use App\Http\Requests\SpendingValidation;

class SpendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $spendings = Spending::where('alliance_id',$decode)->get();
        //dd($alliance);
        return view('alliance.spending.index')->with(compact('alliance','spendings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpendingValidation $request)
    {
        $value =  new Spending();
        $value->fill($request->all(),true)->save();
        return redirect()->back()->with('success','Потрачено!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Spendings  $spendings
     * @return \Illuminate\Http\Response
     */
    public function show(Spendings $spendings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Spendings  $spendings
     * @return \Illuminate\Http\Response
     */
    public function edit(Spendings $spendings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Spendings  $spendings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spendings $spendings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Spendings  $spendings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spendings $spendings)
    {
        //
    }
}
