<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceCounterValues;
use App\AllianceUsers;
use App\AllianceUsersValues;
use App\User;
use DB;
use App\Spending;
use Illuminate\Http\Request;
use Hashids;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Carbon;

class ReportsController extends Controller
{
    public function index($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
		
        $previous_month = AllianceCounterValues::where('alliance_id',$decode)
            ->orderBy('alliance_counter_date_check', 'DESC')
            ->get()
            ->first();

        $thisYear = AllianceCounterValues::where('alliance_id',$decode)
            ->where('alliance_counter_date_check', '>=', date('Y-01-01'))
            ->get();
        $fees = DB::table('fees')->select('*')
            ->leftJoin('alliance_users', function ($join) {
                $join->on('alliance_users.id', '=', 'fees.alliance_user_id');
            })
            ->where('fees.alliance_id', $decode)
            ->get();

        $fee_coll = DB::table('fees')->select('*')
            ->leftJoin('alliance_users', function ($join) {
                $join->on('alliance_users.id', '=', 'fees.alliance_user_id');
            })
            ->where('alliance_user_pay_status', '=', 1)
            ->where('fees.alliance_id', $decode)
            ->get();

        $fee_collected = $fee_coll->sum('alliance_user_fee_sum');
        $fee_sum = $fee_collected - $fees->sum('alliance_user_fee_sum');
        //dd($fee_sum);

        $s = Spending::where('alliance_id', $decode)
            ->where('date', '>=', date('Y-15-15'))->get();
        $spent = $s->sum('sum');

        $balance = $fee_collected - $spent;

        $start_date = Carbon::now()->format('Y-m-01');
        $end_date = Carbon::now()->subMonth(1)->format('Y-m-01');

        $consumedKvt = AllianceUsersValues::where('alliance_id', $decode)
            ->whereBetween('alliance_users_date_check', [$end_date, $start_date])->get();

        $consumedKvt_a = AllianceCounterValues::where('alliance_id', $decode)
            ->where('alliance_counter_date_check', '>=', date('Y-01-01'))
            ->orderBy('alliance_counter_date_check', 'DESC')
            ->get()->first();

        $valuesLastMonthAlliance = \DB::table('alliance_counter_values')
            ->select(
                'alliance_counter_payment_total as total',
                'alliance_counter_consumption_d as d',
                'alliance_counter_consumption_n as n')
            ->where('alliance_id', $alliance->id)
            ->where('alliance_counter_date_check', '>=', date('Y-m-15', strtotime("-1 month")))
            ->where('alliance_counter_date_check', '<', date('Y-m-15'))
            ->get()->first();

//        $st_d = Carbon::now()->format('Y-m-15');
//        $end_d = Carbon::now()->addMonth(1)->format('Y-m-15');
//        $valuesLastMonthAlliance = AllianceCounterValues::where('alliance_id', $alliance->id)
//            ->select( 'alliance_counter_payment_total as total',
//                'alliance_counter_consumption_d as d',
//                'alliance_counter_consumption_n as n')
//            ->where('alliance_counter_date_check', '>=', Carbon::now()->subMonth(1)->format('Y-m-15'))
//           ->whereBetween('alliance_counter_date_check', [$st_d, $end_d])
//            ->get()->first();
        //dd($valuesLastMonthAlliance);

        $valuesLastMonthUsers = \DB::table('alliance_users_values')
            ->select(
                \DB::raw('SUM(alliance_users_payment_total) as total'),
                \DB::raw('SUM(alliance_users_consumption_d) as d'),
                \DB::raw('SUM(alliance_users_consumption_n) as n')
            )
            ->where('alliance_id', $alliance->id)
            ->where('alliance_users_date_check', '>=', date('Y-m-15', strtotime("-1 month")))
            ->where('alliance_users_date_check', '<', date('Y-m-15'))
            ->get()->first();
//dd($valuesLastMonthUsers);
        $balance2 = $valuesLastMonthUsers->total - $valuesLastMonthAlliance->total;

        $saldo = ($valuesLastMonthUsers->d + $valuesLastMonthUsers->n) - ($valuesLastMonthAlliance->d + $valuesLastMonthAlliance->n);

        $d = $saldo / ($valuesLastMonthAlliance->d + $valuesLastMonthAlliance->n) * 100;

        //dd($valuesLastMonthAlliance);

        return view('alliance.reports.index')->with([
            'previous_month'=>$previous_month,
            'thisYear'=>$thisYear,
            'fee_collected'=>$fee_collected,
            'fee_sum'=>$fee_sum,
            'spent'=>$spent,
            'balance'=>$balance,
            'consumedKvt'=>$consumedKvt,
            'consumedKvt_a'=>$consumedKvt_a,
            'alliance'=>$alliance,
            'balance2'=>$balance2,
            'saldo'=>$saldo,
            'valuesLastMonthAlliance'=>$valuesLastMonthAlliance,
            'valuesLastMonthUsers'=>$valuesLastMonthUsers,
            'd'=>$d,
        ]);
    }

    public function getAllianceUsers($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $chef = User::where('id', '=', $alliance->alliance_chef_id)->get()->first();
        $users = AllianceUsers::where('alliance_id', $decode)->get();
        //dd($chef);

        return view('alliance.reports.list')->with(
            ['users' => $users,
                'alliance' => $alliance,
                'chef' => $chef]);
    }

    public function getSpending(Request $request, $id)
    {
        $r_date = date($request->date) . '-01-01';
        //dd($r_date.'-01-01');
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $spending = Spending::where('alliance_id', $decode)
            ->where('date', '>=', $r_date)->get();
        return view('alliance.reports.spending')->with(['spending' => $spending, 'alliance' => $alliance]);
    }

    public function lastMonthKvt(Request $request, $id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);

        $start_date = Carbon::now()->format('Y-m-01');
        if (is_null($request->end_date)) {
            $end_date = Carbon::now()->subMonth(1)->format('Y-m-01');
        } else {
            $end_date = Carbon::parse($request->end_date)->format('Y-m-01');
        }

        $consumedKvt = DB::table('alliance_users_values')
            ->select('alliance_users_values.*',
                'alliance_users.alliance_user_name as name',
                'alliance_users.alliance_id as usr_al_id',
                'alliance_users.alliance_user_surname as surname'
            )
            ->join('alliance_users', 'alliance_users.id', '=', 'alliance_users_values.alliance_users_id')
            ->where('alliance_users_values.alliance_id', $decode)
            ->whereBetween('alliance_users_date_check', [$end_date, $start_date])->get();

        return view('alliance.reports.consump')->with(compact('consumedKvt', 'alliance'));

    }
}