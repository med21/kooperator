<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceUsers;
use App\Fee;
use App\Settings;
use App\AllianceUsersValues;
use App\User;
use App\Payment;
use Hashids;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendFeeForm(Request $request)
    {
        $liq_id = Settings::where('alliance_id', '=', $request->input('alliance_id'))->select('liqpay_id')->get()->first()['liqpay_id'];
        $liq_key = Settings::where('alliance_id', '=', $request->input('alliance_id'))->select('liqpay_pr_key')->get()->first()['liqpay_pr_key'];

        $payment = new Payment();

        $payment->alliance_id = $request->input('alliance_id');
        $payment->user_id = $request->input('user_id');
        $payment->amount = $request->input('amount');
        $payment->type = $request->input('type');
        $payment->key = $request->input('order_id');
        $payment->desc = $request->input('description');
        $payment->save();

        $amount = $request->input('amount');
        $description = $request->input('description');
        $date = $request->input('date');
        $key = $payment->id;
        $phone = $request->input('phone');
        $sender_first_name = $request->input('sender_first_name');
        $sender_last_name = $request->input('sender_last_name');
        $email = $request->input('email');

        //dd($_POST);

        $data = base64_encode(json_encode(array(
            'action' => 'pay',
            'public_key' => $liq_id,
            'amount' => $amount,
            'sandbox' => 1,
            'currency' => 'UAH',
            'description' => $description,
            'order_id' => $key,
            'version' => '3',
            'email' => $email,
            'phone' => $phone,
            'date' => $date,
            'sender_first_name' => $sender_first_name,
            'sender_last_name' => $sender_last_name,
            'result_url' => 'https://it-hause.com/payment/liqpay/response/userFeeLiqpay',
        )));

        dd(base64_decode($data));

        $signature = base64_encode(sha1($liq_key . $data . $liq_key, 1));

        return view('values.liqpay-form')->with(compact('data', 'signature'));

    }

    public function sendForm(Request $request)
    {
        $liq_id = Settings::where('alliance_id', '=', $request->input('alliance_id'))->select('liqpay_id')->get()->first()['liqpay_id'];
        $liq_key = Settings::where('alliance_id', '=', $request->input('alliance_id'))->select('liqpay_pr_key')->get()->first()['liqpay_pr_key'];

        $payment = Payment::where([
            'alliance_id' => $request->input('alliance_id'),
            'user_id' => $request->input('user_id'),
            'type' => $request->input('type'),
            'key' => $request->input('order_id'),
            'amount' => $request->input('amount'),
            'status' => 'failure',
        ])->get()->first();
        if (is_null($payment)) {
            $payment = new Payment();
            $payment->alliance_id = $request->input('alliance_id');
            $payment->user_id = $request->input('user_id');
            $payment->amount = $request->input('amount');
            $payment->type = $request->input('type');
            $payment->key = $request->input('order_id');
            $payment->desc = $request->input('description');
        } else {
            $payment->status = 'processing';
            $payment->result = null;
        }
        $payment->order_id = bin2hex(random_bytes(32));
        $payment->save();


        $amount = $request->input('amount');
        $description = $request->input('description');
        $date = $request->input('date');
        $key = $payment->order_id;
        $phone = $request->input('phone');
        $sender_first_name = $request->input('sender_first_name');
        $sender_last_name = $request->input('sender_last_name');
        $email = $request->input('email');


        $data = base64_encode(json_encode(array(
            'action' => 'pay',
            'public_key' => $liq_id,
            'amount' => $amount,
            'sandbox' => 1,
            'currency' => 'UAH',
            'description' => $description,
            'order_id' => $key,
            'version' => '3',
            'email' => $email,
            'phone' => $phone,
            'date' => $date,
            'sender_first_name' => $sender_first_name,
            'sender_last_name' => $sender_last_name,
            'result_url' => 'https://it-hause.com/payment/liqpay/response',
        )));

        //dd(base64_decode($data));

        $signature = base64_encode(sha1($liq_key . $data . $liq_key, 1));

        return view('values.liqpay-form')->with(compact('data', 'signature'));

    }

    public function sendAccForm(Request $request)
    {
        $liq_id = Settings::where('id', 777)->first()['liqpay_id'];
        $liq_key = Settings::where('id', 777)->first()['liqpay_pr_key'];

        $payment = Payment::where([
            'alliance_id' => $request->input('alliance_id'),
            'user_id' => $request->input('user_id'),
            'type' => $request->input('type'),
            'key' => $request->input('order_id'),
            'amount' => $request->input('amount'),
            'status' => 'failure',
        ])->get()->first();
        if (is_null($payment)) {
            $payment = new Payment();
            $payment->alliance_id = $request->input('alliance_id');
            $payment->user_id = $request->input('user_id');
            $payment->amount = $request->input('amount');
            $payment->type = $request->input('type');
            $payment->key = $request->input('order_id');
            $payment->desc = $request->input('description');
        } else {
            $payment->status = 'processing';
            $payment->result = null;
        }
        $payment->order_id = bin2hex(random_bytes(32));
        $payment->save();

        $amount = $request->input('amount');
        $description = $request->input('description');
        $date = $request->input('date');
        $key = $payment->order_id;
        $phone = $request->input('phone');
        $sender_first_name = $request->input('sender_first_name');
        $sender_last_name = $request->input('sender_last_name');
        $email = $request->input('email');

        //dd($_POST);

        $order_id = User::where('id', '=', $payment->user_id)->get()->first();
        $order_id->order_id = $payment->order_id;
        $order_id->update();

        $data = base64_encode(json_encode(array(
            'action' => 'pay',
            'public_key' => $liq_id,
            'amount' => $amount,
            'sandbox' => 0,
            'currency' => 'UAH',
            'description' => $description,
            'order_id' => $key,
            'version' => '3',
            'email' => $email,
            'phone' => $phone,
            'date' => $date,
            'sender_first_name' => $sender_first_name,
            'sender_last_name' => $sender_last_name,
            'result_url' => 'https://it-hause.com/payment/liqpay/response/acc',
        )));

        //dd(base64_decode($data));

        $signature = base64_encode(sha1($liq_key . $data . $liq_key, 1));
        return view('values.liqpay-form')->with(compact('data', 'signature'));

    }

    public function showLiqpay(Request $request)
    {

        $incoming_signature = $request->signature;
        $data = $request->data;
        $response = json_decode(base64_decode($data));
        $j = Payment::where('order_id', $response->order_id)->get()[0]->alliance_id;
        $liqK = Settings::find($j)->liqpay_pr_key;
        $liqpay_signature = $liqK;
        $signature = base64_encode(sha1($liqpay_signature . $data . $liqpay_signature, 1));

        //dd($response);
        if ($incoming_signature == $signature) {
            if ($response->status !== 'failure') {
                if ($response->status == 'sandbox') {
                    $p = Payment::where('order_id', $response->order_id)->get()->first();
                    if ($response->amount == 350) {

                        $d = Fee::where('id', '=', $p->order_id)->get()->first();
                        $id = Alliance::where('id', $d->alliance_id)->get()->first()['id'];
                        //dd($id);
                        $d->alliance_user_pay_date = Carbon::now()->format('Y-m-d');
                        $d->alliance_user_pay_status = 1;
                        $d->update();

                        $user = AllianceUsers::findOrFail($d->alliance_user_id);

                        Mail::send('emails.payment_ok', ['user' => $user, 'response' => $response], function ($m) use ($user, $response) {
                            $m->from('info@it-hause.com', 'КооператоR');

                            $m->to($user->alliance_user_email)->subject('Оплата через LiqPay!');
                        });
                        return redirect('alliance/' . Hashids::encode($id) . '/dashboard/index/')->with('success', 'Оплата взносов прошла успешно!');
                    } else {

                        $p_s = Payment::where('order_id', $response->order_id)->get()->first();
                        $p_s->result = "ok";
                        $p_s->status = $response->status;
                        $p_s->date = \Carbon\Carbon::now();
                        $p_s->update();

                        $d = AllianceUsersValues::find($p_s->key);
                        $d->alliance_users_payment_total_debt = $p_s->amount;
                        $d->alliance_users_debt = 0;
                        $d->alliance_users_profit = 0;
                        $d->alliance_users_payment_is_paid = 1;
                        $d->update();

                        $user = AllianceUsers::findOrFail($p_s->user_id);
                        Mail::send('emails.payment_ok', ['user' => $user, 'response' => $response], function ($m) use ($user, $response) {
                            $m->from('info@it-hause.com', 'КооператоR');

                            $m->to($user->alliance_user_email)->subject('Оплата через LiqPay!');
                        });

                        return redirect('/members/' . Hashids::encode($d->alliance_id) . '/values/' . Hashids::encode($d->alliance_users_id))->with('success', 'Оплата прошла успешно!');
                    }
                }
            } else {
                $p = Payment::where('order_id', $response->order_id)->get()->first();

                $p->result = "error";
                $p->status = $response->status;
                $p->date = \Carbon\Carbon::now();
                $p->update();
            }
            return redirect('/members/' . Hashids::encode($p->alliance_id) . '/values/' . Hashids::encode($p->user_id))->withErrors(['Оплата не успешна! Попробуйте еще раз через пару минут']);
        }

        return redirect('/')->withErrors(['Оплата не успешна! Попробуйте еще раз через пару минут']);
    }

    public function accPayment(Request $request)
    {
        $incoming_signature = $request->signature;
        $data = $request->data;
        $response = json_decode(base64_decode($data));
        $liqpay_signature = Settings::find(777)->liqpay_pr_key;
        $signature = base64_encode(sha1($liqpay_signature . $data . $liqpay_signature, 1));

        if ($incoming_signature == $signature) {
            if ($response->status == 'try_again') {
                $p = Payment::where('order_id', $response->order_id)->get()->first();
                $p->result = 'error';
                $p->status = $response->status;
                $p->date = \Carbon\Carbon::now();
                $p->update();

                return redirect('/home')->withErrors(['Error!'=>'Оплата не прошла, попитайтесь повторить оплату позже.']);

            } elseif ($response->status !== 'failure') {
                $p = Payment::where('order_id', $response->order_id)->get()->first();
                $p->result = 'ok';
                $p->status = $response->status;
                $p->date = \Carbon\Carbon::now();
                $p->update();

                $d = User::where('id', '=', $p->user_id)->get()->first();
                $id = Alliance::where('alliance_chef_id', $d->id)->get()->first()['id'];
                $d->end_date = Carbon::parse($d->end_date)->addYear(1)->format('Y-m-d');
                $d->status = 1;
                $d->update();

                $user = User::findOrFail($d->id);
                Mail::send('emails.year_acc', ['user' => $user, 'response' => $response], function ($m) use ($user, $response) {
                    $m->from('info@it-hause.com', 'КооператоR');

                    $m->to($user->email)->subject('Оплата годового доступа через LiqPay!');
                    $m->bcc('kkazydub@gmail.com')->subject('Оплата годового доступа через LiqPay!');
                });
                return redirect('alliance/' . Hashids::encode($id) . '/dashboard/index/')->with('success', 'Оплата прошла успешно!');
            } else {
                return redirect('/home')->withErrors(['Error!']);
            }
        }
    }

    public function sendFormPb(Request $request)
    {
        $pb = Settings::where('alliance_id', 777)->get()->first();
//
//
//        $file = simplexml_load_file('test/pb.xml');
//
//
//        $file->data->test = 1;
//        $file->payment->prop->b_card_or_acc = 5168742215775291;
//        $file->payment->prop->order_id = bin2hex(random_bytes(32));
//        $file->payment->prop->amt = 100;
//        $file->payment->prop->ccy = 'UAH';
//        $file->payment->prop->details = 'TestP';
//        $sign = sha1(md5($file->data . $pb->privat24_pk_key));
//        $file->merchant->id = $pb->privat24_id;
//        $file->merchant->signature = $sign;


        $p_id = 22;
        $data =
            '<oper>cmt</oper>' .
            '<wait>90</wait>' .
            '<test>0</test>' .
            '<payment id="' . $p_id . '">' .
            '<prop name="b_card_or_acc" value="5168742215775291" />' .
            '<prop name="amt" value="' . $request->input('amt') . '" />' .
            '<prop name="ccy" value="UAH" />' .
            '<prop name="details" value="' . $request->input('details') . '" />' .
            '</payment>';
        $sign = sha1(md5($data . $pb->privat24_pk_key));

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<request version="1.0">' .
            '<merchant>' .
            '<id>' . $pb->privat24_id . '</id>' .
            '<signature>' . $sign . '</signature>' .
            '</merchant>' .
            '<data>' .
            '<oper>cmt</oper>' .
            '<wait>90</wait>' .
            '<test>1</test>' .
            '<payment id="' . $p_id . '">' .
            '<prop name="b_card_or_acc" value="5168742215775291" />' .
            '<prop name="amt" value="' . $request->input('amt') . '" />' .
            '<prop name="ccy" value="UAH" />' .
            '<prop name="details" value="' . $request->input('details') . '" />' .
            '</payment>' .
            '</data>' .
            '</request>';

        //dd(simplexml_load_string($xml));

        //return view('values.pb-form')->with('xml', $xml);

        $url = 'https://api.privatbank.ua/p24api/pay_pb';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($ch);
        curl_close($ch);

        return dd($result);

    }

    public function showPrivat24(Request $request)
    {

        $payment = $request->payment;
        $signature = $request->signature;

        $pass = Settings::find(777)->privat24_pr_key;

        $checkSignature = sha1(md5($payment . $pass));

        if ($signature == $checkSignature) {

            // Ответ от настоящего сервера
            //echo ("Опа! проверка прошла успешно");

            // Далее парсим $payment
            parse_str($payment, $data);
            $id_cart = $data['order'];
            if ($data['state'] == 'test' || $data['state'] == 'ok') {

                $order = Purchase::where('code', $id_cart)->first();
                $order->update(['status' => 'paid']);

                return redirect('/');
            }
        }

        return redirect('/');
    }
}
