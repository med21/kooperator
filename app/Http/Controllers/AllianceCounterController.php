<?php

namespace App\Http\Controllers;

use App\AllianceCounter;
use App\Alliance;
use App\Http\Requests\AllianceCounterValidation;
use Hashids;
use Illuminate\Http\Request;

class AllianceCounterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $decode = Hashids::decode($id)[0];
        //dd($decode);
        $alliance= Alliance::find($decode);
        return view('alliance.counter.create')->with([
            'alliance'=>$alliance,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllianceCounterValidation $request, $id)
    {
        $data = new AllianceCounter();
        //dd($request->all());
        $data->fill($request->all(),true)->save();

        if (session('t', 0) == 1) {
            return redirect('/alliance/' . $id . '/order/submission');
        } else if (session('t', 0) == 2) {
            return redirect('/alliance/' . $id . '/order/submission');
        } else {
            return redirect('/alliance/'.$id.'/dashboard/index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllianceCounter  $allianceCounter
     * @return \Illuminate\Http\Response
     */
    public function show($id, $allianceCounter)
    {
        $decode1= Hashids::decode($id);
        $decode2= Hashids::decode($allianceCounter);

        $alliance = Alliance::find($decode1)->first;
        $counter = AllianceCounter::find($decode2)->first();

        return view('alliance.counter.show')->with([
            'alliance'=>$alliance,
            'counter' =>$counter,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllianceCounter  $allianceCounter
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $decode1= Hashids::decode($id)[0];

        $alliance = Alliance::find($decode1)->first();
        $counter = AllianceCounter::where('alliance_id', $decode1)->first();

        //dd($counter);

        return view('alliance.counter.edit')->with([
            'alliance'=>$alliance,
            'counter' =>$counter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AllianceCounter  $allianceCounter
     * @return \Illuminate\Http\Response
     */
    public function update($id, $allianceCounter, $request)
    {
        $decode1= Hashids::decode($id);
        $decode2= Hashids::decode($allianceCounter);

        $alliance = Alliance::find($decode1)->first;
        $counter = AllianceCounter::find($decode2)->first();

        $counter->fill($request->all(),true)->update();

        return redirect('/alliance/'.$id.'/counter/show')->with([
            'alliance'=>$alliance,
            'counter' =>$counter,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllianceCounter  $allianceCounter
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllianceCounter $allianceCounter)
    {
        //
    }
}
