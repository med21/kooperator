<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceUsers;
use App\User;
use Carbon\Carbon;
use Hashids;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    public function index($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $chef = User::where('id', $alliance->alliance_chef_id)->get()->first();

        $end = Carbon::parse($chef->end_date);
        $now = Carbon::now();
        if ($end >= $now) {
            $length = $end->diffInDays($now);
        } else {
             $length = "Оплаченый срок пользования истек";
        }

        return view('submission')->with(compact('alliance', 'chef', 'length'));
    }

    public function renew($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::find($decode);
        $chef = User::where('id', $alliance->alliance_chef_id)->get()->first();
        $users = AllianceUsers::where('alliance_id',$decode)->get()->count();

        if($users <= 50) {
            $price = 3578;
        }
        elseif(51 < $users && $users >100 ){
            $price = $users * 12 * 7;
        }
        elseif(101< $users){
            $price = $users *12 *6.5;
        }
        else{
            $price = $users*12*6.1;
        }
        $end = Carbon::parse($chef->end_date);
        $now = Carbon::now();
        if ($end > $now) {
            $length = $end->diffInDays($now);
        } else {
            $length =  "Оплаченый срок пользования истек";
        }

        return view('sub_renew')->with(compact('alliance', 'chef', 'length', 'users', 'price'));
    }
}
