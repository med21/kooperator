<?php

namespace App\Http\Controllers;

use App\AllianceUsers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use App\Notifications\NewUserRegisteredAdmin;
use App\Notifications\TestNotification;
use Illuminate\Support\Facades\Auth;
use Notification;
use DB;

class HomeController extends Controller
{
    use Notifiable;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function testEmail()
    {
        Notification::route('mail', 'kkazydub@yahoo.com')->notify(new NewUserRegisteredAdmin());
        return redirect()->back();
    }

    public function notiff()
    {
        $user_ids = DB::table('alliance_users')->select('id')
            ->where('alliance_id',1)
            ->pluck('id','id');

        $query = DB::table('alliance_users')->select(DB::raw('GROUP_CONCAT(alliance_users.id) as ids'))->where('alliance_id','=',1)->get()->first();
        $user_ids = is_null($query) ? '' : $query->ids;

        dd($user_ids);
    }
}
