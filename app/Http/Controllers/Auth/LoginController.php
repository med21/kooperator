<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();

        //dd($user->user['name']['givenName']);
        $l_user = User::where('email', '=', $user->email)->get()->first();
        if (!is_null($l_user)) {
            if ($l_user->google_id == $user->id) {
                \Auth::login($l_user);
                return redirect('/home');
            } elseif ($l_user->google_id == null) {
                $l_user->google_id = $user->id;
                $l_user->update();
                \Auth::login($l_user);
                return redirect('/home');
            }
        } else {
            $n_user = new User();

            $n_user->name = $user->user['name']['givenName'];
            $n_user->surname = $user->user['name']['familyName'];
            $n_user->email = $user->email;
            $n_user->google_id = $user->id;
            $n_user->password = bcrypt($user->email);
            $n_user->member_id = null;
            $n_user->status = 0;
            $n_user->end_date = Carbon::now()->addMonth(2)->format('Y-m-d');
            $n_user->save();

            $n_user->attachRole('2');
            \Auth::login($n_user);
            return redirect('/home')->with('success', 'Вы создали аккаунт, осталось еще чуть-чуть! Пароль для входа - ваш email!');
        }
    }


    function fbredirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public
    function fbhandleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        //dd($user);
        $pieces = explode(" ", $user->name);
        $l_user = User::where('email', '=', $user->email)->get()->first();
        //dd($l_user);
        if (!is_null($l_user)) {
            if ($l_user->facebook_id == $user->id) {
                \Auth::login($l_user);
                return redirect('/home');
            } elseif ($l_user->facebook_id == null) {
                $l_user->facebook_id = $user->id;
                $l_user->update();
                \Auth::login($l_user);
                return redirect('/home');
            }
        } else {
            $n_user = new User();

            $n_user->name = $pieces[0];
            $n_user->surname = $pieces[1];
            $n_user->email = $user->email;
            $n_user->facebook_id = $user->id;
            $n_user->password = bcrypt($user->email);
            $n_user->member_id = null;
            $n_user->status = 0;
            $n_user->end_date = Carbon::now()->addMonth(2)->format('Y-m-d');
            $n_user->save();

            $n_user->attachRole('2');
            \Auth::login($n_user);
            return redirect('/home')->with('success', 'Вы создали аккаунт, осталось еще чуть-чуть! Пароль для входа - ваш email!');
        }
    }

    public
    function twredirectToProvider()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public
    function twhandleProviderCallback()
    {
        $user = Socialite::driver('twitter')->user();
        //dd($user);
        $pieces = explode(" ", $user->name);
        $l_user = User::where('email', '=', $user->email)->get()->first();
        if (!is_null($l_user)) {
            if ($l_user->twitter_id == $user->id) {
                \Auth::login($l_user);
                return redirect('/home');
            } elseif ($l_user->twitter_id == null) {
                $l_user->twitter_id = $user->id;
                $l_user->update();
                \Auth::login($l_user);
                return redirect('/home');
            }
        } else {
            $n_user = new User();

            $n_user->name = $pieces[0];
            $n_user->surname = $pieces[1];
            $n_user->email = $user->email;
            $n_user->twitter_id = $user->id;
            $n_user->password = bcrypt($user->email);
            $n_user->member_id = null;
            $n_user->status = 0;
            $n_user->end_date = Carbon::now()->addMonth(2)->format('Y-m-d');
            $n_user->save();

            $n_user->attachRole('2');
            \Auth::login($n_user);
            return redirect('/home')->with('success', 'Вы создали аккаунт, осталось еще чуть-чуть! Пароль для входа - ваш email!');
        }
    }
}
