<?php

namespace App\Http\Controllers;

use App\Alliance;
use App\AllianceChef;
use App\Http\Requests\AllianceValidation;
use App\User;
use Hashids;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class AllianceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasRole('superadmin')) {
            $alliance = AllianceChef::where('user_id', Auth::id())->first();
            if (is_null($alliance)) {
                return redirect()->to('/alliance/create')->with('alliance',$alliance);
            } else {
                return redirect()->to('/alliance/' . Hashids::encode($alliance->alliance_id).'/dashboard/index');
            }
        }
        $alliance = DB::table('alliances')
            ->join('users', 'alliances.alliance_chef_id', '=', 'users.id')
            ->select('alliances.id','alliance_name','alliance_state','alliance_region','alliance_street','alliance_edrpou','users.name')
            ->get();
       //dd($alliance);
        return view('alliance.index')->with('alliance',$alliance);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chef = User::all()->filter(function($user) {
            return $user->hasRole('alliance_chef');
        });
        $alliance = AllianceChef::where('user_id', Auth::id())->first();
        return view('alliance.create')->with(['chef'=> $chef,
            'alliance'=>$alliance]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllianceValidation $request)
    {
        $alliance = new Alliance();
        $alliance->fill($request->all(),true)->save();

        $chef = new AllianceChef();
        $chef->alliance_id = $alliance->id;
        $chef->user_id = $request->input('alliance_chef_id');
        $chef->save();

        return redirect('/alliance')->with('success', 'Кооператив добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alliance  $alliance
     * @return \Illuminate\Http\Response
     */
    public function show( $alliance)
    {
        $id = Hashids::decode($alliance)[0];
        //dd($id);
        return view('alliance.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alliance  $alliance
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $decode = Hashids::decode($id)[0];
        $data = Alliance::find($decode);
        $c = AllianceChef::where('alliance_id',$decode)->select('user_id')->get();
        $chef = User::find($c);
        //dd($chef2);
        return view('alliance.edit')->with(['data'=>$data,
        'chef'=>$chef]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alliance  $alliance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $decode = Hashids::decode($id)[0];
            $data = Alliance::find($decode)->first();

            $data->fill($request->all(),true)->update();
            //dd($request->all());
            return redirect("alliance")->with('success', 'Кооператив обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alliance  $alliance
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decode = Hashids::decode($id)[0];
        $alliance = Alliance::findOrFail($decode);
        $alliance->delete();
        return redirect("alliance")->with('success', 'Кооператив удален!');
    }
}
