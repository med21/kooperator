<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Hashids;
use Carbon\Carbon;
use App\Alliance;

class CheckAccountStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     */
    public function handle($request, Closure $next)
    {
        $user = Auth()->user();
        $id = Alliance::where('alliance_chef_id',$user->id)->get()->first();

        $end = Carbon::parse($user->end_date);
        $now = Carbon::now();
        $length_renew = $end <= $now;

        //dd($id);
        if($user->hasRole('superadmin')){
            return $next($request);
        }elseif (is_null($user->member_id)  && $user->status == '0' && is_null($user->end_date)){
            return redirect('/alliance/'.Hashids::encode(Auth()->user()->allianceID()).'/order/submission');
        }
        elseif(is_null($user->member_id)  && $user->status = '0' && $length_renew ){
            return redirect()->action('SubmissionController@index',Hashids::encode($id->id));
        }elseif (is_null($user->member_id) && $user->status = '1' &&  $length_renew){
            return redirect()->action('SubmissionController@renew',Hashids::encode($id->id));
        }
        return $next($request);
    }
}
