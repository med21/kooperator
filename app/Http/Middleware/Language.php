<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Language
{
    public function handle($request, Closure $next)
    {
        $langArr = array("ru", "uk");
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        } else {
            $languages[0] = "en";
        }
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        } else {
            if (in_array($languages[0], $langArr))
                App::setLocale($languages[0]);
        }
        return $next($request);
    }


//    public function handle($request, Closure $next)
//    {
//        if (strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']) == 'uk') {
//            \App::setLocale('uk');
//        } else {
//            \App::setLocale('ru');
//        }
//        return $next($request);
//    }


//    public function handle($request, Closure $next)
//    {
//        if (Session::has('applocale') AND array_key_exists(Session::get('applocale'), Config::get('languages'))) {
//            App::setLocale(Session::get('applocale'));
//        }
//        else { // This is optional as Laravel will automatically set the fallback language if there is none specified
//            App::setLocale(Config::get('app.fallback_locale'));
//        }
//        return $next($request);
//    }
}