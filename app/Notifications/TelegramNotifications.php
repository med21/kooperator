<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class TelegramNotifications extends Notification
{
    public function __construct($user, $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable)
    {
        $url = url('/');

        return TelegramMessage::create()
            ->to($this->user->alliance_user_telegram_id)// Optional.
            ->content($this->message)// Markdown supported.
            ->button('Visit site', $url); // Inline Button
    }
}
