<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceCounterValues extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'alliance_counter_id',
            'alliance_counter_val_d',
            'alliance_counter_val_n',
            'alliance_counter_consumption_d',
            'alliance_counter_consumption_n',
            'alliance_counter_date_check',
            'alliance_counter_date_interval',
            'alliance_counter_payment_d',
            'alliance_counter_payment_n',
            'alliance_counter_payment_total',
            'alliance_counter_payment_total_debt',
            'alliance_counter_paid_gov',
            'alliance_counter_payment_is_paid',
        ];
}
