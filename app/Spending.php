<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spending extends Model
{
    protected $fillable =
        [
            'alliance_id',
            'propose',
            'sum',
            'desc',
            'date',
        ];
}
