@extends('layouts.master')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">

    <div class="col-lg-12">
            <table class="dataTable" id="users" class="display nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>ПІБ</th>
                    <th>№ Ділянкі</th>
                    <th>Площа ділянки (га)</th>
                    <th>Домашня адреса</th>
                    <th>Телефон</th>
                    <th>№ пенсійного посвідчення або іншій документ</th>
                    <th>ІНН</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->alliance_user_surname}} {{$user->alliance_user_name}} {{$user->alliance_user_middlename}}</td>
                        <td>{{$user->alliance_user_address}}</td>
                        <td>{{$user->alliance_users_land_square * 0.05}}</td>
                        <td>{{$user->alliance_users_living_address}}</td>
                        <td>+380{{$user->alliance_user_tel}}</td>
                        <td>@if($user->alliance_users_pensioner == 1)
                                {{$user->alliance_users_pensioner_number}}
                            @elseif($user->alliance_users_pensioner == 0)
                                ------
                            @endif
                        </td>
                        <td>{{$user->inn}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                </tr>
                </tfoot>
                <legend>Голова {{$alliance->name}}, {{$chef->surname}} {{$chef->name}} {{$chef->middle_name}}</legend>
            </table>
        </div>
    </div>
        </div>
    </div>
@endsection
@section('scriptCoOp')
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script language="javascript">
        $(document).ready(function() {
            $('#users').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    { extend: 'excelHtml5', text: 'Скачать в формате XLS', title: 'Список членов кооператива' }

                ]
            } );
        } );
    </script>
@endsection
