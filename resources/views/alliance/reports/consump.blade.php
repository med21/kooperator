@extends('layouts.master')
@section('content')
    <div class="text-center">
    <span class="h2">Абоненты оплатившие в прошлом месяце</span></div>
    <table class="table table-striped" id="cons">
        <thead>
        <tr>
            <th></th>
            <th>Потребление день</th>
            <th>Потребление ночь</th>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
        @foreach($consumedKvt as $c)
            <tr>
                <td>{{$c->surname}} {{$c->name}}</td>
                <td>{{$c->alliance_users_consumption_d}}</td>
                <td>{{$c->alliance_users_consumption_n}}</td>
                <td>{{$c->alliance_users_payment_total_debt}}</td>
            </tr>
        @endforeach
        </tbody>
        <thead>
        <tr>
            <td>{{$consumedKvt->count('alliance_users_id')}}</td>
            <td>{{$consumedKvt->sum('alliance_users_consumption_d')}}</td>
            <td>{{$consumedKvt->sum('alliance_users_consumption_n')}}</td>
            <td>{{$consumedKvt->sum('alliance_users_payment_total_debt')}}</td>
        </tr>
        </thead>
    </table>
@endsection
@section('scriptCoOp')
    <script language="javascript">
        $(document).ready(function () {
            $('#cons').DataTable({
                "pageLength": 35,
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                },
            })
        });
    </script>

@endsection
