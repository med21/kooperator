@extends('layouts.master')
@section('content')
    {{--<div class="col-lg-4">--}}
    {{--<div class="panel">--}}
    {{--<div class="panel-heading">--}}
    {{--<h3>@lang('reports.balance')</h3>--}}
    {{--</div>--}}
    {{--<div class="panel-body">--}}
    {{--@lang('reports.balance_last_m') {{lastMonth($previous_month)}}</li>--}}
    {{--@lang('reports.balance_y') {{round(thisYear($thisYear),2)}}--}}

    {{--</div>--}}
    {{--<a class="btn btn-primary btn-sm center-block"--}}
    {{--href="{{url('members/'.Hashids::encode($previous_month->alliance_id))}}">@lang('reports.more')</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-4">--}}
    {{--<div class="panel">--}}
    {{--<div class="panel-heading">--}}
    {{--<h3>@lang('reports.cashbox')</h3>--}}
    {{--</div>--}}
    {{--<div class="panel-body">--}}
    {{--@lang('reports.cashbox_get') {{$fee_collected}}</li>--}}
    {{--@lang('reports.cashbox_spent') {{$spent}}</li>--}}
    {{--@lang('reports.cashbox_balance') {{$balance}}--}}
    {{--</div>--}}
    {{--<a class="btn btn-primary btn-sm center-block"--}}
    {{--href="{{url('alliance/'.Hashids::encode($previous_month->alliance_id).'/dashboard/spending')}}">@lang('reports.more')</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="col-lg-4">

    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="text-center">@lang('reports.elect')</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <div class="card-body">
                                <h4 class="card-header"> @lang('reports.elect_cons')</h4>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">@lang('reports.day')
                                        <u>{{$consumedKvt->sum('alliance_users_consumption_d') ?? 0}}</u>(кВт)
                                    </li>
                                    <li class="list-group-item"> @lang('reports.night')
                                        <u>{{$consumedKvt->sum('alliance_users_consumption_n') ?? 0}}</u>(кВт)
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <h4 class="card-header">@lang('reports.elect_cons_t_m')</h4>
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> @lang('reports.day')
                                        <u>{{$consumedKvt_a->alliance_counter_consumption_d ?? 0}}</u>(кВт)
                                    </li>
                                    <li class="list-group-item"> @lang('reports.night')
                                        <u>{{$consumedKvt_a->alliance_counter_consumption_n ?? 0}}</u>(кВт)
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <div class="card-body">
                                <h4 class="card-header">@lang('reports.elect_cons_t_y') {{date('Y')}}</h4>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">@lang('reports.day')
                                        {{--<u>{{$consumedKvt_a->sum('alliance_counter_consumption_d')}}</u>(кВт)--}}
                                    </li>
                                    <li class="list-group-item">@lang('reports.night')
                                        {{--<u>{{$consumedKvt_a->sum('alliance_counter_consumption_n')}}</u>(кВт)--}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center"><a class="btn btn-primary btn"
                                            href="{{url('alliance/'.Hashids::encode($previous_month->alliance_id).'/dashboard/reports/lastkvt')}}">@lang('reports.more')</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="text-center">Отчет</h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <div class="card-body">
                                <ul class="list-group list-group-flush"><h4
                                            class="card-header"
                                            style="min-height: 60px;">@lang('reports.central_counter')</h4>
                                    <li class="list-group-item">{{$valuesLastMonthAlliance->total ?? 0}} грн</li>
                                    <li class="list-group-item">{{$valuesLastMonthAlliance->d ?? 0}}</li>
                                    <li class="list-group-item">{{$valuesLastMonthAlliance->n ?? 0}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <div class="card-body">
                                <ul class="list-group list-group-flush"><h4
                                            class="card-header"
                                            style="min-height: 60px;">@lang('reports.users_counters')</h4>
                                    <li class="list-group-item">{{$valuesLastMonthUsers->total ?? 0}} грн</li>
                                    <li class="list-group-item">{{$valuesLastMonthUsers->d ?? 0}}</li>
                                    <li class="list-group-item">{{$valuesLastMonthUsers->n ?? 0}}</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center">
                            <div class="card-body">
                                <ul class="list-group list-group-flush"><h4
                                            class="card-header"
                                            style="min-height: 60px; padding-top: 6%;">@lang('reports.debet')</h4>
                                    <li class="list-group-item">@lang('reports.balance') <b>{{$balance2 ??  0}}</b></li>
                                    <li class="list-group-item">@lang('reports.loss') <b>{{$saldo ?? 0}}</b> (кВт)</li>
                                    <li class="list-group-item">@lang('reports.loss') <b>{{round($d,2)}}</b> % <sup><i
                                                    class="glyphicon glyphicon-question-sign"
                                                    data-toggle="tooltip" title="SMTH"></i></sup></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3>@lang('reports.reports')</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        <li>@lang('reports.reports_all_users')<a
                                    href="{{route('allianceUsers',[Hashids::encode($previous_month->alliance_id)])}}">@lang('reports.load')</a>
                        </li>
                        <li>@lang('reports.reports_all_wastes')
                            <form class="form-inline"
                                  action="{{route('allianceSpending',[Hashids::encode($previous_month->alliance_id)])}}">
                                <input type="hidden" value="{{Hashids::encode($previous_month->alliance_id)}}"
                                       name="alliance_id"><select name="date">
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                </select>
                                <button type="submit">@lang('reports.select')</button>
                            </form>
                        </li>
                        <li>
                            <form class="form-inline"
                                  action="{{route('allianceSpendingKvt',[Hashids::encode($previous_month->alliance_id)])}}">
                                <div class="form-group">
                                    <label for="end_date">@lang('reports.start_date')</label>
                                    <input type="date" class="form-control" name="end_date">
                                </div>
                                <button type="submit" class="btn btn-default">@lang('reports.get')</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styleCoOp')
    <style>
        .panel {
            min-height: 250px;
        }
    </style>
@endsection
@section('scriptCoOp')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
