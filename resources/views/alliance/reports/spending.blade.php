@extends('layouts.master')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
        <div class="col-lg-12">
            <table class="dataTable" id="spend" class="display nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>@lang('reports.date')</th>
                    <th>@lang('reports.amount')</th>
                    <th>@lang('reports.purpose')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($spending as $spent)
                    <tr>
                        <td>{{$spent->date}}
                        <td>{{$spent->sum}}
                        <td>{{$spent->propose}}
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
        </div>
    </div>
@endsection
@section('scriptAdic')
    <script language="javascript">
        $(document).ready(function () {
            $('#spend').DataTable({
                dom: 'Bfrtip',
                buttons: [

                    { extend: 'excelHtml5', text: 'Загрузить в EXCELE',
                        title: 'Кооператив {{$alliance->alliance_name}}' }
                ]
            });
        });
    </script>
@endsection
