@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    <h2 class="page-header" style="padding-left: 30px;">@lang('alliance.counter_edit_header')
        <small> {{$alliance->alliance_name}}</small>
    </h2>
    <!-- end page-header -->
    <form method="POST" class="form-vertical"
          action="{{action('AllianceCounterController@store', [Hashids::encode($alliance->id)])}}">

        <div class="form-group">
            <label for="alliance_counter_type"> @lang('alliance.counter_edit_l_type') </label>
            <select name="alliance_counter_type" class="form-control" value="{{$counter->alliance_counter_type}}"
                    selected>
                <option value="1" @if($counter->alliance_counter_type = 1) selected @endif>@lang('alliance.counter_edit_type_1')</option>
                <option value="2" @if($counter->alliance_counter_type = 2) selected @endif>@lang('alliance.counter_edit_type_2')                </option>
            </select>
        </div>

        <div class="form-group">
            <label for="alliance_counter_number"> @lang('alliance.counter_edit_number') </label>
            <input class="form-control" name="alliance_counter_number" type="text"
                   value="{{ $counter->alliance_counter_number }}">
            </input>
        </div>


        <div class="form-group">
            <label for="alliance_counter_date"> @lang('alliance.counter_edit_date_check') </label>
            <input class="form-control" name="alliance_counter_date" type="date"
                   value="{{ $counter->alliance_counter_date }}">
            </input>
        </div>


        <div class="form-group">
            <label for="alliance_counter_tariff_d"> @lang('alliance.counter_edit_tariff_1') </label>
            <input class="form-control" name="alliance_counter_tariff_d" type="text"
                   value="{{$counter->alliance_counter_tariff_d}}">
            </input>
        </div>


        <div class="form-group">
            <label for="alliance_counter_tariff_n"> @lang('alliance.counter_edit_tariff_2') </label>
            <input class="form-control" name="alliance_counter_tariff_n" type="text"
                   value="{{$counter->alliance_counter_tariff_n}}">
            </input>
        </div>

        <input name="_token" type="hidden" value="{{ csrf_token() }}">
        <input name="alliance_id" type="hidden" value="{{ $alliance->id }}">
        <button class="btn btn-default waves-effect waves-light center-block" id="save" type="submit">
            @lang('alliance.counter_edit_save')
        </button>
        </input>
    </form>

@endsection