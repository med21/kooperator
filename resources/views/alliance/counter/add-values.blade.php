@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    <h1 class="page-header" style="padding-left: 30px;">@lang('alliance.add_header')
        <small><b>№ {{$counter->alliance_counter_number}}</b></small>
    </h1>
    <!-- end page-header -->
    <div class="panel-body">
        <form class="form form-vertical" name="addValue" method="POST"
              action="/alliance/{{Hashids::encode($alliance->id)}}/dashboard/add-values/{{Hashids::encode($counter->id)}}">
            {{ csrf_field() }}
            <input type="hidden" name="alliance_counter_id" value="{{$counter->id}}">
            <input type="hidden" class="form-control" id="lastdate" name="lastdate"
                   value="{{$data->alliance_counter_date_check ?? \Carbon\Carbon::now()->format('Y-m-d') }}">
            <input type="hidden" class="form-control" id="alliance_id" name="alliance_id"
                   value="{{$alliance->id}}">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="last_val_d">@lang('alliance.add_prev_val_d')</label>
                    <input type="number" class="form-control" id="last_val_d" name="last_val_d"
                           value="{{ $data->alliance_counter_val_d ?? 0}}" readonly>
                </div>
            </div>
            @if($counter->alliance_counter_type != 1)
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="">@lang('alliance.add_prev_val_n')</label>
                        <input type="number" class="form-control" id="last_val_n" name="last_val_n"
                               value="{{ $data->alliance_counter_val_n ?? 0}}" readonly>
                    </div>
                </div>
            @endif
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_counter_date_interval">@lang('alliance.add_days_int')</label>
                    <input type="number" class="form-control" id="alliance_counter_date_interval"
                           name="alliance_counter_date_interval"
                           value="" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="alliance_counter_val_d">@lang('alliance.add_cur_val_d')</label>
                    <input type="number" class="form-control" id="alliance_counter_val_d"
                           name="alliance_counter_val_d"
                           placeholder="120кВт" onblur="clc()">
                </div>
            </div>
            @if($counter->alliance_counter_type != 1)
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="alliance_counter_val_n">@lang('alliance.add_cur_val_n')</label>
                        <input type="number" class="form-control" id="alliance_counter_val_n"
                               name="alliance_counter_val_n"
                               placeholder="120кВт">
                    </div>
                </div>
            @endif
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="button">@lang('alliance.add_calc')</label>
                    <button type="button" class="btn btn-primary" id="button"
                            onclick="calcValues()"
                            style="width: 100%;">
                        @lang('alliance.add_calc')
                    </button>
                </div>
            </div>
            <div id="form" style="display: none;">
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_counter_consumption_d">@lang('alliance.add_cons_val_d')</label>
                        <input type="text" class="form-control" id="alliance_counter_consumption_d"
                               name="alliance_counter_consumption_d"
                               placeholder="120кВт" readonly="true">
                    </div>
                    @if($counter->alliance_counter_type != 1)
                        <div class="form-group">
                            <label for="alliance_counter_consumption_n">@lang('alliance.add_cons_val_n')</label>
                            <input type="text" class="form-control"
                                   id="alliance_counter_consumption_n"
                                   name="alliance_counter_consumption_n"
                                   placeholder="120кВт" readonly="true">
                        </div>
                    @endif
                </div>
                <div class=" col-md-6">
                    <div class="form-group">
                        <label for="alliance_counter_payment_d">@lang('alliance.add_price_d')</label>
                        <input type="text" class="form-control" id="alliance_counter_payment_d"
                               name="alliance_counter_payment_d"
                               placeholder="201 грн" readonly="true">
                    </div>
                </div>
                @if($counter->alliance_counter_type != 1)
                    <div class=" col-md-6">
                        <div class="form-group ">
                            <label for="alliance_counter_payment_n">@lang('alliance.add_price_n')</label>
                            <input type="text" class="form-control" id="alliance_counter_payment_n"
                                   name="alliance_counter_payment_n"
                                   placeholder="201 грн" readonly="true">
                        </div>
                    </div>
                @endif
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_counter_payment_total_debt">@lang('alliance.add_total')</label>
                        <input type="text" class="form-control"
                               id="alliance_counter_payment_total_debt"
                               name="alliance_counter_payment_total_debt">
                        <input type="hidden" class="form-control" id="alliance_counter_payment_total"
                               name="alliance_counter_payment_total">
                    </div>
                </div>
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_counter_date_check">@lang('alliance.add_date')</label>
                        <input id="date" class="form-control" type="date"
                               id="alliance_counter_date_check" name="alliance_counter_date_check"
                               value="{{ \Carbon\Carbon::now()->format('Y-m-d')}}">
                    </div>
                </div>
                <div class="text-center">
                    <button id="submit" class="btn btn-primary" type="submit">
                        @lang('alliance.add_add')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scriptCoOp')
    <script language="javascript">
        $('form[name=addValue] div#form').hide();

        function calcValues() {
            var last_val_d = $('#last_val_d').val();
            var alliance_counter_val_d = $('#alliance_counter_val_d').val();
            var min_d = alliance_counter_val_d - last_val_d;
            var paym_d = min_d * {{ $tariff_d }};
            var total_paym = paym_d;

            $('#alliance_counter_consumption_d').val(min_d);
            $('#alliance_counter_payment_d').val(Number.parseFloat(paym_d).toFixed(2));

                    @if($counter->alliance_counter_type != 1)
            var last_val_n = $('#last_val_n').val();
            var alliance_counter_val_n = $('#alliance_counter_val_n').val();
            var min_n = alliance_counter_val_n - last_val_n;
            var paym_n = min_n * {{ $tariff_n }};
            total_paym = paym_d + paym_n;

            $('#alliance_counter_consumption_n').val(min_n);
            $('#alliance_counter_payment_n').val(Number.parseFloat(paym_n).toFixed(2));
            @endif

            $('#alliance_counter_payment_total').val(Number.parseFloat(total_paym).toFixed(2));
            $('#alliance_counter_payment_total_debt').val(Number.parseFloat(total_paym).toFixed(2));

            $('form[name=addValue] div#form').show();
        }
    </script>
    <script language="JavaScript">
        function clc() {
            var oneDay = 24 * 60 * 60 * 1000;    // hours*minutes*seconds*milliseconds
            var firstDate = new Date(document.addValue.alliance_counter_date_check.value);
            var secondDate = new Date(document.addValue.lastdate.value);

            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));

            $('#alliance_counter_date_interval').val(diffDays);

            console.log(diffDays);

        }
    </script>
@endsection