@extends('layouts.master')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @if($alliance->payDay() < 30)
                    <p class="h3 text-center text-danger" style="margin-bottom: 1em">
                        <b>@lang('alliance.dashboard_header_warning')</b></p>
                @endif
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading dark-blue">
                                <i class="fa fa-users fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content dark-blue">
                            <div class="circle-tile-description text-faded">
                                @lang('alliance.dashboard_alliance_users')
                            </div>
                            <div class="circle-tile-number text-faded" style="min-height: 72px;">
                                {{ $users }}<br>
                                Внески {{$fee_collected}}
                                <span id="sparklineA"></span>
                            </div>
                            <a href="{{url('members/'.Hashids::encode($alliance->id))}}"
                               class="circle-tile-footer">@lang('alliance.dashboard_more')<i
                                        class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!--Конец пользователи!-->
                <div class="col-lg-4 col-sm-6" style="min-height: 227px;">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading @if($debtors_count > 0) dark-red @endif dark-green">
                                <i class="fa fa-users fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content  @if($debtors_count > 0) dark-red @endif dark-green">
                            <div class="circle-tile-description text-faded">
                                @lang('alliance.dashboard_alliance_debtors_elect')
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{ $debtors_count }}<br>
                                {{ $debtors_sum }} ГРН
                                <span id="sparklineA"></span>
                            </div>
                            <a class="circle-tile-footer"
                               data-toggle="collapse"
                               data-target="#debtors">@lang('alliance.dashboard_more')
                                <i
                                        class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!--Конец должников по электричеству!-->
                <div class="col-lg-4 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading @if($fee_count > 0) dark-red @endif dark-green">
                                <i class="fa fa-users fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content @if($fee_count > 0) dark-red @endif dark-green">
                            <div class="circle-tile-description text-faded">
                                @lang('alliance.dashboard_alliance_debtors_fees')

                            </div>
                            <div class="circle-tile-number text-faded">
                                {{ $fee_count }}<br>
                                {{$fee_sum}} ГРН
                                <span id="sparklineA"></span>
                            </div>
                            <a href="#" class="circle-tile-footer"
                               data-toggle="collapse"
                               data-target="#etc">@lang('alliance.dashboard_more')<i
                                        class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!--Конец должников по взносам!-->
            </div>
            <div class="row">
                <div id="debtors" class="collapse">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>@lang('alliance.dashboard_tb_debtors_elect_name')</td>
                            <td>@lang('alliance.dashboard_tb_debtors_elect_sum')</td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($debtors as $db=> $d)
                            <tr>
                                <td>{{$db}}</td>
                                <td>{{$d->alliance_user_surname}} {{$d->alliance_user_name}} {{$d->alliance_user_middlename}}</td>
                                <td>{{$d->diff }}</td>
                            </tr>
                        @empty
                            <p style="text-align: center; background-color: green; color: white">
                                @lang('alliance.dashboard_no_debtors')</p>
                        @endforelse
                        </tbody>

                    </table>
                </div>
                <div id="etc" class="collapse">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>@lang('alliance.dashboard_tb_debtors_fee_name')</td>
                            <td>@lang('alliance.dashboard_tb_debtors_fee_purpose')</td>
                            <td>@lang('alliance.dashboard_tb_debtors_fee_sum')</td>
                            <td>@lang('alliance.dashboard_tb_debtors_fee_pay')</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fee_debtor as $dbs=> $d)
                            <tr>
                                <td>{{$dbs+1}}</td>
                                <td>{{$d->alliance_user_surname}} {{$d->alliance_user_name}} {{$d->alliance_user_middlename}} </td>
                                <td>{{$d->alliance_user_pay_purpose}}</td>
                                <td>{{$d->alliance_user_fee_sum}}</td>
                                <td>@if($d->alliance_user_pay_status == 0)
                                        <a class="btn btn-xs btn-info" title="Оплатить долг"
                                           data-toggle="modal"
                                           data-target="#pay_fee{{$dbs}}"><span
                                                    class="pe-7s-check"></span></a>
                                        <div class="modal fade" id="pay_fee{{$dbs}}"
                                             role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                        </button>
                                                        <h4 class="modal-title">@lang('alliance.dashboard_tb_debtors_fee_pay')</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="/members/{{Hashids::encode($alliance->id)}}/values/{{Hashids::encode($d->alliance_user_id)}}/fee/pay/{{Hashids::encode($d->order_id)}}"
                                                              method="post">
                                                            @csrf
                                                            <div class="form-group ">
                                                                <label for="alliance_user_fee_sum">@lang('alliance.dashboard_tb_debtors_fee_sum')</label>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="alliance_user_fee_sum"
                                                                       placeholder="{{$d->alliance_user_fee_sum}}"
                                                                       readonly="true" value="{{$d->alliance_user_fee_sum}}">
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="alliance_users_payment_n">@lang('alliance.dashboard_tb_debtors_fee_sum')</label>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="alliance_user_fee_sum_c"
                                                                       value="{{$d->alliance_user_fee_sum}}">
                                                            </div>
                                                            <button class="btn btn-success"
                                                                    type="submit">@lang('alliance.dashboard_tb_debtors_fee_pay')
                                                            </button>
                                                        </form>
                                                        <form accept-charset="utf-8"
                                                              action="/payment/liqpay/sendFeeForm"
                                                              method="POST">
                                                            <input type="hidden" name="amount"
                                                                   value="{{$d->alliance_user_fee_sum}}"/>
                                                            <input type="hidden" name="alliance_id"
                                                                   value="{{$d->alliance_id}}"/>
                                                            <input type="hidden" name="order_id"
                                                                   value="{{$d->order_id}}"/>
                                                            <input type="hidden" name="description"
                                                                   value="Сплата членських внескiв за {{$d->alliance_user_pay_desc}} pik"/>
                                                            <input type="hidden" name="date"
                                                                   value="{{\Carbon\Carbon::now()->format('Y-m-d')}}"/>
                                                            <input type="hidden" name="phone"
                                                                   value="380{{$d->alliance_user_tel}}"/>
                                                            <input type="hidden" name="sender_first_name"
                                                                   value="{{$d->alliance_user_surname}}"/>
                                                            <input type="hidden" name="sender_last_name"
                                                                   value="{{$d->alliance_user_name}}"/>
                                                            <input type="hidden" name="email"
                                                                   value="{{$d->alliance_user_email}}"/>
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-success btn-lg btn-block">
                                                                @lang('payments.pay_liqpay')
                                                            </button>
                                                            <small>@lang('payments.pay_liqpay_commission')</small>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button"
                                                                class="btn btn-default"
                                                                data-dismiss="modal">@lang('alliance.dashboard_close')
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                </td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                <a href="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/add-values/'.Hashids::encode($counter->id))}}"
                   class="btn btn-primary" style="margin: 10px 0px 10px 0px;">@lang('alliance.dashboard_new_values')</a>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="display table table-striped table-bordered nowrap" id="alliance_counter">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('values.table_date')</th>
                            <th>@lang('alliance.dashboard_table_cons_d')</th>
                            <th>@lang('alliance.dashboard_table_cons_d_1')</th>
                            <th>@lang('alliance.dashboard_table_cons_n')</th>
                            <th>@lang('alliance.dashboard_table_cons_n_1')</th>
                            <th>@lang('alliance.dashboard_table_paid')</th>
                            <th>@lang('alliance.dashboard_table_paid_to')</th>
                            <th>@lang('alliance.dashboard_table_profit')</th>
                            <th>@lang('values.table_actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($values as $key => $v)
                            <tr>
                                <td>{{ $key+1 }}                        </td>
                                <td>{{$v->alliance_counter_date_check}}</td>
                                <td>{{$v->alliance_counter_consumption_d}}</td>
                                <td>{{$v->alliance_counter_val_d}}</td>
                                <td>{{$v->alliance_counter_consumption_n}}</td>
                                <td>{{$v->alliance_counter_val_n}}</td>
                                <td>{{$v->alliance_counter_payment_total}}</td>
                                <td>{{$v->alliance_counter_paid_gov}}</td>
                                <td>@if($v->alliance_counter_paid_gov != 0)
                                        {{$v->alliance_counter_payment_total - $v->alliance_counter_paid_gov}}
                                    @else
                                        0
                                    @endif</td>
                                <td>
                                    @if($v->alliance_counter_paid_gov == 0)
                                        <a class="btn btn-xs btn-success"
                                           title="РЕС" data-toggle="modal"
                                           data-target="#pay_debt{{$key}}"><span
                                                    class="glyphicon glyphicon-check"></span>Оплатить</a>
                                        <div class="modal" style="z-index: 1" id="pay_debt{{$key}}" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                        </button>
                                                        <h4 class="modal-title">
                                                            @lang('alliance.dashboard_paid')</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="post"
                                                              action="https://it-hause.com/alliance/{{Hashids::encode($alliance->id)}}/dashboard/pay-gov/{{Hashids::encode($v->alliance_counter_id)}}">
                                                            @csrf
                                                            <div class="form-group ">
                                                                <input type="hidden"
                                                                       name="alliance_id"
                                                                       value="{{$alliance->id}}">
                                                                <input type="hidden"
                                                                       name="record"
                                                                       value="{{$v->id}}">
                                                                <label for="paid_gov">Сумма</label>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="paid_gov"
                                                                       placeholder="201 грн"
                                                                       name="paid_gov"
                                                                       value="{{$v->alliance_counter_payment_total}}">
                                                            </div>
                                                            <button type="submit"
                                                                    class="btn btn-success btn-lg btn-block">
                                                                @lang('alliance.dashboard_paid_to_res')
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($v->alliance_counter_paid_gov !=0)
                                        @lang('alliance.dashboard_ok')
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>@lang('alliance.dashboard_total')</td>
                            <td>{{\Carbon\Carbon::now()->format('Y-m-d')}}</td>
                            <td>{{$values->sum('alliance_counter_consumption_d')}}</td>
                            <td>{{$values->sum('alliance_counter_val_d')}}</td>
                            <td>{{$values->sum('alliance_counter_consumption_n')}}</td>
                            <td>{{$values->sum('alliance_counter_val_n')}}</td>
                            <td>{{$values->sum('alliance_counter_payment_total')}}</td>
                            <td>{{$values->sum('alliance_counter_paid_gov')}}</td>
                            <td>@if($profit < 0)
                                    @lang('alliance.dashboard_no_profit') {{round($profit, 2)}}
                                    @else
                                    @lang('alliance.dashboard_profit') {{round($profit, 2)}}
                                @endif</td>
                            <td>@if($values->sum('alliance_counter_paid_gov') !==0)
                                    @lang('alliance.dashboard_ok')
                                    @else
                                    @lang('alliance.dashboard_attention')
                            @endif</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scriptCoOp')
    <script>
        $(document).ready(function () {
            var table = $('#alliance_counter').DataTable({

                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            })
                    @if($counter->alliance_counter_type == 1)
            var column = table.column('3');
            column.visible(!column.visible());
            @endif

        });
    </script>
@endsection
@section('styleCoOp')
<style>
    /*
    Generic Styling, for Desktops/Laptops
    */
    table {
        width: 100%;
        border-collapse: collapse;
    }

    /* Zebra striping */
    tr:nth-of-type(odd) {
        background: #eee;
    }

    th {
        background: #333;
        color: white;
        font-weight: bold;
    }

    td, th {
        padding: 6px;
        border: 1px solid #ccc;
        text-align: left;
    }

    /*
    Max width before this PARTICULAR table gets nasty
    This query will take effect for any screen smaller than 760px
    and also iPads specifically.
    */
    @media only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px) {

        /* Force table to not be like tables anymore */
        table, thead, tbody, tfoot, th, td, tr {
            display: block;
        }

        /* Hide table headers (but not display: none;, for accessibility) */
        thead tr {
            position: absolute;
            top: -9999px;
            left: -9999px;
        }

        tr {
            border: 1px solid #ccc;
        }

        td {
            /* Behave  like a "row" */
            border: none;
            border-bottom: 1px solid #eee;
            position: relative;
            padding-left: 50%;
        }

        td:before {
            /* Now like a table header */
            position: absolute;
            /* Top/left values mimic padding */
            top: 6px;
            left: 6px;
            width: 45%;
            padding-right: 10px;
            white-space: nowrap;
        }

        /*
        Label the data
        */
        td:nth-of-type(1):before {
            content: "№";
        }

        td:nth-of-type(2):before {
            content: "@lang('values.table_date')";
        }

        td:nth-of-type(3):before {
            content: "@lang('alliance.dashboard_table_cons_d')";
        }
        td:nth-of-type(4):before {
            content: "Показатели день";
        }

        td:nth-of-type(5):before {
            content: "@lang('alliance.dashboard_table_cons_n')";
        }
        td:nth-of-type(6):before {
            content: "Показатели ночь";
        }
        td:nth-of-type(7):before {
            content: "@lang('alliance.dashboard_table_paid')";
        }

        td:nth-of-type(8):before {
            content: "@lang('alliance.dashboard_table_paid_to')";
        }

        td:nth-of-type(9):before {
            content: "@lang('alliance.dashboard_table_profit')";
        }

        td:nth-of-type(10):before {
            content: "@lang('values.table_actions')";
        }
    }

    /* Smartphones (portrait and landscape) ----------- */
    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px) {
        body {
            padding: 0;
            margin: 0;
            width: 320px;
        }
    }

    /* iPads (portrait and landscape) ----------- */
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        body {
            width: 495px;
        }
    }

    @media (min-width: 768px) {

    }

    #page-wrapper .breadcrumb {
        padding: 8px 15px;
        margin-bottom: 20px;
        list-style: none;
        background-color: #e0e7e8;
        border-radius: 0px;

    }

    @media (min-width: 768px) {
        .circle-tile {
            margin-bottom: 30px;
        }
    }

    .circle-tile {
        margin-bottom: 15px;
        text-align: center;
    }

    .circle-tile-heading {
        position: relative;
        width: 80px;
        height: 80px;
        margin: 0 auto -40px;
        border: 3px solid rgba(255, 255, 255, 0.3);
        border-radius: 100%;
        color: #fff;
        transition: all ease-in-out .3s;
    }

    .dark-blue {
        background-color: #34495e;
    }

    .dark-green {
        background-color: green;
    }

    .dark-red {
        background-color: red;
    }

    .text-faded {
        color: rgba(255, 255, 255, 0.7);
    }

    .circle-tile-heading .fa {
        line-height: 80px;
    }

    .circle-tile-content {
        padding-top: 50px;
    }

    .circle-tile-description {
        text-transform: uppercase;
    }

    .text-faded {
        color: rgba(255, 255, 255, 0.7);
    }

    .circle-tile-number {
        padding: 5px 0 15px;
        font-size: 26px;
        font-weight: 700;
        line-height: 1;
    }

    .circle-tile-footer {
        display: block;
        padding: 5px;
        color: rgba(255, 255, 255, 0.5);
        background-color: rgba(0, 0, 0, 0.1);
        transition: all ease-in-out .3s;
    }

    .circle-tile-footer:hover {
        text-decoration: none;
        color: rgba(255, 255, 255, 0.5);
        background-color: rgba(0, 0, 0, 0.2);
    }
</style>
@endsection