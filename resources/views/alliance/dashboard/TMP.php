

<br>




<div class="btn-group show-on-hover pull-left">
    <button type="button" class="btn btn-default dropdown-toggle"
            data-toggle="dropdown">
        Выберите год <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @foreach($y as $t)
        <li>
            <a href="/alliance/{{Hashids::encode($alliance->id)}}/dashboard/index/?year={{ $t->year }}">Год {{$t->year}}</a>
        </li>
        @endforeach
    </ul>
</div>
Показания центрального счетчика
<small><span> {{$year}}</span></small>
<a id="add_alliance_values"
   href="{{url('/alliance/'.Hashids::encode($alliance->id).'/dashboard/add-values/'.Hashids::encode($counter->id))}}"
   title="Новые показания"><i class="fa fa-plus">Новые
        показания</i></a>



<div class="col-lg-4 col-sm-6">
    <div class="circle-tile">
        <a href="#">
            <div class="circle-tile-heading dark-blue">
                <i class="fa fa-users fa-fw fa-3x"></i>
            </div>
        </a>
        <div class="circle-tile-content dark-blue">
            <div class="circle-tile-description text-faded">
                Пользователей
            </div>
            <div class="circle-tile-number text-faded">
                {{ $users }}<br>
                {{ $fee_collected }} ГРН
                <span id="sparklineA"></span>
            </div>
            <form action="{{url('members/'.Hashids::encode($alliance->id))}}">
                <input type="submit" class="btn circle-tile-footer" value="Подробнее"/>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-4 col-sm-6">
    <div class="circle-tile">
        @if($debtors_count > 0)
        <div class="circle-tile-heading dark-red">
            <i class="fa fa-frown-o fa-fw fa-3x"></i>
            @else
            <div class="circle-tile-heading dark-green">
                <i class="fa fa-smile-o fa-fw fa-3x"></i>
                @endif

            </div>
            @if($debtors_count > 0)
            <div class="circle-tile-content dark-red">
                @else
                <div class="circle-tile-content dark-green">
                    @endif
                    <div class="circle-tile-description text-faded">
                        Должников по электричеству
                    </div>
                    <div class="circle-tile-number text-faded">
                        {{ $debtors_count }}<br>
                        {{ $debtors_sum }} ГРН
                        <span id="sparklineA"></span>
                    </div>
                    <button type="button" class="btn circle-tile-footer"
                            data-toggle="collapse"
                            data-target="#debtors">Подробнее
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-sm-6">
    <div class="circle-tile">
        @if($fee_count > 0)
        <div class="circle-tile-heading dark-red">
            <i class="fa fa-money fa-fw fa-3x"></i>
            @else
            <div class="circle-tile-heading dark-green">
                <i class="fa fa-money fa-fw fa-3x"></i>
                @endif
            </div>
            @if($fee_count > 0)
            <div class="circle-tile-content dark-red">
                @else
                <div class="circle-tile-content dark-green">
                    @endif
                    <div class="circle-tile-description text-faded">
                        Должники по взносам и другим платежам
                    </div>
                    <div class="circle-tile-number text-faded">
                        {{ $fee_count }}<br>
                        {{$fee_sum}} ГРН
                        <span id="sparklineA"></span>
                    </div>
                    <button type="button"
                            class="btn circle-tile-footer"
                            data-toggle="collapse"
                            data-target="#etc">Подробнее
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="row">
    <div class="content table-responsive table-full-width">
        <table class="display table table-striped table-bordered nowrap" id="alliance_counter">
            <thead>
            <tr>
                <th>№</th>
                <th>Дата</th>
                <th>Потребление день</th>
                <th>Потребление ночь</th>
                <th>Оплачено</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($values as $key => $v)
            <tr>
                <td>{{ $key+1 }}                        </td>
                <td>{{$v->alliance_counter_date_check}}</td>
                <td>{{$v->alliance_counter_consumption_d}}</td>
                <td>{{$v->alliance_counter_consumption_n}}</td>
                <td>{{$v->alliance_counter_payment_total}}</td>
                <td>{{$v->alliance_counter_payment_total - $v->alliance_counter_payment_total_debt}}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td>Итого
                <td>{{\Carbon\Carbon::now()->format('Y-m-d')}}</td>
                <td>{{$values->sum('alliance_counter_consumption_d')}}</td>
                <td>{{$values->sum('alliance_counter_consumption_n')}}</td>
                <td>{{$values->sum('alliance_counter_payment_total')}}</td>
                <td>@if(!empty($profit)){{round($profit, 2)}}

                    @endif
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>