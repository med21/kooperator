@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="col-lg-6">
            <h3 class="text-center">Новое уведомление</h3>
            <div class="panel-body">
                <form method="post" action="{{route('notificationCreate',Hashids::encode($alliance->id))}}">
                    @csrf
                    <div class="form-group">
                        <label for="title">Название</label>
                        <input type="text" id="title" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="text">Текс уведомления</label>
                        <input type="text" id="text" name="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="date">Дата окончания актуальности</label>
                        <input type="date" id="date" name="date" placeholder="{{\Carbon\Carbon::now()}}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="priority">Приоритет</label>
                        <select id="priority" name="priority" class="form-control">
                            <option></option>
                            <option value="low">Низкий</option>
                            <option value="medium">Средний</option>
                            <option value="high">Высокий</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="users">Выбрать пользователей</label>
                        <select class="select2 select2-container select2-container--default select2-container--above select2-container--focus users"
                                style="width: 100%" id="users" name="users[]" multiple="multiple">
                            @foreach($user as $user)
                                <option value="{{$user->id}}">{{$user->alliance_user_surname}} {{$user->alliance_user_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="all_users">Отправить всем</label>
                        <input type="checkbox" id="all_users" name="all_users" class="form-control">
                    </div>
                    <input type="hidden" name="alliance_id" value="{{$alliance->id}}">
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary ">Создать</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-6">
            <h3 class="text-center">Все уведомления</h3>
            <table class="table text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Кол-во просмотров</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($notifications as $key => $n)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$n->title}}</td>
                        <td>{{\Carbon\Carbon::parse($n->created_at)->format('Y-m-d')}}</td>
                        <td>{{$n->views ?? 0}}/{{$n->users_count}}</td>
                        <td>{{$n->m_status}}</td>
                        <td>
                            <a href="{{url('/alliance/'.Hashids::encode($alliance->id).'/dashboard/notifications/'.$n->id.'/destroy')}}"><i
                                        class="glyphicon glyphicon-trash"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('scriptCoOp')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.users').select2(
                {
                    placeholder: 'Пользователь....',
                }
            );
        });
    </script>
@endsection
@section('styleCoOp')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection