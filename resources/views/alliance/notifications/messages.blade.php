@foreach($notifications as $notification)
    <div class="alert img-rounded @if($notification->priority == 'low')alert-info @elseif($notification->priority == 'medium')alert-warning
@elseif($notification->priority == 'high') alert-danger @endif">
        <p> {{$notification->text}} <a
                    href="{{url('members/'.Hashids::encode($alliance->id).'/values/'.Hashids::encode($user->id).'/notification/'.$notification->id.'/read')}}"
                    class="btn btn-primary btn-sm pull-right">Ознакомлен</a></p>
    </div>
@endforeach
