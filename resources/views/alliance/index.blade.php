@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <!-- begin #content -->
            <div id="content" class="content">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Кооперативы
                        </div>
                        <div class="panel-body">

                        @include('layouts.errors')
                        @include('layouts.success')
                        <!-- begin #content -->
                            <div id="content" class="content">
                                <!-- begin breadcrumb -->
                                <ol class="breadcrumb pull-right">
                                    <li><a href="{{url('/home')}}">Главная</a></li>
                                    <li class="active">кооперативы</li>
                                </ol>
                                <!-- end breadcrumb -->
                                <!-- begin page-header -->
                                <h1 class="page-header">Все <small>Кооперативы</small></h1>
                                <!-- end page-header -->

                                <div style="padding-bottom: 15px;text-align: right">
                                    <a href="{{url('alliance/create')}}" class="btn btn-primary" id="crear-usuario" title="Добавить кооператив"><i class="glyphicon glyphicon-plus"></i></a>
                                </div>

                                <table class="table table-striped table-bordered table-hover" id="table">
                                    <thead>

                                    <th>#</th>
                                    <th>Название</th>
                                    <th>Глава</th>
                                    <th>Адрес</th>

                                    <th>Действия</th>

                                    </thead>
                                    <tbody>
                                    @foreach($alliance as $key => $entity)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$entity->alliance_name}}</td>
                                            <td>{{$entity->name}}</td>
                                            <td>{{$entity->alliance_state}} {{$entity->alliance_state}}</td>
                                            <!-- begin actions-->
                                            <td>
                                                <!-- edit alliance-->
                                                <a id="editar-alliance" class="btn btn-xs btn-primary" href="{{ url('alliance/'.Hashids::encode($entity->id).'/edit') }}" title="Редактировать"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                                <!-- Borrar -->
                                                <a id="delete-alliance" class="btn btn-xs btn-danger" href="{{ url('alliance/destroy/'. Hashids::encode($entity->id).'') }}" onclick='return confirm("Удалить?");' title="Eliminar Usuario"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                                <a id="delete-alliance" class="btn btn-xs btn-success" href="{{ url('alliance/'.Hashids::encode($entity->id).'/dashboard/index')}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>

                                            </td>
                                            <!-- end acciones -->
                                        </tr>
                                    @Endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
