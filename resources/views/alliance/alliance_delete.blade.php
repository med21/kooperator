@extends('layouts.master')
@section('content')
    @include('layouts.success')
    @include('layouts.errors')

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <p>Последний шаг перед тем как удалить свой аакаунт из системы!</p><br>
                <p> Для продолжения нажмите на кнопку ниже</p><br>
                <a href="#" class="btn btn-danger btn-md center-text" onclick="conf()">Удалить
                    аккаунт</a>
            </div>
        </div>
    </div>
@endsection
@section('scriptCoOp')
    <script type="text/javascript">
        function conf () {
            if (confirm("Нажав ОК, вы Безвозвратно удалите аккаунт в сервисе КооператоR")) {
                location.replace("{{route('deleteAcc',Hashids::encode($alliance->id))}}");
            } else {

            }
        }
    </script>
@endsection