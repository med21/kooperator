@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Кооператив
                    </div>
                @include('layouts.errors')
                @include('layouts.success')
                <!-- begin #content -->
                    <div id="content" class="content">
                        <!-- begin breadcrumb -->
                        <ol class="breadcrumb pull-right">
                            <li><a href="{{url('/home')}}">Главная</a></li>
                            <li class=""><a href="{{url('/alliance')}}">Кооперативы</a></li>
                            <li class="active">Новый кооператив</li>
                        </ol>
                        <!-- end breadcrumb -->
                        <!-- begin page-header -->
                        <h1 class="page-header" style="padding-left: 30px;">Кооператив<small> новый</small></h1>
                        <!-- end page-header -->
                        <div class="panel-body">
                            <form method="POST" action="/alliance/{{Hashids::encode($data->id)}}/update">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Название кооператива
                                        </label>
                                        <input class="form-control" name="alliance_name" type="text" value="{{ $data->alliance_name }}">
                                        </input>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Глава кооператива
                                        </label>
                                        <input class="form-control" name="chef_id" type="text" value="{{ $chef[0]->name }}" disabled="true">
                                        </input>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Область
                                        </label>
                                        <input class="form-control" name="alliance_state" type="text" value="{{ $data->alliance_state }}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Район
                                        </label>
                                        <input class="form-control" name="alliance_region" type="text" value="{{ $data->alliance_region }}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Ближайший населенный пунк
                                        </label>
                                        <input class="form-control" name="alliance_village" type="text" value="{{ $data->alliance_village }}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Улица
                                        </label>
                                        <input class="form-control" name="alliance_street" type="text" value="{{ $data->alliance_street }}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Код ЕДРПОУ
                                        </label>
                                        <input class="form-control" name="code_edrpou" type="text" value="{{ $data->alliance_edrpou}}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            P/P
                                        </label>
                                        <input class="form-control" name="alliance_bank_account" type="text" value="{{$data->alliance_bank_account}}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Дневной тариф
                                        </label>
                                        <input class="form-control" name="tariff_d" type="text" value="{{$data->tariff_d}}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Ночной тариф
                                        </label>
                                        <input class="form-control" name="tariff_n" type="text" value="{{$data->tariff_n}}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            GPS высота
                                        </label>
                                        <input class="form-control" name="alliance_geo_alt" type="text" value="{{ $data->alliance_geo_alt }}">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            GPS широта
                                        </label>
                                        <input class="form-control" name="alliance_geo_ltd" type="text" value="{{$data->alliance_geo_ltd}}">
                                        </input>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <button class="btn btn-default waves-effect waves-light" id="save" type="submit">
                                        Сохранить
                                    </button>
                                    </input>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection







