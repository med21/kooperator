@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <!-- begin #content -->
            <div id="content" class="content">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Настройки
                        </div>
                        <div class="panel-body">

                        @include('layouts.errors')
                        @include('layouts.success')

                        <!-- begin #content -->
                            <div id="content" class="content">
                                <!-- begin breadcrumb -->
                                <ol class="breadcrumb pull-right">
                                    <li><a href="{{url('/home')}}">Главная</a></li>
                                    <li class="active">Настройки</li>
                                </ol>
                                <!-- end breadcrumb -->
                                <!-- begin page-header -->
                                <h1 class="page-header">Настройки
                                    <small>{{$alliance->alliance_name}}</small>
                                </h1>
                                <!-- end page-header -->

                                <form method="post" action="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/settings/save')}}">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <labele>Приватный ключ LiqPay</labele>
                                            <input type="text" class="form-control" name="liqpay_pr_key" value="{{$settings->liqpay_pr_key ?? ""}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <labele>ID LiqPay</labele>
                                            <input type="text" class="form-control" name="liqpay_id" value="{{$settings->liqpay_id ?? ""}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="alliance_id" value="{{$alliance->id}}">
                                           <button type="submit" class="btn btn-default">Сохранить</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <a href="{{route('deleteAlliance',Hashids::encode($alliance->id))}}" class="btn btn-danger pull-left btn-md">Удалить Кооператив</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
