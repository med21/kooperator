@extends('layouts.master')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel">
                        @include('layouts.errors')
                        @include('layouts.success')
                        <div class="panel-heading">
                            <h4>@lang('spending.header')</h4>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="{{route('spending.Store',[Hashids::encode($alliance->id)])}}">
                                @csrf
                                <div class="form-group">
                                    <label for="propose">@lang('spending.purpose')</label>
                                    <input type="text" class="form-control" name="propose">
                                </div>
                                <div class="form-group">
                                    <label for="sum">@lang('spending.amount')</label>
                                    <input type="number" class="form-control" name="sum">
                                </div>
                                <div class="form-group">
                                    <label for="desc">@lang('spending.desc')</label>
                                    <textarea class="form-control" rows="4" cols="50" name="desc"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="date">@lang('spending.date')</label>
                                    <input type="date" class="form-control" name="date" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
                                </div>
                                <input type="hidden" name="alliance_id" value="{{$alliance->id}}">

                                <button type="submit" class="btn btn-default">@lang('spending.save')</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>@lang('spending.header')</h4>
                        </div>
                        <div class="panel-body">
                            <table class="dataTable" id="spending">
                                <thead>
                                <tr>
                                    <th>@lang('spending.date')</th>
                                    <th>@lang('spending.amount')</th>
                                    <th>@lang('spending.purpose')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($spendings as $spend)
                                <tr>
                                    <td>{{$spend->date}}</td>
                                    <td>{{$spend->sum}}</td>
                                    <td>{{$spend->propose}}</td>
                                </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                    <td>{{$spendings->sum('sum')}}</td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scriptAdic')
    <script>
        $(document).ready(function () {
            var table = $('#spending').DataTable({

                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            } );
        } );
    </script>
@endsection