@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    <h1 class="text-center">@lang('alliance.register_alliance_header')
    </h1>
    <!-- end page-header -->
    <form method="POST" action="{{action('AllianceController@store')}}?s=3">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_name">
                        @lang('alliance.register_alliance_name')
                    </label>
                    <input class="form-control" name="alliance_name" id="alliance_name" type="text"
                           value="{{old('alliance_name', "")}}">
                </div>
            </div>
            @if(Auth::user()->hasRole('superadmin'))
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="alliance_chef_id">
                            @lang('alliance.register_alliance_chef')
                        </label>
                        <select class="form-control" name="alliance_chef_id">
                            @foreach($chef as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
            @else
                <input type="hidden" name="alliance_chef_id" value="{{ Auth::id() }}">
            @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_state">@lang('alliance.register_alliance_state')
                    </label>
                    <input class="form-control" name="alliance_state" id="alliance_state" type="text"
                           value="{{old('alliance_state', "")}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_region">
                        @lang('alliance.register_alliance_region')
                    </label>
                    <input class="form-control" name="alliance_region" id="alliance_region" type="text"
                           value="{{old('alliance_region', "")}}">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_village">
                        @lang('alliance.register_alliance_village')
                    </label>
                    <input class="form-control" name="alliance_village" id="alliance_village" type="text"
                           value="{{old('alliance_village', "")}}">

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_street">
                        @lang('alliance.register_alliance_street')
                    </label>
                    <input class="form-control" name="alliance_street" id="alliance_street" type="text"
                           value="{{old('alliance_street', "")}}">
                    <small class="form-text text-muted">@lang('alliance.register_alliance_street_help')</small>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_edrpou">
                        @lang('alliance.register_alliance_edrpou')
                    </label>
                    <input class="form-control" name="alliance_edrpou" id="alliance_edrpou" type="text"
                           value="{{old('alliance_edrpou', "")}}">

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_bank_account">
                        @lang('alliance.register_alliance_rr')
                    </label>
                    <input class="form-control" name="alliance_bank_account" id="alliance_bank_account"
                           type="text"
                           value="{{old('alliance_bank_account', "")}}">
                    <small class="form-text text-muted"></small>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tariff_d">
                        @lang('alliance.register_alliance_tariff_d')
                    </label>
                    <input class="form-control" name="tariff_d" id="tariff_d" type="text"
                           value="{{old('tariff_d', "")}}">
                    <small class="form-text text-muted">@lang('alliance.register_alliance_tariff_d_h')</small>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tariff_n">
                        @lang('alliance.register_alliance_tariff_n')
                    </label>
                    <input class="form-control" name="tariff_n" id="tariff_n" type="text"
                           value="{{old('tariff_n', "")}}">
                    <small class="form-text text-muted">@lang('alliance.register_alliance_tariff_n_h')</small>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_geo_alt">
                        @lang('alliance.register_alliance_gps_alt')
                    </label>
                    <input class="form-control" name="alliance_geo_alt" id="alliance_geo_alt" type="text"
                           value="{{old('alliance_geo_alt', 0)}}">
                    <small class="form-text text-muted"> @lang('alliance.register_alliance_n_r')</small>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_geo_ltd">
                        @lang('alliance.register_alliance_gps_ltd')
                    </label>
                    <input class="form-control" name="alliance_geo_ltd" id="alliance_geo_ltd" type="text"
                           value="{{old('alliance_geo_ltd', 0)}}">
                    <small class="form-text text-muted"> @lang('alliance.register_alliance_n_r')</small>

                </div>
            </div>
        </div>

        <div class="col-sm-12 text-center">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <button class="btn btn-default waves-effect waves-light" id="save" type="submit">
                @lang('alliance.register_alliance_save')
            </button>

        </div>
    </form>
@endsection







