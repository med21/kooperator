<!DOCTYPE html>
<html>
<head>
    @include('emails.layouts.header')
    @include('emails.layouts.style')
    @yield('styleEmail')
</head>
<body>
<section>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</section>
@include('emails.layouts.footer')
@include('emails.layouts.script')
</body>
</html>
