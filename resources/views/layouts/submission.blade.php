<!doctype html>
<html lang="ru">
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оплата доступа</title>
</head>
<body>
<div class="text-center" style="padding-top: 2em">
    @if($length == 0)
        <p class="h1 text-danger">У вас закончился срок предоплаченого доступа!<br>
            Выберите вариант продления:</p>
    @elseif($length < 90)
        <p class="h1 text-warning">У вас скоро закончился срок предоплаченого доступа!<br>
            <span>Остадось <span class="text-danger"><b><u>{{$length}}</u></b></span> дней</span><br>
            Выберите вариант продления:</p>
    @elseif($length > 120)

    @elseif($length < 61)
        <p class="h1 text-danger">Cкоро закончился срок бесплатного доступа! Осталось {{$length}}<br>
            Выберите вариант продления:</p>

    @endif
</div>
@if($length >120)
    <div class="section" id="pricing" style="padding-top: 20%">
        <div class="container">
            <p class="h1 text-success">Срок предоплаченого доступа окончится через <b><u>{{$length}}</u></b> дней.<br>
            </p>
            <p class="h1 text-center text-success">Никаких действий предпринимать не нужно!</p>
            <p class="h1 text-center text-success">Спасибо что пользуетесь нашим сервисом!</p>
            <div class="col-lg-12 text-center">
                <a href="{{url('/alliance/'.Hashids::encode($alliance->id).'/dashboard/index')}}"
                   class="btn btn-success text-center btn-lg">Назад</a>
            </div>
        </div>
    </div>
@else
    <div class="section" id="pricing" style="padding-top: 2em">
        <div class="container">
            <div class="section-title">
                <h3>Варианты доступа</h3>
            </div>

            <div class="card-deck">
                <div class="card pricing popular">
                    <div class="card-head">
                        <small class="text-primary">Годовой</small>
                        <span class="price">3578<sub>грн</sub><sub>/год</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <div class="list-group-item">Полный доступ</div>
                        <div class="list-group-item">Всевозможные отчеты</div>
                        <div class="list-group-item">Импорт данных</div>
                        <div class="list-group-item">Оплата картой</div>
                        <div class="list-group-item">Личный кабинет для каждого</div>

                    </ul>
                    <div class="card-body">
                        <form accept-charset="utf-8" action="/payment/liqpay/access" method="POST"/>
                        <input type="hidden" name="amount" value="3578"/>
                        <input type="hidden" name="type" value="subscription"/>
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}"/>
                        <input type="hidden" name="alliance_id" value="{{auth()->user()->allianceID()->id}}"/>
                        <input type="hidden" name="order_id"
                               value="{{str_pad(auth()->user()->id + 2, 8, "0", STR_PAD_LEFT)}}"/>
                        <input type="hidden" name="description"
                               value="Сплата за користування сервісом КооператоR.Aккаут кооператива Ялинка до {{\Carbon\Carbon::now()->addYear(1)->format('Y-m-d')}}"/>
                        <input type="hidden" name="date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}"/>
                        <input type="hidden" name="phone" value="+380{{auth::user()->tel}}"/>
                        <input type="hidden" name="sender_first_name" value="{{auth::user()->name}}"/>
                        <input type="hidden" name="sender_last_name" value="{{auth::user()->surname}}"/>
                        <input type="hidden" name="email" value="{{auth::user()->email}}"/>
                        <button type="submit" class="btn btn-success btn-lg btn-block">Оплатить через LiqPay.</button>
                        <small class="text-center">(+5% комиссия)</small>
                        </form>
                    </div>
                </div>
                <div class="card pricing popular">
                    <div class="card-head">
                        <small class="text-primary">Помесячно</small>
                        <span class="price">350<sub>грн/м</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <div class="list-group-item">Помесячная оплата</div>
                        <div class="list-group-item">Всевозможные отчеты</div>
                        <div class="list-group-item">Импорт данных</div>
                        <div class="list-group-item">Личный кабинет для каждого</div>
                        <div class="list-group-item">
                            <del>Оплата картой</del>
                        </div>
                    </ul>
                    <div class="card-body">
                        <form accept-charset="utf-8" action="/payment/liqpay/access" method="POST"/>
                        <input type="hidden" name="amount" value=""/>
                        <input type="hidden" name="alliance_id" value=""/>
                        <input type="hidden" name="order_id" value=""/>
                        <input type="hidden" name="description"
                               value="Сплата за електроенергію за {{\Carbon\Carbon::now()->format('Y-m-d')}}"/>
                        <input type="hidden" name="date" value=""/>
                        <input type="hidden" name="phone" value="380"/>
                        <input type="hidden" name="sender_first_name" value=""/>
                        <input type="hidden" name="sender_last_name" value=""/>
                        <input type="hidden" name="email" value=""/>
                        <button type="submit" class="btn btn-success btn-lg btn-block">Оплатить через LiqPay.</button>
                        <small class="text-center">(+5% комиссия)</small>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<!-- // end .pricing -->
</body>
</html>
