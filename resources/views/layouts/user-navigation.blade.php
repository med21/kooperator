<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url('/')}}" class="simple-text">
                    КооператоR
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{url('/members/'.Hashids::encode($alliance->id).'/values/'.Hashids::encode($user->id))}}">
                        <i class="pe-7s-graph"></i>
                        <p>@lang('menu.main')</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/members/{{ Hashids::encode($alliance->id) }}/edit/{{Hashids::encode($user->id)}}">
                        <i class="pe-7s-user"></i>
                        <p>@lang('menu.profile')</p>
                    </a>
                </li>
                <li class="active">
                    <a href="/members/{{ Hashids::encode($alliance->id) }}/chart/{{Hashids::encode($user->id).'/?year='.\Carbon\Carbon::now()->format('Y')}}">
                        <i class="pe-7s-graph1"></i>
                        <p>@lang('menu.chart')</p>
                    </a>
                </li>
                <li class="active"><a
                            href="/members/{{ Hashids::encode($alliance->id) }}/values/{{Hashids::encode($user->id)}}/fee/index">
                        <i class="pe-7s-cash"></i>
                        <p>Платежи <span class="label label-danger">{{Auth::user()->getFeesCount()}}</span></p>
                    </a></li>

                {{--<li class="active"><a--}}
                            {{--href="/members/{{ Hashids::encode($alliance->id) }}/api/{{Hashids::encode($user->id)}}">--}}
                        {{--<i class="pe-7s-settings"></i>--}}
                        {{--<p>Настройки</p>--}}
                    {{--</a></li>--}}

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">@lang('menu.my_cabinet')</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> {{Auth::user()->surname}} {{Auth::user()->name}}
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{ route('login') }}">@lang('main.login')</a></li>
                            <li><a href="{{ route('register') }}">@lang('main.register')</a></li>
                        @else
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    @lang('auth.logout')
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>