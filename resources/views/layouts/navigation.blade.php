<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="{{asset('assets/img/sidebar-5.jpg')}}">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{url('/')}}" class="simple-text">
                    КооператоR
                </a>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/index')}}">
                        <i class="pe-7s-graph"></i>
                        <p>@lang('menu.main')</p>
                    </a>
                </li>
                @if(!is_null($alliance->getCounter()))
                    <li class="active">
                        <a href=" {{url('members/'.Hashids::encode($alliance->id))}}">
                            <i class="pe-7s-users"></i>
                            <p>@lang('menu.alliance_users')</p>
                        </a>
                    </li>
                    <li class="active">
                        <a href=" {{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/reports')}}">
                            <i class="pe-7s-file"></i>
                            <p>@lang('menu.reports')</p>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/spending')}}">
                            <i class="pe-7s-calculator"></i>
                            <p>@lang('menu.spending')</p>
                        </a>
                    </li>

                    <li class="active"><a
                                href="{{url('/members/'.Hashids::encode($alliance->id).'/values/import/users/index')}}"
                        ><i class="pe-7s-cloud-upload"></i>
                            <p>@lang('menu.import_users')</p>
                        </a>
                    </li>
                    <li class="active"><a
                                href="{{url('/members/'.Hashids::encode($alliance->id).'/values/import/index')}}">
                            <i class="pe-7s-cloud-upload"></i>
                            <p>@lang('menu.import_values')</p>
                        </a>
                    </li>
                
                    <li class="active"><a href="{{url('members/'.Hashids::encode($alliance->id).'/create')}}">
                            <i class="pe-7s-plus"></i>
                            <p>@lang('menu.add_user')</p></a></li>
                    <li class="active"><a href="{{url('members/'.Hashids::encode($alliance->id).'/fee/create')}}">
                            <i class="pe-7s-cash"></i>
                            <p>@lang('menu.make_payment')</p>
                        </a>
                    </li>
                    <li class="active"><a href="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/notifications/')}}">
                            <i class="pe-7s-mail-open"></i>
                            <p>@lang('menu.notification_create')</p>
                        </a>
                    </li>
                    <li class="active"><a
                                href="{{url('alliance/'.Hashids::encode($alliance->id).'/order/submission/renew')}}">
                            @if($alliance->payDay() < 30)
                                <i class="pe-7s-attention" style="color: red"></i>
                            @else
                                <i class="pe-7s-piggy"></i>
                            @endif
                            <p>@lang('menu.access')</p>
                        </a>
                    </li>
                    <li class="active"><a
                                href="{{url('alliance/'.Hashids::encode($alliance->id).'/dashboard/settings')}}">
                            @if($alliance->payDay() < 30)
                                <i class="pe-7s-attention" style="color: red"></i>
                            @else
                                <i class="pe-7s-config"></i>
                            @endif
                            <p>@lang('menu.settings')</p>
                        </a>
                    </li>
                    @else
                    <li class="active"><a
                                href="{{route('deleteAlliance',Hashids::encode($alliance->id))}}">
                            <i class="pe-7s-trash"></i>
                            <p>Удалить аккаунт</p>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">@lang('menu.short_report')</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        </li>
                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                                {{--{{ Config::get('languages')[App::getLocale()] }}--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu">--}}
                                {{--@foreach (Config::get('languages') as $lang => $language)--}}
                                    {{--@if ($lang != App::getLocale())--}}
                                        {{--<li>--}}
                                            {{--<a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>--}}
                                        {{--</li>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a href="{{ route('login') }}">@lang('main.login')</a></li>
                            <li><a href="{{ route('register') }}">@lang('main.register')</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            @lang('auth.logout')
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>

                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>