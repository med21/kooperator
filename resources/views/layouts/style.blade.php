<!-- Bootstrap core CSS     -->
<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet"/>

<!--  Light Bootstrap Table core CSS    -->
<link href="{{asset('assets/css/light-bootstrap-dashboard.css?v=1.4.0')}}" rel="stylesheet"/>

<!--     Fonts and icons     -->
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
<link href="{{asset('assets/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />

<!--    DataTable     -->
<link href="{{asset('assets/DataTables-3/datatables.css')}}" rel="stylesheet" />