<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<link href="{{asset('css/bootstrap.min.css.map')}}">
<!-- Themify Icons -->
<link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
<!-- Owl carousel -->
<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
<!-- Main css -->
<link rel="stylesheet" href="{{asset('css/main.css')}}">