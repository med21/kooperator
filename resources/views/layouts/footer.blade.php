<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="{{url('/#opportunity')}}" target="_blank">
                        @lang('main.press')
                    </a>
                </li>
                <li>
                    <a href="{{url('/terms')}}" target="_blank">
                        @lang('main.terms')
                    </a>
                </li>
                <li>
                    <a href="{{url('/policy')}}" target="_blank">
                        @lang('main.privacy')
                    </a>
                </li>

            </ul>
        </nav>
        <p class="copyright pull-right">
            @lang('main.copyrights'){{\Carbon\Carbon::now()->format('Y')}}
        </p>
    </div>
</footer>