<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="{{url('/home')}}">Главная</a></li>
        @role('alliance_chef')
        <li class=""><a href="{{url('members/'.Hashids::encode($alliance->id))}}">Участники</a></li>
        @endrole
        <li class="active">{{$user->alliance_user_surname}} {{$user->alliance_user_name}} </li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->