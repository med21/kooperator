<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        @role('superadmin')
        <li><a href="{{url('/home')}}">Главная</a></li>
        @endrole
        <li><a href="{{url('/alliance/'.Hashids::encode($alliance->id).'/dashboard/index')}}">Кооператив</a>
        </li>
        <li class="active">Пользователи</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Все
        <small>Пользователи</small>
    </h1>
    <!-- end page-header -->