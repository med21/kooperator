@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <!-- begin #content -->
            <div id="content" class="content">
                <div class="col-md-12">
                <div class="panel panel-default">
                    @include('users.delete')
                    <div class="panel-heading">
                        Главы кооперативов
                    </div>
                    <div class="panel-body">

                    @include('layouts.errors')
                    @include('layouts.success')

                    <!-- begin #content -->
                        <div id="content" class="content">
                            <!-- begin breadcrumb -->
                            <ol class="breadcrumb pull-right">
                                <li><a href="{{url('/home')}}">Гавная</a></li>
                                <li class="active">Главы кооперативов</li>
                            </ol>
                            <!-- end breadcrumb -->
                            <!-- begin page-header -->
                            <h1 class="page-header">Главы <small>кооперативов</small></h1>
                            <!-- end page-header -->

                            <div style="padding-bottom: 15px;text-align: right">
                                <a href="{{url('users/create')}}" class="btn btn-primary" id="crear-usuario" title="Добавить главу"><i class="glyphicon glyphicon-plus"></i></a>
                            </div>

                            <table class="table table-striped table-bordered table-hover" id="table">
                                <thead>

                                <th>#</th>
                                <th>Имя</th>
                                <th>Email</th>
                                <th>Роль</th>

                                <th>Действия</th>

                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->index  + 1}}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @foreach($user->roles as $row)
                                                @if($row->id == 4)
                                                    <li style="list-style-type:none; "> <label class="label label-default   ">{{ $row->display_name }}</label></li>
                                                @else
                                                    <li style="list-style-type:none; "> <label class="label label-success">{{ $row->display_name }}</label></li>
                                                @endif
                                            @endforeach
                                        </td>
                                        <!--кнопки действий-->
                                        <td>
                                               <!-- Редактировать-->
                                            <a id="editar-roles" class="btn btn-xs btn-primary" href="{{ url('users/'.Hashids::encode($user->id).'/edit') }}" title="Editar Usuario"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                            <!-- Удалить -->
                                            <a id="eliminar-usuario" class="btn btn-xs btn-danger" href="{{ url('users/destroy/'. Hashids::encode($user->id).'') }}" onclick='return confirm("Desea eliminar el registro?");' title="Eliminar Usuario"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>

                                        </td>
                                        <!-- end действий-->
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
