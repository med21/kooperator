@extends('layouts.master')
<style>
    #profile-image1 {
        cursor: pointer;

        width: 150px;
        height: 150px;
        border: 2px solid #03b1ce;
    }

    .title {
        font-size: 16px;
        font-weight: 500;
    }

    .bot-border {
        border-bottom: 1px #f8f8f8 solid;
        margin: 5px 0 5px 0
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Кооператив
                    </div>
                @include('layouts.errors')
                @include('layouts.success')
                <!-- begin #content -->
                    <div id="content" class="content">
                        <!-- begin breadcrumb -->
                        <ol class="breadcrumb pull-right">
                            <li><a href="{{url('/home')}}">Главная</a></li>
                            @if(\Auth::user()->hasRole('superadmin'))
                            <li class=""><a href="{{url('/alliances')}}">Кооперативы</a></li>
                            <li class=""><a href="{{url('members/'.Hashids::encode($alliance->id))}}">Участники</a>
                            </li>
                            @endif
                            <li class="active">{{ $users->alliance_user_surname }} {{ $users->alliance_user_name
 }} {{ $users->alliance_user_middlename
 }}</li>
                        </ol>
                        <!-- end breadcrumb -->
                        <!-- begin page-header -->
                        <h1 class="page-header" style="padding-left: 30px;">Участник</h1>
                        <div class="container">
                            <div class="panel-body">

                                <div class="box box-info">

                                    <div class="box-body">
                                        <div class="col-sm-6">
                                            <div align="center"><img alt="User Pic"
                                                                     src="/uploads/avatars/{{$users->avatar}}"
                                                                     id="profile-image1"
                                                                     class="img-circle img-thumbnail">

                                                <!--Upload Image Js And Css-->
                                                <form enctype="multipart/form-data"
                                                      action="/members/{{ Hashids::encode($alliance->id) }}/profile/{{ Hashids::encode($users->id) }}"
                                                      method="POST">
                                                    <label>Аватар</label>
                                                    <input type="file" name="avatar">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <hr>
                                                    <input type="submit" class="btn btn-sm btn-primary">

                                                </form>
                                            </div>

                                            <br>

                                            <!-- /input-group -->
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 style="color:#00b1b1;">{{ $users->alliance_user_surname }} {{ $users->alliance_user_name
 }} {{ $users->alliance_user_middlename
 }}</h4></span>
                                            <hr>
                                            <div class="col-sm-5 col-xs-6 title ">Адресс</div>
                                            <div class="col-sm-7">{{ $alliance->address_one }} {{ $alliance->address_two }}
                                                "C/T {{ $alliance->alliance_name }}" {{$users->alliance_user_address
}}</div>

                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>

                                            <div class="col-sm-5 col-xs-6 title ">Телефон</div>
                                            <div class="col-sm-7">{{ $users->alliance_user_tel }}</div>

                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>

                                            <div class="col-sm-5 col-xs-6 title ">Email</div>
                                            <div class="col-sm-7">{{$users->alliance_user_email }}</div>

                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>

                                            <div class="col-sm-5 col-xs-6 title ">№ Счетчика</div>
                                            <div class="col-sm-7">{{$users->alliance_user_counter_number
 }}</div>

                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>

                                            <div class="col-sm-5 col-xs-6 title ">Тариф</div>
                                            <div class="col-sm-7">{{$users->alliance_user_counter_type
                                                            }}</div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <hr style="margin:5px 0 5px 0;">


                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection