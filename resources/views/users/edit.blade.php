@extends('layouts.master')
@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="header">
                <h4 class="title">Редактировать профайл</h4>
            </div>
            <div class="content">
                <form method="POST"
                      action="{{action( 'AllianceUsersController@update',[Hashids::encode($alliance->id), Hashids::encode($user->id)])}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" class="form-control"
                                       value="{{ $user->alliance_user_surname }}" name="alliance_user_surname">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" class="form-control" placeholder="Username" value="{{ $user->alliance_user_name
 }}" name="alliance_user_name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Отчество</label>
                                <input type="text" class="form-control" value="{{ $user->alliance_user_middlename
 }}" name="alliance_user_middlename">
                            </div>
                        </div>
                    </div>
                    <!-- Pension!-->
                    @if($user->alliance_users_pensioner !=0)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Удостоверение дающее льготы</label>
                                <select class="form-control" name="alliance_users_pensioner">
                                    <option>Інвалід першої або другої групи</option>
                                    <option>Фізичні особи, які виховують трьох і більше дітей віком до 18 років</option>
                                    <option>Пенсіонери (за віком)</option>
                                    <option>Ветерани війни та особи, на яких поширюється дія Закону України «Про статус ветеранів війни, гарантії їх соціального захисту»</option>
                                    <option>Фізичні особи, визнані законом особами, які постраждали внаслідок Чорнобильської катастрофи</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Серия и Номер</label>
                                <input type="text" class="form-control" value="{{ $user->alliance_users_pensioner_number
 }}" name="alliance_users_pensioner_number">
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--End Pension!-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Электронный адрес</label>
                                <input type="email" class="form-control" placeholder="Company"
                                       value="{{$user->alliance_user_email }}" name="alliance_user_email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" class="form-control" placeholder="Last Name"
                                       value="{{$user->alliance_user_tel }}" name="alliance_user_tel">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary center-block" type="button" data-toggle="collapse"
                                data-target="#pwd" aria-expanded="false" aria-controls="pwd">
                            Для смены пароля нажмите на копку
                        </button>
                        <div class="row collapse" id="pwd">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Новый пароль</label>
                                    <input type="password" class="form-control" placeholder="Пароль"
                                           value="" name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Повторите пароль</label>
                                    <input type="password" class="form-control" placeholder="Пароль"
                                           value="" name="confirm_password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Адрес дома</label>
                                <input type="text" class="form-control" placeholder="Home Address"
                                       value="{{$alliance->alliance_state}} обл. {{$alliance->alliance_region}} р-н., {{$alliance->alliance_village}}, ул. {{$alliance->alliance_street}}, {{$user->alliance_user_address}}"
                                       disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Адрес прописки</label>
                                <input type="text" class="form-control" placeholder="Home Address"
                                       value="{{$user->alliance_users_living_address}}"
                                       name="alliance_users_living_address">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Обновить</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
    <!--End Form !-->
    <div class="col-md-4">
        <div class="card card-user">
            <div class="image">
                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400"
                     alt="..."/>
            </div>
            <div class="content">
                <div class="author">
                    <a href="#">
                        <img class="avatar border-gray" src="assets/img/faces/face-3.jpg" alt="..."/>

                        <h4 class="title">{{ $user->alliance_user_surname }} {{ $user->alliance_user_name }}<br/>
                            <small>Участник кооператива</small>
                        </h4>
                    </a>
                </div>
                <p class="description text-center">

                </p>
            </div>
            <hr>
            {{--<div class="text-center">--}}
            {{--<button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>--}}
            {{--<button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>--}}
            {{--<button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>--}}

            {{--</div>--}}
        </div>
    </div>


@endsection







