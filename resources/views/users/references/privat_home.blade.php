<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        @page { margin: 2cm }
        p { margin-bottom: 0.25cm; line-height: 115% }
    </style>
</head>
<body lang="ru-RU" dir="ltr">
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font color="#000000">
        </font><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>Дата <u>{{\Carbon\Carbon::now()->format('d.m.Y')}}</u>
                                вих.номер <u>{{$user->id}}-002-{{\Carbon\Carbon::now()->format('Y')}}</u> </b></font></font></font></font></font></font></p>
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 16pt"><b>Довідка</b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>
</b></font></font></span></font></font></font></font></p>
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="center" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>	Видана
члену садівничого товариства
“{{$alliance->alliance_name}}” П.І.Б.<u>{{$user->alliance_user_surname}} {{$user->alliance_user_name}} {{$user->alliance_user_middlename}}</u> в тому, що вона є
власником земельної ділянки № <u>{{ $alliance->alliance_user_address }}</u> площею
7 кв.м., яка знаходиться в садівничому
товаристві “{{$alliance->alliance_name}}” по вул.
<u>{{$alliance->alliance_street}}</u> №<u>{{$alliance->alliance_user_address}}</u>, що знаходитьсяза адресо<u> {{$alliance->alliance_state}} обл, {{$alliance->alliance_region}} р-н, {{$alliance->alliance_village}}</u> , на підставі Державного
акту на право власності на земельну
серії ___--__№ __--___ від ___--____ . </b></font></font></span></font></font></font></font></p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>	На
зазначеній земельній ділянці знаходяться
(будинок, сарай), які побудовані до
(дата)року. </b></font></font></span></font></font></font></font></p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>	Земельна
ділянка площею ___--__кв.м. виділена
садівничому товариству
“______--_______” рішенням
________--_________ № __--__ від __--__ в безстрокове
користування. </b></font></font></span></font></font></font></font></p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>Гоглова
правління СТ “{{$alliance->alliance_name}}” </b></font></font></span></font></font></font></font>
</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b><u>{{$chef->name}}</u></b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>_______________________________________</b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>
П.І.Б. /</b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>підпис</b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>/печать
</b></font></font></span></font></font></font></font></p>
<p align="left" style="margin-bottom: 0cm; line-height: 200%"><br/>

</p>
<p align="left" style="margin-bottom: 0cm; line-height: 100%"><br/>

</p>

<p align="center" style="margin-bottom: 0cm; line-height: 125%"><font color="#000000"><font face="Times New Roman, serif"><font size="3" style="font-size: 12pt"><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>Хотянівка
- </b></font></font></span></font><font color="#000000"><span style="text-decoration: none"><font face="Times New Roman, serif"><font size="4" style="font-size: 14pt"><b>2018</b></font></font></span></font></font></font></font></p>
</body>
</html>