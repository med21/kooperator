@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="col-lg-6">
            <h3 class="text-center">Форма импорта данных потребления</h3>
        <form class="form" method="POST"
              action="/members/{{ Hashids::encode($alliance->id) }}/values/import/import_parse"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                <label for="csv_file" class="form-control">Выберите файл для импорта</label>

                <div class="form-group">
                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                    @if ($errors->has('csv_file'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                    @endif
                </div>
                <hr>
                <div>
                    <label>
                        <input type="checkbox" name="header" checked> В файле есть заголовки?
                    </label>
                </div>
                <div>
                    <button type="submit" class="btn btn-primary">
                        Прочесть файл
                    </button>
                </div>
            </div>
        </form>
        </div>
        <div class="col-lg-6">
            <h3 class="text-center">Инструкция</h3>
            <ol>
                <li>Загрузите файл- пример по ссылке: <a href="{{url('members/'.Hashids::encode($alliance->id).'/values/import/download')}}">ПРИМЕР</a> </li>
                <li>Значение поля "alliance_id" для каждой записи одинаковое </li>
                <li>Значение поля "alliance_id" для Вашего кооператива: <big><u><strong>{{$alliance->id}}</strong></u></big> </li>
                <li>Значение поля "alliance_users_id" - ID члена кооператива</li>

            </ol>
        </div>
    </div>
@endsection
