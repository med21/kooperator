<footer class="text-center">
    <p class="mb-2">
        <small>{{date('Y')}}. @lang('main.copyrights')</small>
    </p>

    <small>
        <a href="{{url('/')}}" class="m-2 btn ">@lang('main.press')</a>
        <a href="{{url('/terms')}}" class="m-2 btn ">@lang('main.terms')</a>
        <a href="{{url('/policy')}}" class="m-2 btn ">@lang('main.privacy')</a>
    </small>
</footer>

<!-- jQuery and Bootstrap -->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<!-- Plugins JS -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<!-- Custom JS -->
<script src="{{asset('js/script.js')}}"></script>