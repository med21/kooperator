@include('auth.header')
    <aside class="mx-auto" style="max-width: 500px; padding-top: 28vh;">
        @include('layouts.errors')
        <div class="card h-100">
            <article class="card-body">
                <a href="{{route('register')}}" class="float-right btn btn-outline-primary">@lang('menu.registration_m')</a>
                <h4 class="card-title mb-4 mt-1">@lang('auth.enter')</h4>
                <p>
                    <a href="{{url('/login/twitter')}}" class="btn btn-block btn-outline-info "> <i class="fab fa-twitter"></i>  @lang('auth.twitter')</a>
                    <a href="{{url('/login/facebook')}}" class="btn btn-block btn-outline-primary"> <i class="fab fa-facebook-f"></i>   @lang('auth.facebook')</a>
                    <a href="{{url('/login/google')}}" class="btn btn-block btn-outline-primary"> <i class="fab fa-google"></i>   @lang('auth.google')</a>
                </p>
                <hr>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input name="email" class="form-control" placeholder="@lang('auth.email')" type="email">
                    </div> <!-- form-group// -->
                    <div class="form-group">
                        <input name="password" class="form-control" placeholder="******" type="password" >
                    </div> <!-- form-group// -->
                    <div class="form-group">
                        <label>@lang('auth.remember_me')</label>
                        <input class="form-control" type="checkbox" name="remember">
                    </div> <!-- form-group// -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">@lang('auth.go')</button>
                            </div> <!-- form-group// -->
                        </div>
                        <div class="col-md-6 text-right">
                            <a class="small" href="{{route('password.request')}}">@lang('auth.forget_pass')</a>
                        </div>
                    </div> <!-- .row// -->
                </form>
            </article>
        </div> <!-- card.// -->

    </aside> <!-- col.// -->
@include('auth.footer')