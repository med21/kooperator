@include('auth.header')
<script type="text/javascript">
     function ask_tw (){
         if (confirm("@lang('auth.register_social_alert')")) {
             location.replace("{{url('/login/twitter')}}");
         } else {

         }
    }
     function ask_fb (){
         if (confirm("@lang('auth.register_social_alert')")) {
             location.replace("{{url('/login/facebook')}}");
         } else {

         }
     }
     function ask_g (){
         if (confirm("@lang('auth.register_social_alert')")) {
             location.replace("{{url('/login/google')}}");
         } else {

         }
     }
</script>
<div class="card bg-light">
    <article class="card-body mx-auto" style="max-width: 500px; padding-top: 10vh">
        <h4 class="card-title mt-3 text-center">@lang('auth.register_acc')</h4>
        <p class="text-center">@lang('auth.register_acc_slug')<br></p>
        <p class="divider-text">
            <span class="bg-light">@lang('auth.register_acc_promo')</span>
        </p>
        <p>
            <a onclick="ask_tw()" href="#" class="btn btn-block btn-twitter "> <i class="fab fa-twitter"></i> @lang('auth.register_twitter')</a>
            <a onclick="ask_fb()" href="#" class="btn btn-block btn-facebook "> <i class="fab fa-facebook-f"></i> @lang('auth.register_facebook')</a>
            <a onclick="ask_g()" href="#" class="btn btn-block btn-google btn-outline-danger "> <i class="fab fa-google"></i> @lang('auth.register_google')</a>
        </p>
        <p class="divider-text">
            <span class="bg-light">@lang('auth.or')</span>
        </p>
        @include('layouts.errors')
        <form method="post" action="{{route('register')}}">
            @csrf
            <input type="hidden" name="t" value="{{ request('t', 0) }}">
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.surname')" type="text" name="surname" value="{{ old('surname', '') }}">
            </div> <!-- form-group// -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.name')" name="name" type="text" value="{{ old('name', '') }}">
            </div> <!-- form-group// -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.middle_name')" name="middle_name" type="text" value="{{ old('middle_name', '') }}">
            </div> <!-- form-group// -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.email')" name="email" type="email" value="{{ old('email', '') }}">
            </div> <!-- form-group// -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                </div>
                <input class="form-control" value="+380" disabled="true" style="max-width: 80px;">
                <input class="form-control" placeholder="@lang('auth.phone')" name="tel" type="text" value="{{ old('tel', '') }}">
            </div>
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.pass')" type="password" name="password">
            </div> <!-- form-group// -->
            <div class="form-group input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control" placeholder="@lang('auth.pass_ver')" name="password_confirmation" type="password">
            </div> <!-- form-group// -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">@lang('auth.go')</button>
            </div> <!-- form-group// -->
            <p class="text-center">@lang('auth.registered') <a href="{{route('login')}}"> @lang('auth.enter')</a> </p>
        </form>
    </article>
</div> <!-- card.// -->
<article class="bg-secondary mb-1">
    <div class="card-body text-center">
       @include('auth.footer')
    </div>
    <br><br>
</article>