<!DOCTYPE html>
<html lang="en">
	<head>
	    @include('layouts.header')

	    @include('layouts.style')
	    @yield('styleAdic')
	</head>
	<body >
	    <section>
	        @include('layouts.sidebar')
			<div class="content">
				<div class="container-fluid">
					<div class="row">
	        @yield('content')
					</div>
				</div>
			</div>
	        @include('layouts.footer')

	    </section>
	    @include('layouts.script')
	    @yield('scriptAdic')
	</body>
</html>
