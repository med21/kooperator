<nav class="navbar navbar-" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-center">
            <li><a href="/members/{{ Hashids::encode($alliance->id) }}/profile/{{Hashids::encode($user->id)}}" title="Профиль"><span class="glyphicon glyphicon-user"></span> Профиль</a></li>
            <li><a href="/members/{{ Hashids::encode($alliance->id) }}/chart/{{Hashids::encode($user->id)}}"><span class="glyphicon glyphicon-stats"></span> График потребления</a></li>
            <li><a href="/members/{{ Hashids::encode($alliance->id) }}/values/{{Hashids::encode($user->id)}}/fee/index"><span class="glyphicon glyphicon-money"></span> Обязательные платежи<span class="label label-danger">{{$fees_c}}</span></a></li>
        </ul>
    </div>
</nav>
<style>
    .navbar-nav.navbar-center {
        position: absolute;
        left: 50%;
        transform: translatex(-50%);
    }
</style>