<!-- Styles -->
{{-- <link href="{{ asset('/css/app.css') }}" rel="stylesheet"> --}}

<link href="{{ asset('/lib/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet"> 
<link href="{{ asset('/css/stylemordor.css') }}" rel="stylesheet">
<link href="{{ asset('/css/table.css') }}" rel="stylesheet">
<link href="{{ asset('/lib/DataTables-1.10.16/css/jquery.dataTables.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
