<nav class="navbar navbar-" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-center">
            <li><a href="/members/{{ Hashids::encode($alliance->id) }}/values/import/users/index" title="Импортировать пользователей"><span class="glyphicon glyphicon-user"></span> Импортировать пользователей</a></li>
            <li><a href="/members/{{ Hashids::encode($alliance->id) }}/values/import/index" title="Импортировать показания"><span class="glyphicon glyphicon-stats"></span> Импортировать показания </a></li>
            <li> <a href="{{url('members/'.Hashids::encode($alliance->id).'/create')}}" title="Добавить пользователя"><i class="glyphicon glyphicon-plus"></i>Добавить пользователя</a></li>
            <li> <a href="{{url('members/'.Hashids::encode($alliance->id).'/fee/create')}}" title="Добавить пользователя"><i class="glyphicon glyphicon-plus"></i>Создать платеж</a></li>
        </ul>
    </div>
</nav>
<style>
    .navbar-nav.navbar-center {
        position: relative;
        left: 40%;
        transform: translatex(-40%);
    }
</style>