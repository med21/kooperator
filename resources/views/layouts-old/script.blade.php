<script src="{{ asset('/lib/jquery-3/jQuery-v3-1-1.min.js') }}"></script>
<script src="{{ asset('/lib/bootstrap-3/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/lib/DataTables-1.10.16/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/lib/Responsive-2.2.1/js/dataTables.responsive.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable();
    });
    $('#table').DataTable( {
        language: {
            "processing": "Подождите...",
            "search": "Поиск:",
            "lengthMenu": "Показать _MENU_ записей",
            "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "infoEmpty": "Записи с 0 до 0 из 0 записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "infoPostFix": "",
            "loadingRecords": "Загрузка записей...",
            "zeroRecords": "Записи отсутствуют.",
            "emptyTable": "В таблице отсутствуют данные",
            "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
            },
            "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
            }

        }
    } );
    // $('#users_values').DataTable( {
    //     language: {
    //         "processing": "Подождите...",
    //         "search": "Поиск:",
    //         "lengthMenu": "Показать _MENU_ записей",
    //         "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
    //         "infoEmpty": "Записи с 0 до 0 из 0 записей",
    //         "infoFiltered": "(отфильтровано из _MAX_ записей)",
    //         "infoPostFix": "",
    //         "loadingRecords": "Загрузка записей...",
    //         "zeroRecords": "Записи отсутствуют.",
    //         "emptyTable": "В таблице отсутствуют данные",
    //         "paginate": {
    //             "first": "Первая",
    //             "previous": "Предыдущая",
    //             "next": "Следующая",
    //             "last": "Последняя"
    //         },
    //         "aria": {
    //             "sortAscending": ": активировать для сортировки столбца по возрастанию",
    //             "sortDescending": ": активировать для сортировки столбца по убыванию"
    //         }
    //
    //     }
    // } );

</script>

@yield('script')
