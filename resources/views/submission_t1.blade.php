<!doctype html>
<html lang="en">
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оплата доступа</title>
</head>
<body>
<div class="text-center" style="padding-top: 2em">
    <p class="h1 text-danger">Теперь </p>
</div>
<div class="section" id="pricing" style="padding-top: 2em">
    <div class="container">
        <div class="section-title">
            <h3>Варианты доступа</h3>
        </div>

        <div class="card-deck">
            <div class="card pricing popular">
                <div class="card-head">
                    <small class="text-primary">Годовой</small>
                    <span class="price">3578<sub>грн</sub><sub>/год</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">Полный доступ</div>
                    <div class="list-group-item">Всевозможные отчеты</div>
                    <div class="list-group-item">Импорт данных</div>
                    <div class="list-group-item">Оплата картой</div>
                    <div class="list-group-item">Личный кабинет для каждого</div>

                </ul>
                <div class="card-body">
                    <form  accept-charset="utf-8" action="/payment/liqpay/access" method="POST"/>
                    <input type="hidden" name="amount" value="3578" />
                    <input type="hidden" name="type" value="subscription" />
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
                    <input type="hidden" name="alliance_id" value="{{auth()->user()->allianceID()->id}}" />
                    <input type="hidden" name="order_id" value="{{str_pad(auth()->user()->id + 2, 8, "0", STR_PAD_LEFT)}}" />
                    <input type="hidden" name="description" value="Сплата за користування сервісом КооператоR аккаут кооператива Ялинка до {{\Carbon\Carbon::now()->addYear(1)->format('Y-m-d')}}" />
                    <input type="hidden" name="date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" />
                    <input type="hidden" name="phone" value="+380{{auth::user()->tel}}" />
                    <input type="hidden" name="sender_first_name" value="{{auth::user()->name}}" />
                    <input type="hidden" name="sender_last_name" value="{{auth::user()->surname}}" />
                    <input type="hidden" name="email" value="{{auth::user()->email}}" />
                    <button type="submit" class="btn btn-success btn-lg btn-block">Оплатить через LiqPay.</button>
                    <small class="text-center">(+2.75% комиссия)</small>
                    </form>
                </div>
            </div>
            <div class="card pricing popular">
                <div class="card-head">
                    <small class="text-primary">Помесячно</small>
                    <span class="price">350<sub>грн/м</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">Помесячная оплата</div>
                    <div class="list-group-item">Всевозможные отчеты</div>
                    <div class="list-group-item">Импорт данных</div>
                    <div class="list-group-item">Личный кабинет для каждого</div>
                    <div class="list-group-item"><del>Оплата картой</del></div>
                </ul>
                <div class="card-body">
                    <form  accept-charset="utf-8" action="/payment/liqpay/access" method="POST"/>
                    <input type="hidden" name="amount" value="" />
                    <input type="hidden" name="alliance_id" value="" />
                    <input type="hidden" name="order_id" value="" />
                    <input type="hidden" name="description" value="Сплата за електроенергію за {{\Carbon\Carbon::now()->format('Y-m-d')}}" />
                    <input type="hidden" name="date" value="" />
                    <input type="hidden" name="phone" value="380" />
                    <input type="hidden" name="sender_first_name" value="" />
                    <input type="hidden" name="sender_last_name" value="" />
                    <input type="hidden" name="email" value="" />
                    <button type="submit" class="btn btn-success btn-lg btn-block">Оплатить через LiqPay.</button>
                    <small class="text-center">(+2.75% комиссия)</small>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // end .pricing -->
</body>
</html>
