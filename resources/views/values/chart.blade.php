@extends('layouts.master')
@section('content')

    <a href="/members/{{ Hashids::encode($alliance->id) }}/chart/{{Hashids::encode($user->id)}}/?year=2017"
       class="btn btn-default">2017</a>
    <a href="/members/{{ Hashids::encode($alliance->id) }}/chart/{{Hashids::encode($user->id)}}/?year=2018"
       class="btn btn-default">2018</a>
    <hr>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <div class="col-lg-12">
        <div id="chart_div" class="chart"></div>
    </div>
    <script>
        google.charts.load('current', {packages: ['corechart', 'bar'], 'language': 'ru'});
        google.charts.setOnLoadCallback(drawMultSeries);

        function drawMultSeries() {
            var data = new google.visualization.DataTable();
            data.addColumn('date', 'Дата');
            data.addColumn('number', 'Дневной тариф');
            @if($night)
            data.addColumn('number', 'Ночной тариф');
            @endif

            data.addRows([
                    @for($i = 1; $i <= 12; $i++)
                [
                    {v: new Date({{$year}}, {{$i}}, 01)},
                    {{ $values[$i]->day ?? $values["0{$i}"]->day ?? 0 }},
                    @if($night){{ $values[$i]->night ?? $values["0{$i}"]->night ?? 0 }}@endif
                ],
                @endfor
            ]);

            var options = {
                title: '@lang('alliance.alliance_member_chart_header')',
                @if($night)
                isStacked: true,
                @endif
                hAxis: {
                    title: '@lang('alliance.alliance_member_chart_r_header')'
                },
                vAxis: {
                    title: '@lang('alliance.alliance_member_chart_v_header')',
                    viewWindow: {
                        min: 0,
                        max: {{ $max }},
                    }
                }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }

        function resize () {
            // change dimensions if necessary
            chart.draw(data, options);
        }
        if (window.addEventListener) {
            window.addEventListener('resize', resize);
        }
        else {
            window.attachEvent('onresize', resize);
        }
    </script>
@endsection

@section('styleCoOp')
    <style>
        .chart {
            width: 100%;
            min-height: 450px;
        }

        .row {
            margin: 0 !important;
        }
    </style>
@endsection
