@extends('layouts.master')
@section('content')
    <div class="col-md-12">
    @include('layouts.errors')
    @include('layouts.success')
    <!-- begin page-header -->
        <h1 class="page-header" style="padding-left: 30px;">@lang('values.add_header')
            <small><b>№ {{$user->alliance_user_counter_number}}</b></small>
        </h1>
        <!-- end page-header -->
        <form class="form form-vertical" name="addValue" method="POST"
              action="{{ action('AllianceUsersValuesController@store',[Hashids::encode($alliance->id),Hashids::encode($user->id)])}}">
            {{ csrf_field() }}
            <input type="hidden" name="alliance_users_id" value="{{$user->id}}">
            <input type="hidden" class="form-control" id="lastdate" name="lastdate"
                   value="{{$data->alliance_users_date_check ?? \Carbon\Carbon::now()->format('Y-m-d') }}">
            <input type="hidden" class="form-control" id="alliance_id" name="alliance_id"
                   value="{{$alliance->id}}">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="last_val_d">@lang('values.add_prev_val_d')</label>
                    <input type="number" class="form-control" id="last_val_d" name="last_val_d"
                           value="{{ $data->alliance_users_val_d ?? 0}}" readonly>
                </div>
            </div>
            @if($user->alliance_user_counter_type != 1)
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="">@lang('values.add_prev_val_n')</label>
                        <input type="number" class="form-control" id="last_val_n" name="last_val_n"
                               value="{{ $data->alliance_users_val_n ?? 0}}" readonly>
                    </div>
                </div>
            @endif
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alliance_users_date_interval">@lang('values.add_days_int')</label>
                    <input type="number" class="form-control" id="alliance_users_date_interval"
                           name="alliance_users_date_interval"
                           value="" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="alliance_users_val_d">@lang('values.add_cur_val_d')</label>
                    <input type="number" class="form-control" id="alliance_users_val_d"
                           name="alliance_users_val_d"
                           placeholder="120кВт" onblur="clc()" autofocus>
                </div>
            </div>
            @if($user->alliance_user_counter_type != 1)
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="alliance_users_val_n">@lang('values.add_cur_val_n')</label>
                        <input type="number" class="form-control" id="alliance_users_val_n"
                               name="alliance_users_val_n"
                               placeholder="120кВт">
                    </div>
                </div>
            @endif
            <div class="col-md-6">
                <div class="form-group ">
                    <label for="button">@lang('values.add_calc')</label>
                    <button type="button" class="btn btn-primary" id="button"
                            onclick="calcValues()"
                            style="width: 100%;">
                        @lang('values.add_calc')
                    </button>
                </div>
            </div>
            <div id="form" style="display: none;">
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_users_consumption_d">@lang('values.add_cons_val_d')</label>
                        <input type="text" class="form-control" id="alliance_users_consumption_d"
                               name="alliance_users_consumption_d"
                               placeholder="120кВт" readonly="true">
                    </div>
                    @if($user->alliance_user_counter_type != 1)
                        <div class="form-group">
                            <label for="alliance_users_consumption_n">@lang('values.add_cons_val_n')</label>
                            <input type="text" class="form-control"
                                   id="alliance_users_consumption_n"
                                   name="alliance_users_consumption_n"
                                   placeholder="120кВт" readonly="true">
                        </div>
                    @endif
                </div>
                <div class=" col-md-6">
                    <div class="form-group">
                        <label for="alliance_users_payment_d">@lang('values.add_price_d')</label>
                        <input type="text" class="form-control" id="alliance_users_payment_d"
                               name="alliance_users_payment_d"
                               placeholder="201 грн" readonly="true">
                    </div>
                </div>
                @if($user->alliance_user_counter_type != 1)
                    <div class=" col-md-6">
                        <div class="form-group ">
                            <label for="alliance_users_payment_n">@lang('values.add_price_n')</label>
                            <input type="text" class="form-control" id="alliance_users_payment_n"
                                   name="alliance_users_payment_n"
                                   placeholder="201 грн" readonly="true">
                        </div>
                    </div>
                @endif
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_users_payment_total_debt">@lang('values.add_total')</label>
                        <input type="text" class="form-control"
                               id="alliance_users_payment_total_debt"
                               name="alliance_users_payment_total_debt">
                        <input type="hidden" class="form-control" id="alliance_users_payment_total"
                               name="alliance_users_payment_total">
                    </div>
                </div>
                <div class=" col-md-6">
                    <div class="form-group ">
                        <label for="alliance_users_date_check">@lang('values.add_date')</label>
                        <input id="date" class="form-control" type="date"
                               id="alliance_users_date_check" name="alliance_users_date_check"
                               value="{{ \Carbon\Carbon::now()->format('Y-m-d')}}">
                    </div>
                </div>
                <div class="text-center">
                    <button id="submit" class="btn btn-primary" type="submit">
                        @lang('values.add_add')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scriptCoOp')
    <script language="javascript">
        $('form[name=addValue] div#form').hide();

        function calcValues() {
            var last_val_d = $('#last_val_d').val();
            var alliance_users_val_d = $('#alliance_users_val_d').val();
            var min_d = alliance_users_val_d - last_val_d;
            var paym_d = min_d * {{ $tariff_d }};
            var total_paym = paym_d;

            $('#alliance_users_consumption_d').val(min_d);
            $('#alliance_users_payment_d').val(Number.parseFloat(paym_d).toFixed(2));

                    @if($user->alliance_user_counter_type != 1)
            var last_val_n = $('#last_val_n').val();
            var alliance_users_val_n = $('#alliance_users_val_n').val();
            var min_n = alliance_users_val_n - last_val_n;
            var paym_n = min_n * {{ $tariff_n }};
            total_paym = paym_d + paym_n;

            $('#alliance_users_consumption_n').val(min_n);
            $('#alliance_users_payment_n').val(Number.parseFloat(paym_n).toFixed(2));
            @endif

            $('#alliance_users_payment_total').val(Number.parseFloat(total_paym).toFixed(2));
            $('#alliance_users_payment_total_debt').val(Number.parseFloat(total_paym).toFixed(2));

            $('form[name=addValue] div#form').show();
        }
    </script>
    <script language="JavaScript">
        function clc() {
            var oneDay = 24 * 60 * 60 * 1000;    // hours*minutes*seconds*milliseconds
            var firstDate = new Date(document.addValue.alliance_users_date_check.value);
            var secondDate = new Date(document.addValue.lastdate.value);

            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));

            $('#alliance_users_date_interval').val(diffDays);

            console.log(diffDays);

        }
    </script>
@endsection