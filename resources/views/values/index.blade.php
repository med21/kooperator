@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    @include('alliance.notifications.messages')
    <div class="col-lg-12">
        <h1 class="page-header" style="padding-left: 30px;">
            @if(auth()->user()->hasRole('alliance_chef'))
                <a class="btn btn-primary"
                   href="{{ url('members/'.Hashids::encode($alliance->id).'/values/'.Hashids::encode($user->id)).'/add' }}"
                   title="Добавить показания"><span
                            class="glyphicon glyphicon-plus-sign"></span></a>
            @endif
            @lang('values.name')
            <small><b>№ {{$user->alliance_user_counter_number}}</b></small>
        </h1>
    </div>
    <!-- end page-header -->
    <div class="content table-responsive table-full-width">
        <table class="display table table-striped table-bordered" id="users_table">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('values.table_date')</th>
                <th><strong>@lang('values.table_cons_val_d')</strong></th>
                <th>Показания день</th>
                <th><b>@lang('values.table_cons_val_n')</b></th>
                <th>Показания ночь</th>
                @if($debt < 0)
                    <th>@lang('values.table_paid')</th>
                    <th>@lang('values.table_to_pay')</th>
                @else
                    <th>@lang('values.table_paid')</th>
                @endif
                @if($debt < 0)
                    <th>@lang('values.table_debt')</th>
                    <th>@lang('values.table_actions')</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($value as $key => $v)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$v->alliance_users_date_check}}</td>
                    <td>{{$v->alliance_users_consumption_d}}</td>
                    <td>{{$v->alliance_users_val_d}}</td>
                    <td>{{$v->alliance_users_consumption_n}}</td>
                    <td>{{$v->alliance_users_val_n}}</td>
                    <td>{{$v->alliance_users_payment_total_debt}}</td>
                    @if($debt < 0)
                        <td>@if(($v->alliance_users_payment_total_debt-$v->alliance_users_payment_total) < 0){{$v->alliance_users_payment_total_debt-$v->alliance_users_payment_total}}  @endif</td>
                        <td>@if(($v->alliance_users_payment_total_debt-$v->alliance_users_payment_total) < 0){{$v->alliance_users_payment_total_debt-$v->alliance_users_payment_total}}  @endif</td>
                        <td>@if($v->alliance_users_payment_total > $v->alliance_users_payment_total_debt)
                                <a class="btn btn-xs btn-success" title="Оплатить долг"
                                   data-toggle="modal" data-target="#pay_debt{{$key}}"><span
                                            class="glyphicon glyphicon-check"></span> @lang('values.table_debt_pay')</a>
                                <div class="modal fade" id="pay_debt{{$key}}" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close"
                                                        data-dismiss="modal">&times;
                                                </button>
                                                <h4 class="modal-title">@lang('values.table_pay_debt')</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group ">
                                                    <label for="alliance_users_payment_n">@lang('values.table_debt')</label>
                                                    <input type="text" class="form-control"
                                                           id="alliance_users_payment_n"
                                                           placeholder="201 грн" readonly="true"
                                                           value="{{$v->alliance_users_payment_total - $v->alliance_users_payment_total_debt}}">
                                                </div>
                                                <div class="form-group ">
                                                    <label for="alliance_users_payment_total">@lang('values.table_to_pay')</label>
                                                    <input type="text" class="form-control"
                                                           id="alliance_users_payment_total"
                                                           name="alliance_users_payment_total"
                                                           placeholder="{{$v->alliance_users_payment_total - $v->alliance_users_payment_total_debt}}">
                                                </div>
                                                @if(!is_null($alliance->getPayout()))
                                                    <form accept-charset="utf-8" action="/payment/liqpay/form"
                                                          method="POST">
                                                        <input type="hidden" name="amount"
                                                               value="{{$v->alliance_users_payment_total - $v->alliance_users_payment_total_debt}}"/>
                                                        <input type="hidden" name="alliance_id"
                                                               value="{{$alliance->id}}"/>
                                                        <input type="hidden" name="user_id"
                                                               value="{{$user->id}}"/>
                                                        <input type="hidden" name="type"
                                                               value="electricity"/>
                                                        <input type="hidden" name="order_id" value="{{$v->id}}"/>
                                                        <input type="hidden" name="description"
                                                               value="Сплата за електроенергію {{$user->alliance_user_surname}} {{$user->alliance_user_name}} {{ $user->alliance_user_middlename }} за {{\Carbon\Carbon::parse($v->alliance_users_date_check)->format('Y-F')}}"/>
                                                        <input type="hidden" name="date"
                                                               value="{{$v->alliance_users_date_check}}"/>
                                                        <input type="hidden" name="phone"
                                                               value="380{{$user->alliance_user_tel}}"/>
                                                        <input type="hidden" name="sender_first_name"
                                                               value="{{$user->alliance_user_surname}}"/>
                                                        <input type="hidden" name="sender_last_name"
                                                               value="{{$user->alliance_user_name}}"/>
                                                        <input type="hidden" name="email"
                                                               value="{{$user->alliance_user_email}}"/>
                                                        <button type="submit"
                                                                class="btn btn-success btn-lg btn-block">
                                                            @lang('values.table_pay_liqpay')
                                                        </button>
                                                        <small> @lang('values.table_pay_liqpay_commission')</small>
                                                    </form>
                                                    <form method="post"
                                                          action="{{url('members/'.Hashids::encode($v->alliance_id).'/values/'.Hashids::encode($v->alliance_users_id).'/pay_debt/'.Hashids::encode($v->id))}}">
                                                        @csrf
                                                        <input type="text" class="form-control"
                                                               name="alliance_users_payment_total"
                                                               placeholder="{{$v->alliance_users_payment_total - $v->alliance_users_payment_total_debt}}">
                                                        <button type="submit"
                                                                class="btn btn-success btn-lg btn-block">
                                                            @lang('values.table_pay_cash')
                                                        </button>
                                                    </form>
                                                @else
                                                    <form method="post"
                                                          action="{{url(Hashids::encode($v->alliance_users_id).'/pay_debt/'.Hashids::encode($v->id))}}">
                                                        @csrf
                                                        <input type="text" class="form-control"
                                                               name="alliance_users_payment_total"
                                                               placeholder="{{$v->alliance_users_payment_total - $v->alliance_users_payment_total_debt}}">
                                                        <button type="submit"
                                                                class="btn btn-success btn-lg btn-block">
                                                            @lang('values.table_pay_cash')
                                                        </button>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>{{$cons_d}}</td>
                <td></td>
                <td>{{$cons_n}}</td>
                <td></td>
                <td>{{$paid}}</td>
                @if($debt < 0)
                    <td>{{$debt}}</td>
                    <td>{{$debt}}</td>
                    <td></td>
                @endif
            </tr>
            </tfoot>
        </table>
    </div>

@endsection

@section('scriptCoOp')
    <script>
        $(document).ready(function () {
            var table = $('#users_table').DataTable({

                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            })
                    @if($user->alliance_user_counter_type == 1)
            var column = table.column('3');
            column.visible(!column.visible());
                    @endif
                    @if(Auth::user()->hasRole('alliance_user'))
            var column2 = table.column('6');
            column2.visible(!column2.visible());
            @else
            column2.visible(column2.visible());
            @endif

        });
    </script>
@endsection
@section('styleCoOp')
    <style>
        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #333;
            color: white;
            font-weight: bold;
        }

        td, th {
            padding: 6px;
            border: 1px solid #ccc;
            text-align: left;
        }

        /*
        Max width before this PARTICULAR table gets nasty
        This query will take effect for any screen smaller than 760px
        and also iPads specifically.
        */
        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px) {

            /* Force table to not be like tables anymore */
            table, thead, tbody, tfoot, th, td, tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr {
                border: 1px solid #ccc;
            }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }

            /*
            Label the data
            */
            td:nth-of-type(1):before {
                content: "№";
            }

            td:nth-of-type(2):before {
                content: "@lang('values.table_date')";
            }

            td:nth-of-type(3):before {
                content: "@lang('values.table_val_d')";
            }

            td:nth-of-type(4):before {
                content: "@lang('values.table_val_n')";
            }

            td:nth-of-type(5):before {
                content: "@lang('values.table_paid')";
            }

            @if($debt < 0)
            td:nth-of-type(6):before {
                content: "@lang('values.table_debt')";
            }

            td:nth-of-type(7):before {
                content: "@lang('values.table_actions')";
            }

        @endif

        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px;
            }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

        @media (min-width: 768px) {

        }

        #page-wrapper .breadcrumb {
            padding: 8px 15px;
            margin-bottom: 20px;
            list-style: none;
            background-color: #e0e7e8;
            border-radius: 0px;

        }

        @media (min-width: 768px) {
            .circle-tile {
                margin-bottom: 30px;
            }
        }

        .circle-tile {
            margin-bottom: 15px;
            text-align: center;
        }

        .circle-tile-heading {
            position: relative;
            width: 80px;
            height: 80px;
            margin: 0 auto -40px;
            border: 3px solid rgba(255, 255, 255, 0.3);
            border-radius: 100%;
            color: #fff;
            transition: all ease-in-out .3s;
        }

        .dark-blue {
            background-color: #34495e;
        }

        .dark-green {
            background-color: green;
        }

        .dark-red {
            background-color: red;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-heading .fa {
            line-height: 80px;
        }

        .circle-tile-content {
            padding-top: 50px;
        }

        .circle-tile-description {
            text-transform: uppercase;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-number {
            padding: 5px 0 15px;
            font-size: 26px;
            font-weight: 700;
            line-height: 1;
        }

        .circle-tile-footer {
            display: block;
            padding: 5px;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.1);
            transition: all ease-in-out .3s;
        }

        .circle-tile-footer:hover {
            text-decoration: none;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.2);
        }
    </style>
@endsection





