@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    <div class="text-center">
        @if($length == 0)
            <p class="h2 text-danger">@lang('sub.text_1')</p>
        @elseif($length < 90)
            <p class="h1 text-warning">@lang('sub.text_2') <span class="text-danger"><b><u>{{$length}}</u></b></span>  @lang('sub.text_2_2')</p>
        @elseif($length < 61)
            <p class="h1 text-danger">@lang('sub.text_3'){{$length}} @lang('sub.text_3_2')</p>

        @endif
    </div>
    @if($length >=90)
        <div class="section" id="pricing">
            <div class="container">
                <p class="h1 text-center text-success">@lang('sub.text_4') <b><u>{{$length}}</u></b> @lang('sub.text_4_2')</p>
                <p class="h1 text-center text-success">@lang('sub.text_5')</p>
                <p class="h1 text-center text-success">@lang('sub.text_6')</p>
            </div>
        </div>
    @else
        <div class="section" id="pricing">
            <div class="container">
                <div class="card-deck">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="card pricing popular">
                            <div class="card-head">
                                <small class="text-primary">@lang('sub.card_header')</small>
                                <span class="price">{{$price}}
                                @lang('sub.card_year')
                                </span>
                            </div>
                            <ul class="list-group list-group-flush">
                                <div class="list-group-item">@lang('sub.option_1')</div>
                                <div class="list-group-item">@lang('sub.option_2')</div>
                                <div class="list-group-item">@lang('sub.option_3')</div>
                                <div class="list-group-item">@lang('sub.option_4')</div>
                                <div class="list-group-item">@lang('sub.option_5')</div>

                            </ul>
                            <div class="card-body">
                                <form accept-charset="utf-8" action="/payment/liqpay/access" method="POST"/>
                                <input type="hidden" name="amount" value="{{$price}}"/>
                                <input type="hidden" name="type" value="subscription"/>
                                <input type="hidden" name="user_id" value="{{auth()->user()->id}}"/>
                                <input type="hidden" name="alliance_id" value="{{auth()->user()->allianceID()->id}}"/>
                                <input type="hidden" name="order_id"
                                       value="{{str_pad(auth()->user()->id + 2, 8, "0", STR_PAD_LEFT)}}"/>
                                <input type="hidden" name="description"
                                       value="Сплата за користування сервісом КооператоR. Aккаунт кооператива '{{$alliance->alliance_name}}' до {{\Carbon\Carbon::now()->addYear(1)->format('Y-m-d')}}"/>
                                <input type="hidden" name="date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}"/>
                                <input type="hidden" name="phone" value="+380{{auth::user()->tel}}"/>
                                <input type="hidden" name="sender_first_name" value="{{auth::user()->name}}"/>
                                <input type="hidden" name="sender_last_name" value="{{auth::user()->surname}}"/>
                                <input type="hidden" name="email" value="{{auth::user()->email}}"/>
                                <button type="submit" class="btn btn-success btn-lg btn-block">@lang('payments.pay_liqpay')
                                </button>
                                <small class="text-center">@lang('payments.pay_liqpay_commission')</small>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('styleCoOp')
    <style>

        @media (max-width: 992px) {
            .card-deck {
                -ms-flex-direction: column;
                flex-direction: column;
            }

            .card-deck .card {
                margin-bottom: 15px;
            }
        }

        .card.pricing {
            border: 1px solid #f1eef1;
        }

        .card.pricing.popular {
            border-top-width: 3px;
            border-color: #ce75b4 #faf6fb #FFF;
            box-shadow: 0px 12px 59px 0px rgba(36, 7, 31, 0.11);
            color: #633991;
        }

        .card.pricing .card-head {
            text-align: center;
            padding: 40px 0 20px;
        }

        .card.pricing .card-head .price {
            display: block;
            font-size: 45px;
            font-weight: 300;
            color: #633991;
        }

        .card.pricing .card-head .price sub {
            bottom: 0;
            font-size: 55%;
        }

        .card.pricing .list-group-item {
            border: 0;
            text-align: center;
            color: #959094;
            padding: 1.05rem 1.25rem;
        }

        .card.pricing .list-group-item del {
            color: #d9d3d8;
        }

        .card.pricing .card-body {
            padding: 1.75rem;
        }

        /*  CALL TO ACTION
        ----------------------*/

        .call-to-action {
            text-align: center;
            color: #FFF;
            margin: 3rem 0;
        }

        .call-to-action .box-icon {
            margin-left: auto;
            margin-right: auto;
            border-radius: 5px;
            transform: scale(0.85);
            margin-bottom: 2.5rem;
        }

        .call-to-action h2 {
            color: #FFF;
        }

        .call-to-action .tagline {
            font-size: 16px;
            font-weight: 300;
            color: #ffb8f6;
            max-width: 650px;
            margin: 0 auto;
        }

        .btn-light img {
            margin-right: 0.4rem;
            vertical-align: text-bottom;
        }

    </style>
@endsection