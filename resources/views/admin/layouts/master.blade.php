<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
    @include('admin.layouts.header')
    @include('admin.layouts.style')
    @yield('adminStyleCoOp')
</head>
<body>
<section>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')

</section>
@include('admin.layouts.script')
@yield('adminScriptCoOp')
</body>
</html>
