@extends('layouts.master')
@section('content')
    <div class="row">
        @if(Session::has('status'))
            <div class="alert alert-info">
                {{Session::get('status')}}
            </div>
        @endif
        <div class="panel">
            <div class="panel-body col-md-6">
                <form action="{{route('saveTelegramSettings')}}" method="post">
                    @csrf()
                    <div class="form-group">
                        <label for="api_key"></label>
                        <input type="text" class="form-control" name="api_key" id="api_key">
                    </div>
                    <div class="form-group">
                        <label for="callback_uri"></label>
                        <input type="text" class="form-control" name="callback_uri" id="callback_uri">
                    </div>
                    <button class="btn btn-primary form-control">Save</button>
                </form>
            </div>
        </div>
        <div class="panel col-md-6">
            <div class="panel-body">
                <div class="form-group">
                    <input class="form-control" value="{{$data['bot_id']}}" readonly>
                </div>
                <div class="form-group">
                    <input class="form-control" value="{{$data['user_name']}}" readonly>
                </div>
                <div class="form-group">
                    <input class="form-control" value="{{$data['bot_name']}}" readonly>
                </div>
                <form action="{{route('setWebHook')}}" method="post">
                    @csrf()
                <div class="form-group">
                    <input class="form-control" name="url">
                </div>
                    <div class="form-group">
                    <button class="form-control">Set</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection()