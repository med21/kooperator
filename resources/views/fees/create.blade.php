@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')

    <!-- begin page-header -->
    <h1 class="page-header" style="padding-left: 30px;">@lang('payments.fee_create_header')</h1>
    <!-- end page-header -->

        <form action="/members/{{Hashids::encode($alliance->id)}}/fee/create" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="alliance_user_fee_sum">@lang('payments.fee_create_amount')</label>
                <input  id="alliance_user_fee_sum" type="text" class="form-control" name="alliance_user_fee_sum">
            </div>
            <div class="form-group">
                <label for="alliance_user_pay_purpose">@lang('payments.fee_create_purpose')</label>
                <select  id="alliance_user_pay_purpose" name="alliance_user_pay_purpose" class="form-control">
                    <option>----</option>
                    <option value="membership{{date('Y')}}">@lang('payments.fee_create_option_1') {{date('Y')}}</option>
                    <option value="road_reconstruction">@lang('payments.fee_create_option_2')</option>
                    <option value="junk_evacuation">@lang('payments.fee_create_option_3')</option>
                    <option value="cleaning">@lang('payments.fee_create_option_4')</option>
                </select>
            </div>
            <div class="form-group">
                <label for="alliance_user_pay_desc">@lang('payments.fee_create_desc')</label>
                <input type="text" class="form-control" name="alliance_user_pay_desc" id="alliance_user_pay_desc">
            </div>
            <input type="hidden" name="alliance_user_pay_status" value="0">
            <button type="submit" class="btn btn-default">@lang('payments.fee_create')</button>
        </form>
@endsection