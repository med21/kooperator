@extends('layouts.master')
@section('content')
    @include('layouts.errors')
    @include('layouts.success')
    <!-- begin #content -->
    <h1 style="padding-left: 30px;">@lang('payments.header')</h1>
    <!-- end page-header -->
    <table class="table" id="fees_table">
        <thead>
        <tr>
            <td>#</td>
            <td>@lang('payments.table_amount')</td>
            <td>@lang('payments.table_purpose')</td>
            <td>@lang('payments.table_desc')</td>
            <td>@lang('payments.table_date')</td>
            <td>@lang('payments.table_status')</td>
            @if($fees->sum('alliance_user_pay_status') == 0)
                <td>@lang('payments.table_actions')</td>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($fees as $fee => $f)
            <tr>
                <td>{{$fee+1}}</td>
                <td>{{$f->alliance_user_fee_sum}}</td>
                <td>@if($f->alliance_user_pay_purpose == 'membership')
                        @lang('payments.membership')
                    @elseif($f->alliance_user_pay_purpose == 'road_construction')
                        @lang('payments.road')
                    @endif
                </td>
                <td>{{$f->alliance_user_pay_desc}}</td>
                <td>{{$f->alliance_user_pay_date}}</td>
                <td>@if($f->alliance_user_pay_status == 1)
                        @lang('payments.paid')
                    @elseif($f->alliance_user_pay_status == 0)
                        @lang('payments.unpaid')
                    @endif
                </td>
                @if($f->alliance_user_pay_status == 0)
                    <td>
                        @if(!is_null($alliance->getPayout()) && Auth::user()->hasRole('alliance_user'))
                            <a class="btn btn-xs btn-success" title="Оплатить долг"
                               data-toggle="modal" data-target="#pay_debt{{$fee}}"><span
                                        class="glyphicon glyphicon-check"></span> @lang('payments.pay')</a>
                            <div class="modal fade" id="pay_debt{{$fee}}" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                    data-dismiss="modal">&times;
                                            </button>
                                            <h4 class="modal-title">@lang('payments.pay_debt')</h4>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="sum">@lang('payments.table_amount')</label>
                                                <input id="sum" class="form-control" type="text"
                                                       value="{{$f->alliance_user_fee_sum}}" readonly>
                                            </div>
                                        </div>
                                        <form accept-charset="utf-8" action="/payment/liqpay/sendFeeForm"
                                              method="POST">
                                            <input type="hidden" name="amount"
                                                   value="{{$f->alliance_user_fee_sum}}"/>
                                            <input type="hidden" name="alliance_id"
                                                   value="{{$f->alliance_id}}"/>
                                            <input type="hidden" name="order_id" value="{{$f->id}}"/>
                                            <input type="hidden" name="description"
                                                   value="Сплата членських внескiв за {{$f->alliance_user_pay_desc}} pik"/>
                                            <input type="hidden" name="date"
                                                   value="{{\Carbon\Carbon::now()->format('Y-m-d')}}"/>
                                            <input type="hidden" name="phone"
                                                   value="380{{$user->alliance_user_tel}}"/>
                                            <input type="hidden" name="sender_first_name"
                                                   value="{{$user->alliance_user_surname}}"/>
                                            <input type="hidden" name="sender_last_name"
                                                   value="{{$user->alliance_user_name}}"/>
                                            <input type="hidden" name="email"
                                                   value="{{$user->alliance_user_email}}"/>
                                                    <button type="submit"
                                                            class="btn btn-sm btn-success btn-lg btn-block">
                                                        @lang('payments.pay_liqpay')
                                                    </button>
                                                    <small>@lang('payments.pay_liqpay_commission')</small>
                                        </form>
                                        @else
                                            <form method="post"
                                                  action="{{url('members/'.Hashids::encode($f->alliance_id).'/values/'.Hashids::encode($f->alliance_users_id).'/pay_debt/'.Hashids::encode($f->id))}}">
                                                @csrf
                                                <div class="col-md-8 col-md-offset-2">
                                                    <div class="form-group">
                                                        <label for="sum">@lang('payments.table_amount')</label>
                                                        <input id="sum" class="form-control" type="text"
                                                               value="{{$f->alliance_user_fee_sum}}" readonly>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control"
                                                       name="alliance_users_payment_total"
                                                       value="">
                                                <button type="submit"
                                                        class="btn btn-success btn-sm btn-lg btn-block">
                                                    @lang('payments.pay_cash')
                                                </button>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('scriptCoOp')
    <script>
        $(document).ready(function () {
            var table = $('#fees_table').DataTable({

                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            })
        });
    </script>
@endsection
@section('styleCoOp')
    <style>
        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #333;
            color: white;
            font-weight: bold;
        }

        td, th {
            padding: 6px;
            border: 1px solid #ccc;
            text-align: left;
        }

        /*
        Max width before this PARTICULAR table gets nasty
        This query will take effect for any screen smaller than 760px
        and also iPads specifically.
        */
        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px) {

            /* Force table to not be like tables anymore */
            table, thead, tbody, tfoot, th, td, tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr {
                border: 1px solid #ccc;
            }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }

            /*
            Label the data
            */
            td:nth-of-type(1):before {
                content: "№";
            }

            td:nth-of-type(2):before {
                content: "@lang('payments.table_amount')";
            }

            td:nth-of-type(3):before {
                content: "@lang('payments.table_purpose')";
            }

            td:nth-of-type(4):before {
                content: "@lang('payments.table_desc')";
            }

            td:nth-of-type(5):before {
                content: "@lang('payments.table_date')";
            }

            td:nth-of-type(6):before {
                content: "@lang('payments.table_status')";
            }
            @if($fees->sum('alliance_user_pay_status') == 0)
            td:nth-of-type(7):before {
                content: "@lang('values.table_actions')";
            }

        @endif
        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px;
            }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

        @media (min-width: 768px) {

        }

        #page-wrapper .breadcrumb {
            padding: 8px 15px;
            margin-bottom: 20px;
            list-style: none;
            background-color: #e0e7e8;
            border-radius: 0px;

        }

        @media (min-width: 768px) {
            .circle-tile {
                margin-bottom: 30px;
            }
        }

        .circle-tile {
            margin-bottom: 15px;
            text-align: center;
        }

        .circle-tile-heading {
            position: relative;
            width: 80px;
            height: 80px;
            margin: 0 auto -40px;
            border: 3px solid rgba(255, 255, 255, 0.3);
            border-radius: 100%;
            color: #fff;
            transition: all ease-in-out .3s;
        }

        .dark-blue {
            background-color: #34495e;
        }

        .dark-green {
            background-color: green;
        }

        .dark-red {
            background-color: red;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-heading .fa {
            line-height: 80px;
        }

        .circle-tile-content {
            padding-top: 50px;
        }

        .circle-tile-description {
            text-transform: uppercase;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-number {
            padding: 5px 0 15px;
            font-size: 26px;
            font-weight: 700;
            line-height: 1;
        }

        .circle-tile-footer {
            display: block;
            padding: 5px;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.1);
            transition: all ease-in-out .3s;
        }

        .circle-tile-footer:hover {
            text-decoration: none;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.2);
        }
    </style>
@endsection

