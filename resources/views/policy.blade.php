<!doctype html>
<html lang="{{App::getLocale()}}">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>КооператоR - ПОЛІТИКА КОНФІДЕНЦІЙНОСТІ ТА ЗАХИСТУ ПЕРСОНАЛЬНИХ ДАНИХ</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Сервис для садовых кооперативов, котеджных городков, осбб, и прочих обьединений.">
    <meta name="keywords"
          content="кооператив, садовое товарищество, котоджный городок, котеджы, дачный кооператив, дача">
    @include('layouts.style-main')
    <script type="text/javascript">
        calculate = function () {
            var resources = document.getElementById('qnt').value;
            if (resources < 0) {
                alert("Введенное число не мот быть отрицательны");
            } else if (resources > 0 && resources <= 49) {
                return document.getElementById('result').innerHTML = parseInt(resources) * 140;
            } else if (resources >= 50 && resources <= 99) {
                document.getElementById('result').innerHTML = parseInt(resources) * 100;
            } else if (resources >= 100 && resources < 1000) {
                document.getElementById('result').innerHTML = parseInt(resources) * 65;
            }


        }
    </script>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<!-- Nav Menu -->

<div class="nav-menu fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/logo.png')}}"
                                                                     class="img-fluid"
                                                                     alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link active" href="#home">@lang('menu.main')<span
                                            class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#opportunity">@lang('menu.ability')<span
                                            class="sr-only">(current)</span></a></li>
                            <li class="nav-item"><a class="nav-link" href="#pricing">@lang('menu.tariffs')<span
                                            class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="{{route('register')}}">@lang('menu.registration_m')</a></li>
                            <li class="nav-item"><a href="{{route('login')}}"
                                                    class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">@lang('menu.cabinet')</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<header class="bg-gradient" id="home">
</header>


<div class="section light-bg" id="opportunity">
    <div class="container">
        <div class="section-title">
            <h3>ПОЛІТИКА КОНФІДЕНЦІЙНОСТІ <br>ТА ЗАХИСТУ ПЕРСОНАЛЬНИХ ДАНИХ</h3>
            <small>
                м. Київ, Україна
                Редакція від «21» червня 2018 року
            </small>
        </div>


        <div class="tab-content">
            <div class="tab-pane fade show active" id="communication">
                <div>
                    <object data="{{asset('/doc/confidentiality.pdf')}}" type="application/pdf" width="100%" height="700px">
                        <embed src="{{asset('/doc/confidentiality.pdf')}}" type="application/pdf" width="100%" height="700px">
                        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="{{asset('/doc/confidentiality.pdf')}}">Download PDF</a>.</p>
                        </embed>
                    </object>
                </div>
            </div>
        </div>


    </div>
</div>

<!-- // end .section -->
<footer class="my-5 text-center">
    <p class="mb-2">
        <small>{{date('Y')}}. @lang('main.copyrights') </small>
    </p>

    <small>
        <a href="#" class="m-2 btn disabled">@lang('main.press')</a>
        <a href="#" class="m-2 btn disabled">@lang('main.terms')</a>
        <a href="#" class="m-2 btn disabled">@lang('main.privacy')</a>
    </small>
</footer>
@include('layouts.script-main')
</body>

</html>
