@extends('layouts.master')
@section('content')

    Кооператив - "{{$alliance->alliance_name}}" <span
            class="pull-right">ID @lang('alliance.alliance_id') <b>{{$alliance->id}}</b></span>

    @include('layouts.errors')
    @include('layouts.success')
    <a href="{{url('/members/'.Hashids::encode($alliance->id).'/create')}}"
       class="btn btn-primary">@lang('alliance.alliance_add_user')</a>
    <div class="content table-responsive table-full-width">
        <table class="display table table-striped table-bordered" id="users">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('alliance.alliance_fio')</th>
                <th>@lang('alliance.alliance_address')</th>
                <th># @lang('alliance.alliance_counter_number')</th>
                <th>@lang('alliance.alliance_counter_tariff')</th>
                <th>@lang('alliance.alliance_actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($alliance_users as $key => $entity)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$entity->alliance_user_name}} {{$entity->alliance_user_middlename}} {{$entity->alliance_user_surname}}</td>
                    <td>{{$entity->alliance_user_address}}</td>
                    <td>{{$entity->alliance_user_counter_number}}</td>
                    <td>@if($entity->alliance_user_counter_type == 1)
                            @lang('alliance.counter_edit_type_1')
                        @else
                            @lang('alliance.counter_edit_type_2')
                        @endif
                    </td>
                    <!-- begin actions-->
                    <td>
                        <!-- edit alliance-->
                        <a id="editar-alliance" class="btn btn-sm btn-primary"
                           href="{{ url('members/'.Hashids::encode($alliance->id).'/values/'.Hashids::encode($entity->id)).'/add' }}"
                           title="Добавить показания"><span class="glyphicon glyphicon-plus-sign"
                                                            aria-hidden="true"></span></a>
                        <a id="editar-alliance" class="btn btn-sm btn-warning"
                           href="{{ url('members/'.Hashids::encode($alliance->id).'/values/'.Hashids::encode($entity->id)) }}"
                           title=" показания"><span class="glyphicon glyphicon-stats"
                                                    aria-hidden="true"></span></a>
                        <a id="editar-alliance" class="btn btn-sm btn-success"
                           href="{{ url('members/'.Hashids::encode($alliance->id).'/edit/'.Hashids::encode($entity->id)) }}"
                           title="Редактировать"><span
                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                        <a id="delete-alliance" class="btn btn-sm btn-danger"
                           href="{{ url('members/'. Hashids::encode($alliance->id).'/delete/'.Hashids::encode($entity->id)) }}"
                           onclick='return confirm("Удалить?");' title="Удалить пользователя"><span
                                    class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                        <a class="btn btn-primary btn-sm" type="button" data-toggle="collapse"
                           data-target="#{{$key}}" aria-expanded="false"
                           aria-controls="">
                            @lang('alliance.alliance_ref')
                        </a>
                        @if($entity->alliance_user_telegram_id)
                        <button type="button" style="color: #0b93d5; border-color: #0b93d5" class="btn btn-sm"
                                data-toggle="modal" data-target="#sendTelegramMessage-{{Hashids::encode($entity->id)}}">
                            <i class="fab fa-telegram"></i>
                        </button>
                        <div id="sendTelegramMessage-{{Hashids::encode($entity->id)}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">@lang('telegram.modal_header_text')</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>@lang('telegram.modal_p_header_text')<br>
                                            <b>{{$entity->alliance_user_name}} {{$entity->alliance_user_middlename}} {{$entity->alliance_user_surname}}</b>
                                        </p>
                                        <form id="Tmessage" action="{{route('sendTelegramMessage')}}" method="post">
                                            <input type="hidden" name="user_id" value="{{Hashids::encode($entity->id)}}">
                                            @csrf()
                                            <div class="form-group">
                                                <label for="message">@lang('telegram.modal_text')</label>
                                                <textarea class="form-control" name="message" rows="5" cols="5" id="message"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="form-control btn" >@lang('telegram.modal_send')</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default"
                                                data-dismiss="modal">@lang('telegram.modal_close')
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif

                        <div class="collapse" id="{{$key}}">
                            <div class="card card-body">
                                <ul style="margin-top: 10px">
                                    <li>
                                        <a href="{{ url('members/'.Hashids::encode($alliance->id).'/privat/'.Hashids::encode($entity->id))}}">
                                            @lang('alliance.alliance_ref_1')</a></li>
                                </ul>
                            </div>
                        </div>

                    </td>
                    <!-- end acciones -->
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('scriptCoOp')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script>
        $(document).ready(function () {
            var table = $('#users').DataTable({
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            })
        });
    </script>
    <script type="text/javascript">
        var frm = $('#Tmessage');

        frm.submit(function (e) {

            e.preventDefault();

            $.ajax({
                type: frm.attr('method'),
                headers: {
                    "X-CSRF-TOKEN": document.getElementsByName('_token'),
                },
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        });
    </script>
@endsection
@section('styleCoOp')
    <style>
        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #333;
            color: white;
            font-weight: bold;
        }

        td, th {
            padding: 6px;
            border: 1px solid #ccc;
            text-align: left;
        }

        /*
        Max width before this PARTICULAR table gets nasty
        This query will take effect for any screen smaller than 760px
        and also iPads specifically.
        */
        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px) {

            /* Force table to not be like tables anymore */
            table, thead, tbody, tfoot, th, td, tr {
                display: block;
            }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr {
                border: 1px solid #ccc;
            }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 30%;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }

            /*
            Label the data
            */
            td:nth-of-type(1):before {
                content: "№";
            }

            td:nth-of-type(2):before {
                content: "@lang('alliance.alliance_fio')";
            }

            td:nth-of-type(3):before {
                content: "@lang('alliance.alliance_address')";
            }

            td:nth-of-type(4):before {
                content: "@lang('alliance.alliance_counter_number')";
            }

            td:nth-of-type(5):before {
                content: "@lang('alliance.alliance_counter_tariff')";
            }

            td:nth-of-type(6):before {
                content: "@lang('alliance.alliance_actions')";
            }

        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px;
            }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

        @media (min-width: 768px) {

        }

        #page-wrapper .breadcrumb {
            padding: 8px 15px;
            margin-bottom: 20px;
            list-style: none;
            background-color: #e0e7e8;
            border-radius: 0px;

        }

        @media (min-width: 768px) {
            .circle-tile {
                margin-bottom: 30px;
            }
        }

        .circle-tile {
            margin-bottom: 15px;
            text-align: center;
        }

        .circle-tile-heading {
            position: relative;
            width: 80px;
            height: 80px;
            margin: 0 auto -40px;
            border: 3px solid rgba(255, 255, 255, 0.3);
            border-radius: 100%;
            color: #fff;
            transition: all ease-in-out .3s;
        }

        .dark-blue {
            background-color: #34495e;
        }

        .dark-green {
            background-color: green;
        }

        .dark-red {
            background-color: red;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-heading .fa {
            line-height: 80px;
        }

        .circle-tile-content {
            padding-top: 50px;
        }

        .circle-tile-description {
            text-transform: uppercase;
        }

        .text-faded {
            color: rgba(255, 255, 255, 0.7);
        }

        .circle-tile-number {
            padding: 5px 0 15px;
            font-size: 26px;
            font-weight: 700;
            line-height: 1;
        }

        .circle-tile-footer {
            display: block;
            padding: 5px;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.1);
            transition: all ease-in-out .3s;
        }

        .circle-tile-footer:hover {
            text-decoration: none;
            color: rgba(255, 255, 255, 0.5);
            background-color: rgba(0, 0, 0, 0.2);
        }
    </style>
@endsection