@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Импорт пользователей коопреатива
                        <b>{{$alliance->alliance_name}}</b></div>

                    <div class="panel-body">
                        <form class="form" method="POST"
                              action="/members/{{ Hashids::encode($alliance->id) }}/values/import/users/import_parse"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group col-md-4{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                <label for="csv_file" class="form-control">Выберите файл для импорта</label>

                                <div class="form-group">
                                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>

                                    @if ($errors->has('csv_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <hr>
                                <div>
                                    <label>
                                        <input type="checkbox" name="header" checked> В файле есть заголовки?
                                    </label>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary">
                                        Прочесть файл
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>
                        HEre is link
                        <a href="/members/{{Hashids::encode($alliance->id)}}/values/import/users/download">Template</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
