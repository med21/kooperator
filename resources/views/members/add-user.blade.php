@extends('layouts.master')
@section('content')
    <h1 class="page-header" style="padding-left: 30px;">
        @lang('auth.add_new_user') <b>{{$alliance->alliance_name}}</b></small>
    </h1>
    <!-- end page-header -->
    <div class="panel-body">
        <form method="POST"
              action="{{action('AllianceUsersController@store',Hashids::encode($alliance->id))}}">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="alliance_user_surname">
                            @lang('alliance.alliance_new_user_surname')
                        </label>
                        <input class="form-control" name="alliance_user_surname" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_name')
                        </label>
                        <input class="form-control" name="alliance_user_name" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_middle_name')
                        </label>
                        <input class="form-control" name="alliance_user_middlename" type="text">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_address')
                        </label>
                        <input class="form-control" name="alliance_user_address" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_tel')
                        </label>
                        <input class="form-control" name="alliance_user_tel" type="tel">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_email')
                        </label>
                        <input class="form-control" name="alliance_user_email" type="email">

                    </div>
                </div>
            </div>
            <div class=" row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_counter_number')
                        </label>
                        <input class="form-control" name="alliance_user_counter_number" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_counter_date_check')
                        </label>
                        <input class="form-control" name="alliance_user_counter_date" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_counter_tariff')
                        </label>
                        <select name="alliance_user_counter_type" class="form-control">
                            <option value="1">@lang('alliance.alliance_new_user_counter_tariff_option_1')</option>
                            <option value="2">@lang('alliance.alliance_new_user_counter_tariff_option_2')</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_land_sq')
                        </label>
                        <input class="form-control" name="alliance_users_land_square" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_land_number')
                        </label>
                        <input class="form-control" name="alliance_users_land_doc_numb" type="text">

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_land_date')
                        </label>
                        <input class="form-control" name="alliance_users_land_doc_date" type="date">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_pensioner')
                        </label>
                        <input type="checkbox" class="form-control" value="1" id="myCheck"
                               name="alliance_users_pensioner"
                               onclick="myFunction()">
                    </div>
                </div>
                <div id="text" style="display:none" class="col-lg-4">
                    <div class="form-group">
                        <label>
                            @lang('alliance.alliance_new_user_pensioner_doc')
                        </label>
                        <input class="form-control" name="alliance_users_pensioner_number" type="text">

                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                <input name="alliance_id" type="hidden" value="{{ $alliance->id }}">
                <button class="btn btn-default waves-effect waves-light" id="save" type="submit">
                    @lang('alliance.alliance_new_user_add')
                </button>

            </div>
        </form>
    </div>
@endsection

<script>
    function myFunction() {
        var checkBox = document.getElementById("myCheck");
        var text = document.getElementById("text");
        if (checkBox.checked == true) {
            text.style.display = "block";
        } else {
            text.style.display = "none";
        }
    }
</script>







