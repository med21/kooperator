<!doctype html>
<html lang="{{App::getLocale()}}">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>КооператоR</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Сервис для садовых кооперативов, котеджных городков, осбб, и прочих обьединений.">
    <meta name="keywords" content="кооператив, садовое товарищество, котоджный городок, котеджы, дачный кооператив, дача">
    <meta property="fb:admins" content="kostia.kazydub"/>
    <meta property="fb:profile_id" content="kostia.kazydub"/>
    <meta property="fb:pages" content="kostia.kazydub"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="КооператоR"/>
    <meta property="og:description" content="Веб-сервис для кооперативов, ОСББ, ЖК"/>
    <meta property="og:image" content="https://it-hause.com/icon.png"/>
    <meta property="og:url" content="https://it-hause.com"/>
    <meta property="og:locale:locale" content="(ru,uk)"/>
@include('layouts.style-main')
    @include('layouts.script-main')
    <script type="text/javascript">
        calculate = function () {
            var resources = document.getElementById('qnt').value;
            if (resources < 0) {
                alert("Введенное число не мот быть отрицательны");
            } else if (resources > 0 && resources <= 49) {
                return document.getElementById('result').innerHTML = parseInt(resources) * 140;
            } else if (resources >= 50 && resources <= 99) {
                document.getElementById('result').innerHTML = parseInt(resources) * 100;
            } else if (resources >= 100 && resources < 1000) {
                document.getElementById('result').innerHTML = parseInt(resources) * 65;
            }


        }
    </script>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "https://it-hause.com",
  "name": "КооператоR",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+380-63-220-9795",
    "contactType": "Customer service"
  }
}
</script>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">

<!-- Nav Menu -->

<div class="nav-menu fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" class="img-fluid"
                                                                     alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                                class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link active" href="#home">@lang('menu.main')<span
                                            class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#opportunity">@lang('menu.ability')<span
                                            class="sr-only">(current)</span></a></li>
                            <li class="nav-item"><a class="nav-link" href="#pricing">@lang('menu.tariffs')<span
                                            class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link"
                                                    href="{{route('register')}}">@lang('menu.registration_m')</a></li>
                            <li class="nav-item"><a href="{{route('login')}}"
                                                    class="btn btn-outline-light my-3 my-sm-0 ml-lg-3">@lang('menu.cabinet')</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<header class="bg-gradient" id="home">
    <div class="container mt-5">
        <h1>@lang('main.header_main')</h1>
        <p class="tagline">@lang('main.slug_main')</div>
    <div class="img-holder mt-3"><img src="{{asset('images/mac_2.png')}}" alt="bar" class="img-fluid"></div>
</header>


<div class="section light-bg" id="opportunity">
    <div class="container">
        <div class="section-title">
            <small>@lang('main.speciality')</small>
            <h3>@lang('main.offer')</h3>
        </div>

        <ul class="nav nav-tabs flex-column flex-sm-row " role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#communication">@lang('main.accounting')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#schedule">@lang('main.control')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages">@lang('main.for_chef')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#livechat">@lang('main.for_user')</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="communication">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="{{asset('images/error_exclamation_256x256.png')}}" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>

                        <h2>@lang('main.accounting')</h2>
                        <p class="lead">@lang('main.simplicity')</p>
                        <p>@lang('main.simplicity_slug')</p>
                        <p>@lang('main.simplicity_content')</p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="schedule">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2>@lang('main.control_slug')</h2>
                        <p class="lead">@lang('main.control_slug_2')</p>
                        <p>@lang('main.control_content')</p>
                        <p></p>
                    </div>
                    <img src="{{asset('images/control.png')}}" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
            <div class="tab-pane fade" id="messages">
                <div class="d-flex flex-column flex-lg-row">
                    <img src="{{asset('images/envelope.png')}}" alt="graphic"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                    <div>
                        <h2>@lang('main.for_chef_slug')</h2>
                        <p class="lead">@lang('main.for_chef_slug_2')</p>
                        <p>@lang('main.for_chef_content')</p>
                        <p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="livechat">
                <div class="d-flex flex-column flex-lg-row">
                    <div>
                        <h2>@lang('main.for_user_slug')</h2>
                        <p class="lead">@lang('main.for_user_slug_2')</p>
                        <p>@lang('main.for_user_content')</p>
                        <p class="lead">@lang('main.for_user_content_2')</p>
                        <p>@lang('main.for_user_content_3')</p>
                    </div>
                    <img src="{{asset('images/pay.png')}}" alt="graphic" width="223" height="286"
                         class="img-fluid rounded align-self-start mr-lg-5 mb-5 mb-lg-0">
                </div>
            </div>
        </div>


    </div>
</div>
<!-- // end .section -->
<div class="section" id="pricing">
    <div class="container">
        <div class="section-title">
            <small>@lang('main.price')</small>
            <h3>@lang('main.price_v')</h3>
        </div>
        <div class="card-deck">
            <div class="card pricing">
                <div class="card-head">
                    <small class="text-primary">@lang('main.price_v_1')</small>
                    <span class="price">0<sub> @lang('main.price_year')</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item">@lang('main.price_v_1_time')</div>
                    <div class="list-group-item">@lang('main.price_v_1_users')</div>
                    <div class="list-group-item"><del>@lang('main.price_report')</del></div>
                    <div class="list-group-item"><del>@lang('main.price_import')</del></div>
                    <div class="list-group-item"><del>@lang('main.price_payment')</del></div>
                    <div class="list-group-item"><del>@lang('main.price_cabinet')</del></div>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-lg btn-block">Выбрать</a>
                </div>
            </div>
            <div class="card pricing popular">
                <div class="card-head">
                    <div class="card-head">
                        <small class="text-primary">@lang('main.price_v_2')</small>
                        <span class="price">3578<sub> @lang('main.price_year')</sub></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <div class="list-group-item">@lang('main.price_full_acc')</div>
                        <div class="list-group-item">@lang('main.price_50_acc')</div>
                        <div class="list-group-item">@lang('main.price_full_report')</div>
                        <div class="list-group-item">@lang('main.price_import')</div>
                        <div class="list-group-item">@lang('main.price_payment')</div>
                        <div class="list-group-item">@lang('main.price_cabinet')</div>
                    </ul>
                    <div class="card-body">
                        <a href="{{route('register')}}?t=1" class="btn btn-primary btn-lg btn-block">Выбрать</a>
                    </div>
                </div>

            </div>
            <div class="card pricing">
                <div class="card-head">
                    <small class="text-primary">@lang('main.price_v_3')</small>
                    <span class="price"><span id="result"></span><sub> @lang('main.price_year')</sub></span>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="list-group-item"><input id="qnt" type="number" min="51" onchange="calculate()"></div>
                    <div class="list-group-item">@lang('main.price_full_acc')</div>
                    <div class="list-group-item">@lang('main.price_over_50_acc')</div>
                    <div class="list-group-item">@lang('main.price_full_report')</div>
                    <div class="list-group-item">@lang('main.price_import')</div>
                    <div class="list-group-item">@lang('main.price_payment')</div>
                    <div class="list-group-item">@lang('main.price_cabinet')</div>
                </ul>
                <div class="card-body">
                    <a href="{{route('register')}}?t=2" class="btn btn-primary btn-lg btn-block">Выбрать</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // end .pricing -->
@if(!Auth::guest())
<div class="section" id="faq">
    <div class="container">
        <div class="section-title">
            <h3>Часто задаваемие вопросы</h3>
        </div>
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                           При использовании сервиса отпрадвет ли необходимость в бухгалтере?
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                       Нет, так как согласно законам Украины бухгалтер обязателен для Садового Товарищества.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                            Оплата картой?
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        да
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- Contact-->
<div class="light-bg py-5" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
                <p class="mb-2"><span class="ti-location-pin mr-2"></span>@lang('main.address')</p>
                <div class=" d-block d-sm-inline-block">
                    <p class="mb-2">
                        <span class="ti-email mr-2"></span> <a class="mr-4" href="mailto:info@it-hause.com">info@it-hause.com</a>
                    </p>
                </div>
                <div class="d-block d-sm-inline-block">
                    <p class="mb-0">
                        <span class="ti-headphone-alt mr-2"></span> <a href="tel:+380632209795">+380 63-220-97-95</a>
                    </p>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="social-icons">
                    <a href="https://www.facebook.com/myKooperatoR" target="_blank"><span class="ti-facebook"></span></a>
                    <a href="https://twitter.com/KKazydub" target="_blank"><span class="ti-twitter-alt"></span></a>
                    <a href="https://www.instagram.com/kooperator8/" target="_blank"><span class="ti-instagram"></span></a>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- // end .section -->
<footer class="my-5 text-center">
    <p class="mb-2">
        <small>{{date('Y')}}. @lang('main.copyrights') </small>
    </p>

    <small>
        <a href="{{url('https://it-hause.com')}}" class="m-2 btn">@lang('main.press')</a>
        <a href="/terms" class="m-2 btn">@lang('main.terms')</a>
        <a href="/policy" class="m-2 btn">@lang('main.privacy')</a>
    </small>
</footer>
</body>

</html>
