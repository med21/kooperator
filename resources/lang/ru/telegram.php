<?php

return [
    'modal_header_text' => 'Отправить сообщение в Telegram!',
    'modal_p_header_text' => 'Сообщение пользователю:',
    'modal_text' => 'Текст сообщения',
    'modal_send' => 'Отправить',
    'modal_close' => 'Закрыть',
    '' => '',
];