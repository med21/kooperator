<?php
return[
    'membership' => 'Членський внесок',
    'road' => 'Ремонт дороги',
    'header' => 'Обов\'язкові платежі',
    'table_amount' => 'Сума',
    'table_purpose' => 'Призначення',
    'table_desc' => 'Коментар',
    'table_date' => 'Дата',
    'table_status' => 'Статус',
    'table_actions' => 'Дії',
    'paid' => 'Сплачено',
    'unpaid' => 'Не оплачено ',
    'pay' => 'Сплатити',
    'pay_debt' => 'Сплатити борг',
    'pay_cash' => 'Готівкою',
    'pay_liqpay' => 'Сплатити через Liqpay.',
    'pay_liqpay_commission' => '(+2,75% комісія)',
    'fee_create_header' => 'Створити обов\'язковий платіж',
    'fee_create_amount' => 'Сума платежу',
    'fee_create_purpose' => 'Призначення',
    'fee_create_option_1' => 'Річний членський внесок',
    'fee_create_option_2' => 'Ремонт дороги',
    'fee_create_option_3' => 'Вивіз сміття',
    'fee_create_option_4' => 'Прибирання території',
    'fee_create_desc' => 'Опис',
    'fee_create' => 'Створити платіж',
];