<?php

return [
    'modal_header_text' => 'Надіслати повідомлдення в Telegram!',
    'modal_p_header_text' => 'Повідомлення користувачу:',
    'modal_text' => 'Текст повідомлення',
    'modal_send' => 'Надіслати',
    'modal_close' => 'Зачинити',
    '' => '',
];