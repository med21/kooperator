<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/notiff', 'HomeController@notiff');
Route::get('/policy', 'LanguageController@policy');
Route::get('/terms', 'LanguageController@terms');
Route::get('/testemail', 'HomeController@testEmail');

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);
Auth::routes();
/*Google login*/
Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');
/*Facebook login*/
Route::get('login/facebook', 'Auth\LoginController@fbredirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@fbhandleProviderCallback');
/*Twitter login*/
Route::get('login/twitter', 'Auth\LoginController@twredirectToProvider');
Route::get('login/twitter/callback', 'Auth\LoginController@twhandleProviderCallback');
/* Payment*/

//Route::any('/payment/privat24/{allianceUsersValue}','PurchaseController@showPrivat24');
Route::any('/payment/privat24/form', 'PurchaseController@sendFormPb');

Route::post('/telegrambot/'.Telegram::getAccessToken(), function () {
    Telegram::commandsHandler(true);
});

Route::any('/payment/liqpay/response', 'PurchaseController@showLiqpay');
Route::any('/payment/liqpay/response/acc', 'PurchaseController@accPayment');
Route::any('/payment/portmone/response/success', 'PortmonePayment@testPayResponse');
Route::any('/payment/portmone/response/fail', 'PortmonePayment@testPayResponseFail');
Route::any('/payment/portmone/response/getResult', 'PortmonePayment@getResult');
Route::any('/payment/portmone/sendForm', 'PortmonePayment@testPay');
Route::any('/payment/portmone/', 'PortmonePayment@index');
Route::post('/payment/liqpay/form', 'PurchaseController@sendForm');
Route::post('/payment/liqpay/access', 'PurchaseController@sendAccForm');
Route::post('/payment/liqpay/sendFeeForm', 'PurchaseController@sendFeeForm');


Route::group(['middleware' => ['auth']], function () {
    Route::resource('/home', 'UserController');

    /* Users.   */
    Route::group(['middleware' => ['role:superadmin']], function () {
        Route::resource('users', 'UserController');
        Route::get('users', 'UserController@index');
        Route::get('users/destroy/{id}', 'UserController@destroy');
        Route::post('users/{id}/update', 'UserController@update');


        /* Permissions.  */
        Route::resource('permissions', 'PermissionsController');
        Route::get('permissions/destroy/{id}', 'PermissionsController@destroy');
        Route::post('permissions/{id}/update', 'PermissionsController@update');
        Route::post('permissions/store', 'PermissionsController@store');

        /* Roles.   */
        Route::resource('roles', 'RoleController');
        Route::get('roles/destroy/{id}', 'RoleController@destroy');
        Route::post('roles/{id}/update', 'RoleController@update');
        Route::post('roles/store', 'RoleController@store');
    });
    /* Submission*/
    Route::get('alliance/{id}/order/submission', 'SubmissionController@index');
    Route::get('alliance/{id}/order/submission/renew', 'SubmissionController@renew');

    /*Delete - Account*/
    Route::get('/alliance/{id}/delete-account', 'AllianceDashBoardController@deleteAccount')->name('deleteAccount');
    Route::get('/alliance/{id}/delete', 'AllianceDashBoardController@deleteAcc')->name('deleteAcc');

    /* Alliance */
    Route::group(['middleware' => ['status']], function () {
        Route::group(['middleware' => ['role:alliance_chef', 'status']], function () {
//        Route::get('alliance/{id}/order/submission', 'SubmissionController@index');
//        Route::get('alliance/{id}/order/submission/renew', 'SubmissionController@renew');
            Route::resource('alliance', 'AllianceController');
            Route::get('alliance/{id}/dashboard/index/', 'AllianceDashBoardController@index');
            Route::get('alliance/destroy/{id}', 'AllianceController@destroy');
            //Route::post('alliance/{id}/edit', 'AllianceController@update');
            Route::post('alliance/{id}/update', 'AllianceController@update');
            Route::post('alliance/store', 'AllianceController@store');
            Route::group(['prefix' => 'alliance/{id}/counter'], function () {
                Route::get('create', 'AllianceCounterController@create');
                Route::post('store', 'AllianceCounterController@store');
                Route::get('edit', 'AllianceCounterController@edit');
                Route::post('update', 'AllianceCounterController@update');
            });
            Route::group(['prefix' => 'alliance/{id}/dashboard/'], function () {
                Route::get('add-values/{allianceCounter}', 'AllianceCounterValuesController@create');
                Route::post('add-values/{allianceCounter}', 'AllianceCounterValuesController@store');
                Route::post('pay-gov/{allianceCounter}/', 'AllianceCounterValuesController@payGov');
                Route::get('edit', 'AllianceCounterController@edit');
                Route::post('update', 'AllianceCounterController@update');
                Route::get('settings', 'AllianceDashBoardController@settings');
                Route::post('settings/save', 'AllianceDashBoardController@settingsSave');
                Route::get('settings/delete-alliance', 'AllianceDashBoardController@deleteAlliance')->name('deleteAlliance');
                /* Reports */
                Route::group(['prefix' => 'reports'], function () {
                    Route::get('/', 'ReportsController@index');
                    Route::get('/users', 'ReportsController@getAllianceUsers')->name('allianceUsers');
                    Route::get('/spending', 'ReportsController@getSpending')->name('allianceSpending');
                    Route::get('/lastkvt', 'ReportsController@lastMonthKvt')->name('allianceSpendingKvt');
                });
                /*Spending*/
                Route::group(['prefix' => 'spending'], function () {
                    Route::get('/', 'SpendingController@index');
                    Route::post('/', 'SpendingController@store')->name('spending.Store');
                });
                Route::group(['prefix' => 'notifications'], function () {
                    Route::get('/', 'NotificationMessageController@index');
                    Route::post('/', 'NotificationMessageController@store')->name('notificationCreate');
                    Route::get('/{key}/destroy', 'NotificationMessageController@destroy')->name('notificationDestroy');
                });

            });
        });


        /* Members */
        Route::group(['prefix' => 'members'], function () {
            Route::get('/{id}/profile/{allianceUsers}', 'AllianceUsersController@userProfile');
            Route::get('/{id}/api/{allianceUsers}', 'AllianceUsersController@showApiSettings');
            Route::post('/{id}/profile/{allianceUsers}', 'AllianceUsersController@update_avatar');
            Route::get('/{id}/edit/{allianceUsers}', 'AllianceUsersController@edit');
            Route::get('/{id}/chart/{allianceUsers}/', 'AllianceUsersController@chart');
            Route::get('/{id}/values/{allianceUsers}/fee/index', 'FeeController@index');
            Route::post('/{id}/values/{allianceUsers}/fee/pay/{fee}', 'FeeController@payFee');
            Route::post('/{id}/update/{allianceUsers}', 'AllianceUsersController@update');
            Route::post('/{id}/destroy/{allianceUsers}', 'AllianceUsersController@destroy');
            Route::get('/{id}/fee/create', 'FeeController@create');
            Route::post('/{id}/fee/create', 'FeeController@store');

            Route::group(['middleware' => ['role:alliance_chef']], function () {
                Route::get('/{id}', 'AllianceUsersController@index');
                Route::get('/{id}/create', 'AllianceUsersController@create');
                Route::get('{id}/delete/{allianceUsers}', 'AllianceUsersController@destroy');
                Route::post('/{id}/store', 'AllianceUsersController@store');
                Route::post('send-telegram-message','AllianceUsersController@sendTelegramMessage')->name('sendTelegramMessage');

                /*Users References*/
                Route::get('/{id}/privat/{allianceusers}{?date}', 'AllianceUsersReference@index');


            });


            /* User Values */
            Route::group(['prefix' => '{id}/values'], function () {
                Route::get('/{allianceUsers}', 'AllianceUsersValuesController@index');
                Route::get('/{allianceUsers}/notification/{key}/read', 'AllianceCounterValuesController@markAsRead');
                Route::post('/{allianceUsers}/pay_debt/{allianceUsersValues}', 'AllianceUsersValuesController@paydebt')->name('payCash');

                Route::group(['prefix' => 'import'], function () {
                    Route::get('/index', 'ImportController@getImport')->name('import');
                    Route::post('/import_parse', 'ImportController@parseImport')->name('import_parse');
                    Route::post('/import_process', 'ImportController@processImport')->name('import_process');
                    Route::get('/download', 'ValuesExportController@exportValues');
                });
                Route::group(['prefix' => '/import/users/'], function () {
                    Route::get('/index', 'ImportUsersController@getImport')->name('import_users');
                    Route::post('/import_parse', 'ImportUsersController@parseImport')->name('import_parse_users');
                    Route::post('/import_process', 'ImportUsersController@processImport')->name('import_process_users');
                    Route::get('/download', 'ValuesExportController@exportUsers');
                });

                Route::group(['middleware' => ['role:alliance_chef']], function () {
                    Route::get('/{allianceUsers}/add', 'AllianceUsersValuesController@create');
                    Route::post('/{allianceUsers}/create', 'AllianceUsersValuesController@store');
                    Route::get('/{allianceUsers}/edit', 'AllianceUsersValuesController@edit');
                    Route::post('/{allianceUsers}/update', 'AllianceUsersValuesController@update');
                });

            });
        });
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'role:superadmin'], function () {
        Route::get('/', 'AdminController@index');
        Route::get('/telegram', 'TelegramBotController@telegram')->name('telegram');
        Route::get('/telegram/telegrambot', 'TelegramBotController@setWH')->name('setWebHook');
        Route::get('/telegram/get-webhook-info', 'TelegramBotController@getWebHookInfo')->name('getWebHookInfo');
        Route::get('/telegram/settings-save', 'AdminController@saveTelegramSettings')->name('saveTelegramSettings');
    });
});