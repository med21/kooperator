<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:api')->get('/user', function (Request $request){
//    return $request->user();
//});
//
//Route::post('register', 'Api\Auth\RegisterController@register');
//Route::post('login', 'Api\Auth\LoginController@login');
//Route::post('refresh', 'Api\Auth\LoginController@refresh');
//Route::post('social_auth', 'Api\Auth\SocialAuthController@socialAuth');
//
//Route::group(['middleware' => 'auth:api'], function(){
//    Route::get('details', 'Api\Auth\LoginController@details');
//    Route::get('user-values','Api\ApiAllianceUsersValuesController@usersValues');
//    Route::get('user-fee','Api\ApiAllianceUsersValuesController@usersFee');
//    Route::get('user-latest-values','Api\ApiAllianceUsersValuesController@latestUserValues');
//    Route::post('add-new-alliance-values','Api\ApiAllianceUsersValuesController@addAllianceValues');
//    Route::delete('delete-alliance-values','Api\ApiAllianceUsersValuesController@deleteAllianceValues');
//    Route::post('new-fee','Api\ApiAllianceUsersValuesController@createFee');
//    Route::get('get-tariff','Api\ApiAllianceUsersValuesController@getTariff');
//    Route::put('edit-user-info','Api\ApiAllianceUsersValuesController@editUserInfo');
//    Route::get('alliance-credential','Api\ApiAllianceUsersValuesController@getAllianceCredential');
//});

//Route::group(['prefix' => 'v2'], function () {
//    Route::post('register', 'ApiV2\Auth\RegisterController@register');
//    Route::post('login', 'ApiV2\Auth\LoginController@login');
//    Route::post('refresh', 'ApiV2\Auth\LoginController@refresh');
//    Route::post('social_auth', 'ApiV2\Auth\SocialAuthController@socialAuth');
//
//    Route::group(['middleware' => 'auth:api'], function(){
//        Route::get('details', 'ApiV2\Auth\LoginController@details');
//        Route::get('user-values','ApiV2\ApiAllianceUsersValuesController@usersValues');
//        Route::get('user-fee','ApiV2\ApiAllianceUsersValuesController@usersFee');
//        Route::get('user-latest-values','ApiV2\ApiAllianceUsersValuesController@latestUserValues');
//        Route::post('add-new-alliance-values','ApiV2\ApiAllianceUsersValuesController@addAllianceValues');
//        Route::delete('delete-alliance-values','ApiV2\ApiAllianceUsersValuesController@dropAllValues');
//        Route::post('new-fee','ApiV2\ApiAllianceUsersValuesController@createFee');
//        Route::get('get-tariff','ApiV2\ApiAllianceUsersValuesController@getTariff');
//        Route::put('edit-user-info','ApiV2\ApiAllianceUsersValuesController@editUserInfo');
//        Route::get('alliance-credential','ApiV2\ApiAllianceUsersValuesController@getAllianceCredential');
//        Route::get('all-users','ApiV2\ApiAllianceUsersValuesController@getAllianceUsers');
//    });
//});

