<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alliance_id');
            $table->integer('alliance_user_id');
            $table->integer('alliance_user_fee_sum');
            $table->date('alliance_user_pay_date')->nullable();
            $table->string('alliance_user_pay_purpose');
            $table->string('alliance_user_pay_desc');
            $table->integer('alliance_user_pay_status');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
}
