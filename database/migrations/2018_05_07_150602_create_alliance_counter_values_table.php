<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllianceCounterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliance_counter_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alliance_id');
            $table->integer('alliance_counter_id');
            $table->integer('alliance_counter_val_d');
            $table->integer('alliance_counter_val_n')->default(0);
            $table->integer('alliance_counter_consumption_d');
            $table->integer('alliance_counter_consumption_n')->default(0);
            $table->date('alliance_counter_date_check');
            $table->integer('alliance_counter_date_interval');
            $table->float('alliance_counter_payment_d')->default(0);
            $table->float('alliance_counter_paid_gov');
            $table->float('alliance_counter_payment_n')->default(0);
            $table->float('alliance_counter_payment_total')->default(0);
            $table->float('alliance_counter_payment_total_debt');
            $table->boolean('alliance_counter_payment_is_paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alliance_counter_values');
    }
}
