<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsDebtProfit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alliance_users_values', function ($table) {
            $table->double('alliance_users_debt')->after('alliance_users_payment_total_debt');
            $table->double('alliance_users_profit')->after('alliance_users_debt');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alliance_users_values', function($table) {
            $table->double('alliance_users_debt');
            $table->double('alliance_users_profit');

        });
    }
}
