<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *user_id - користувач котрому потрібно показати сповіщення
     *notification_id - ідентифікатор сповіщення
     *status ['new', 'old', 'read', 'unread'] - статус сповіщення для користувача (прочитав, не прочитав, стара, нова)
     *date - дата, коли користувач прочитав сповіщення
     * @return void
     */
    public function up()
    {
        Schema::create('notification_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->integer('alliance_id');
            $table->integer('notification_id');
            $table->enum('status', ['read', 'unread']);
            $table->enum('priority', ['low','medium','high']);
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_users');
    }
}
