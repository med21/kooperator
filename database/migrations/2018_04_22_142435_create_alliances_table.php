<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlliancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alliance_name');
            $table->integer('alliance_chef_id');
            $table->string('alliance_state');
            $table->string('alliance_region');
            $table->string('alliance_village')->nullable();
            $table->string('alliance_street');
            $table->integer('alliance_edrpou')->unique()->default(0);
            $table->integer('alliance_geo_alt')->default(0);
            $table->integer('alliance_geo_ltd');
            $table->integer('alliance_bank_account')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alliances');
    }
}
