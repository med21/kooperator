<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTariffFields extends Migration
{
    public function up()
    {
        Schema::table('alliances', function ($table) {
            $table->string('tariff_d')->after('alliance_edrpou')->default(0);
            $table->string('tariff_n')->after('tariff_d')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alliances', function($table) {
            $table->string('tariff_d');
            $table->string('tariff_n');
        });
    }
}
