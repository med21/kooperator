<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersLandAmount extends Migration
{
    public function up()
    {
        Schema::table('alliance_users', function ($table) {
            $table->double('alliance_users_land_amount')->after('alliance_user_address')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alliance_users', function($table) {
            $table->double('alliance_users_land_amount');

        });
    }
}
