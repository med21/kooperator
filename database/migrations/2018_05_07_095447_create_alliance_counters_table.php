<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllianceCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliance_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alliance_id');
            $table->integer('alliance_counter_type');
            $table->string('alliance_counter_number');
            $table->date('alliance_counter_date');
            $table->double('alliance_counter_tariff_d');
            $table->double('alliance_counter_tariff_n');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alliance_counters');
    }
}
