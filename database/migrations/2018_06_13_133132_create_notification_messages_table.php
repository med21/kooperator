<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *alliance_id - кооператив, котрий створив сповіщення
     *title - заголовок нотифікації
     *text - текст сповіщення
     *end_date - дата до якої сповіщення актуальне
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('alliance_id');
            $table->integer('users_count')->default(0);
            $table->string('title');
            $table->string('text');
            $table->enum('m_status',['active','inactive']);
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_messages');
    }
}
