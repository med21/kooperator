<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id', 32);
            $table->integer('alliance_id');
            $table->integer('user_id');
            $table->integer('key')->nullable(true);
            $table->double('amount');
            $table->enum('type', ['fees', 'subscription', 'electricity']);
            $table->enum('result', ['ok', 'error'])->nullable(true);
            $table->dateTime('date')->nullable(true);
            $table->enum('status', ['success', 'failure', 'error', 'sandbox','processing'])->default('processing');
            $table->string('desc')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
