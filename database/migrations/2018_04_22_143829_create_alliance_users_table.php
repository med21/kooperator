<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllianceUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliance_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alliance_user_surname');
            $table->string('alliance_user_name');
            $table->string('alliance_user_middlename');
            $table->string('alliance_user_address');
            $table->string('alliance_user_counter_number');
            $table->string('alliance_user_email');
            $table->string('alliance_user_tel');
            $table->date('alliance_user_counter_date');
            $table->string('alliance_user_counter_type');
            $table->integer('alliance_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alliance_users');
    }
}
