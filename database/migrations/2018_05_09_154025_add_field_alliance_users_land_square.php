<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAllianceUsersLandSquare extends Migration
{
    public function up()
    {
        Schema::table('alliance_users', function ($table) {
            $table->double('alliance_users_land_square')->after('alliance_users_land_amount')->default(1);
            $table->double('alliance_users_land_doc_numb')->after('alliance_users_land_square')->default(1);
            $table->date('alliance_users_land_doc_date')->after('alliance_users_land_doc_numb')->nullable(true);
            $table->integer('alliance_users_pensioner')->after('alliance_users_land_doc_date')->default(0);
            $table->string('alliance_users_pensioner_number')->after('alliance_users_pensioner')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alliance_users', function($table) {
            $table->double('alliance_users_land_square');
            $table->double('alliance_users_land_doc_numb');
            $table->date('alliance_users_land_doc_date');
            $table->integer('alliance_users_pensioner');
            $table->string('alliance_users_pensioner_number');

        });
    }
}
