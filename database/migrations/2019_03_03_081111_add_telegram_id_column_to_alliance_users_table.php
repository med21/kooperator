<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTelegramIdColumnToAllianceUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alliance_users', function (Blueprint $table) {
            $table->string('alliance_user_telegram_id', 200)->after('alliance_user_tel')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alliance_users', function (Blueprint $table) {
            $table->dropColumn('alliance_user_telegram_id');
        });
    }
}
