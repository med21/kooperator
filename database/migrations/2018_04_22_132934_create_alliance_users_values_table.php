<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllianceUsersValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alliance_users_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alliance_id');
            $table->integer('alliance_users_id');
            $table->integer('alliance_users_val_d');
            $table->integer('alliance_users_val_n')->default(0);
            $table->integer('alliance_users_consumption_d');
            $table->integer('alliance_users_consumption_n')->default(0);
            $table->date('alliance_users_date_check');
            $table->integer('alliance_users_date_interval');
            $table->float('alliance_users_payment_d');
            $table->float('alliance_users_payment_n')->default(0);
            $table->float('alliance_users_payment_total')->default(0);
            $table->float('alliance_users_payment_total_debt');
            $table->boolean('alliance_users_payment_is_paid')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alliance_users_values');
    }
}
