<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('surname')->after('member_id');
            $table->string('middle_name')->after('name');
            $table->string('tel')->after('middle_name')->nullable(true);
            $table->integer('order_id')->after('status')->nullable(true);
            $table->integer('alliance_id')->after('id')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->string('surname');
            $table->string('middle_name');
            $table->string('tel');
            $table->integer('order_id');

        });
    }
}
